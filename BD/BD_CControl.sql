-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: GS_CCCONTROL_VIAJES
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblConfiguracion`
--

DROP TABLE IF EXISTS `tblConfiguracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblConfiguracion` (
  `intIDConfiguraccion` int(11) NOT NULL AUTO_INCREMENT,
  `chrTitulo` varchar(150) DEFAULT NULL,
  `chrSubTitulo` text,
  `chrModulo` varchar(45) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDConfiguraccion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblConfiguracion`
--

LOCK TABLES `tblConfiguracion` WRITE;
/*!40000 ALTER TABLE `tblConfiguracion` DISABLE KEYS */;
INSERT INTO `tblConfiguracion` VALUES (2,'Sistema de reservación de vuelos & hospedaje','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum facilisis elit id fringilla laoreet. Praesent sit amet lacinia nunc, at porta dui.','clogin',1),(3,'Sistema de reservación de vuelos & hospedaje','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptates, dolore tenetur ipsa necessitatibus, voluptatem quidem fugit sint earum blanditiis sit eligendi.','clogin',1);
/*!40000 ALTER TABLE `tblConfiguracion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDatosBancarios`
--

DROP TABLE IF EXISTS `tblDatosBancarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDatosBancarios` (
  `intIDDatosBancarios` int(11) NOT NULL AUTO_INCREMENT,
  `chrNombreBanco` varchar(45) DEFAULT NULL,
  `chrCuenta` varchar(45) DEFAULT NULL,
  `chrNombreBeneficiario` varchar(45) DEFAULT NULL,
  `chrNumSeguridad` varchar(45) DEFAULT NULL,
  `chrAnioMesVencimiento` varchar(45) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intIDDatosBancarios`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDatosBancarios`
--

LOCK TABLES `tblDatosBancarios` WRITE;
/*!40000 ALTER TABLE `tblDatosBancarios` DISABLE KEYS */;
INSERT INTO `tblDatosBancarios` VALUES (1,'Banorte','888888888888888','Araceli Mundo',NULL,'08/20',1),(2,NULL,NULL,NULL,NULL,NULL,1),(3,NULL,NULL,NULL,NULL,NULL,1),(4,'Banorte','1234-1234-1234-1234','Rubén Ortega Miranda',NULL,'02/yy',1),(5,'Banorte','1234-1234-1234-1234','Rubén Ortega Miranda',NULL,'02/01',1);
/*!40000 ALTER TABLE `tblDatosBancarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDatosFiscales`
--

DROP TABLE IF EXISTS `tblDatosFiscales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDatosFiscales` (
  `intIDDatosFiscales` int(11) NOT NULL AUTO_INCREMENT,
  `chrRazonSocial` varchar(50) DEFAULT NULL,
  `chrRFC` varchar(50) DEFAULT NULL,
  `chrDireccion` varchar(50) DEFAULT NULL,
  `chrCiudad` varchar(50) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intIDDatosFiscales`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDatosFiscales`
--

LOCK TABLES `tblDatosFiscales` WRITE;
/*!40000 ALTER TABLE `tblDatosFiscales` DISABLE KEYS */;
INSERT INTO `tblDatosFiscales` VALUES (1,'Servicios Corporativos Control, S.A. de CV','SCC987623TU','Padre Mier 102 Col. Centro','Monterrey Nuevo León 64000',0);
/*!40000 ALTER TABLE `tblDatosFiscales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDetalleHuespedes`
--

DROP TABLE IF EXISTS `tblDetalleHuespedes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDetalleHuespedes` (
  `intIDDetalleHuespedes` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKIDEmpleado` varchar(50) DEFAULT NULL,
  `intFKIDDetalleSolicitudHotel` int(11) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intIDDetalleHuespedes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDetalleHuespedes`
--

LOCK TABLES `tblDetalleHuespedes` WRITE;
/*!40000 ALTER TABLE `tblDetalleHuespedes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblDetalleHuespedes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDetalleSolicitudHotel`
--

DROP TABLE IF EXISTS `tblDetalleSolicitudHotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDetalleSolicitudHotel` (
  `intIDDetalleSolicitudHotel` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKIDSolicitud` int(11) DEFAULT NULL,
  `chrClaveOrigen` varchar(50) DEFAULT NULL,
  `chrClaveDestino` varchar(50) DEFAULT NULL,
  `dtdFechaSalida` date DEFAULT NULL,
  `dtdFechaRegreso` date DEFAULT NULL,
  `chrPresupuesto` decimal(10,2) DEFAULT NULL,
  `chrComentarios` varchar(225) DEFAULT NULL,
  `intFKIDTipoHabitacion` int(11) DEFAULT NULL,
  `chrEsHotel` varchar(2) DEFAULT NULL,
  `intNoHabitaciones` int(11) DEFAULT NULL,
  `chrCentroCostoHospedaje` varchar(45) DEFAULT NULL,
  `chrNochesReservadas` int(11) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDDetalleSolicitudHotel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDetalleSolicitudHotel`
--

LOCK TABLES `tblDetalleSolicitudHotel` WRITE;
/*!40000 ALTER TABLE `tblDetalleSolicitudHotel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblDetalleSolicitudHotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDetalleSolicitudViaje`
--

DROP TABLE IF EXISTS `tblDetalleSolicitudViaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDetalleSolicitudViaje` (
  `intIDDetalleSolicitudViaje` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKIDSolicitud` int(11) DEFAULT NULL,
  `intFKIDTipoVuelo` int(11) DEFAULT NULL,
  `chrClaveOrigen` varchar(150) DEFAULT NULL,
  `chrClaveDestino` varchar(150) DEFAULT NULL,
  `dtdFechaSalidad` date DEFAULT NULL,
  `dtdFechaRegreso` date DEFAULT NULL,
  `chrPresupuesto` decimal(10,2) DEFAULT NULL,
  `chrComentarios` varchar(225) DEFAULT NULL,
  `intFKIDTipoViaje` int(11) DEFAULT NULL,
  `chrCentroCostoViaje` varchar(45) DEFAULT NULL,
  `chrAerolinea` varchar(45) DEFAULT NULL,
  `chrMotivoViaje` varchar(45) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDDetalleSolicitudViaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDetalleSolicitudViaje`
--

LOCK TABLES `tblDetalleSolicitudViaje` WRITE;
/*!40000 ALTER TABLE `tblDetalleSolicitudViaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblDetalleSolicitudViaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDetalleViajeros`
--

DROP TABLE IF EXISTS `tblDetalleViajeros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDetalleViajeros` (
  `intIDDetalleViajes` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKIDEmpleado` varchar(50) DEFAULT NULL,
  `intFKIDDetalleSolicitudViaje` int(11) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDDetalleViajes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDetalleViajeros`
--

LOCK TABLES `tblDetalleViajeros` WRITE;
/*!40000 ALTER TABLE `tblDetalleViajeros` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblDetalleViajeros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblEmpleados`
--

DROP TABLE IF EXISTS `tblEmpleados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblEmpleados` (
  `intIDEmpleado` varchar(50) NOT NULL,
  `chrCodigoEmpleado` varchar(50) DEFAULT NULL,
  `chrNombre` varchar(45) DEFAULT NULL,
  `chrApellidoPaterno` varchar(45) DEFAULT NULL,
  `chrApellidoMaterno` varchar(45) DEFAULT NULL,
  `dtdFechaNacimiento` date DEFAULT NULL,
  `chrPassword` varchar(45) DEFAULT NULL,
  `dtdFechaAlta` date DEFAULT NULL,
  `dtdFechaBaja` date DEFAULT NULL,
  `chrCorreo` varchar(45) DEFAULT NULL,
  `intIDPerfil` int(11) DEFAULT NULL,
  `chrEstatus` varchar(45) DEFAULT NULL,
  `chrSociedad` varchar(45) DEFAULT NULL,
  `chrPuesto` varchar(45) DEFAULT NULL,
  `chrDepartamento` varchar(45) DEFAULT NULL,
  `chrCentroCosto` varchar(45) DEFAULT NULL,
  `chrPasaporte` varchar(45) DEFAULT NULL,
  `chrSession` varchar(250) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  `intFKIDEmpleadoAutorizador` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`intIDEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblEmpleados`
--

LOCK TABLES `tblEmpleados` WRITE;
/*!40000 ALTER TABLE `tblEmpleados` DISABLE KEYS */;
INSERT INTO `tblEmpleados` VALUES ('01000014','01000014','Xiomara',NULL,NULL,NULL,'control2019','2019-12-17',NULL,'amundo@globalsoftm.com',1,'True',NULL,'Tester',NULL,'1409999999',NULL,'460635d8-2124-11ea-8a12-92b550b0bcb8',0,'123'),('1000014','1000014','JOSE JUAN CASTAÑEDA MARTINEZ',NULL,NULL,'1971-01-27',NULL,NULL,NULL,'amundo@globalsoftm.com',NULL,NULL,'Control DOME S.A. de C.V.','GERENTE DE PROCESOS Y DESARROLLO','GERENCIA DE PROC Y DES DE APLICACIONES','OF11320000',NULL,NULL,0,NULL),('1000025','1000025','VICTOR VALERO FENTON',NULL,NULL,'1962-04-25',NULL,NULL,NULL,'amundo@globalsoftm.com',NULL,NULL,'Control DOME S.A. de C.V.','SUBDIRECTOR DE MERCADOTECNIA','SUBDIRECCION DE MERCADOTECNIA','OF11320000',NULL,NULL,0,NULL),('1000028','1000028','FRANCISCO JAVIER RODRIGUEZ VILLANUEVA',NULL,NULL,'1978-11-18','vm2007123','2020-01-07',NULL,'amundo@globalsoftm.com',1,'True','Control DOME S.A. de C.V.','SUBDIRECTOR DE COMPRAS','SUBDIRECCION DE ROPA','OF11320000',NULL,'2de061be-3183-11ea-8a12-92b550b0bcb8',0,'10019170'),('1000029','1000029','JOSE HUMBERTO GONZALEZ LARA',NULL,NULL,'1980-10-17','vm2007123','2020-02-27',NULL,'prueba3@globalsoftm.com',1,'True','Control DOME S.A. de C.V.','SUBDIRECTOR DE OPERACIONES','SUBDIRECCION DE OPERACIONES A','OF11320000',NULL,'9d8e5ec3-59bf-11ea-8724-92b550b0bcb8',0,'10019170'),('1000030','1000030','CARLOS ALBERTO GIL GARDA',NULL,NULL,'1989-06-06',NULL,NULL,NULL,'amundo@globalsoftm.com',NULL,NULL,'Dominio Estratégico, S. de R.L. de C.V.','ASESOR','DIRECCION GENERAL','OF13340000',NULL,NULL,0,NULL),('1000031','1000031','VANESSA CASTILLEJA DE LA VEGA',NULL,NULL,'1974-12-30','vm2007123','2020-02-27',NULL,'prueba4@globalsoftm.com',2,'True','Control DOME S.A. de C.V.','COORDINADOR DE COMPRAS','COORDINACION DAMAS','OF11320000',NULL,'db58495e-59bf-11ea-8724-92b550b0bcb8',0,'10019170'),('1000032','1000032','CLAUDIA SUSANA MORENO SALAZAR',NULL,NULL,'1976-03-31','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',5,'True','Control DOME S.A. de C.V.','GERENTE DE FISCAL','GERENCIA DE FISCAL','OF11320000',NULL,'aa2f5f56-026f-11ea-aab4-92b550b0bcb8',0,'10019170'),('1000033','1000033','ENRIQUE CRUZ GUTIERREZ',NULL,NULL,'1972-11-04',NULL,NULL,NULL,'amundo@globalsoftm.com',NULL,NULL,'DOMINIO ESTRATTGICO, S. DE R.L. DE C.V.','TECNICO HANGAR','DIRECCION GENERAL','OF13340000',NULL,NULL,0,NULL),('1000034','1000034','RICARDO VAZQUEZ SALDA-A',NULL,NULL,'1971-05-14',NULL,NULL,NULL,'amundo@globalsoftm.com',NULL,NULL,'Dominio Estratégico, S. de R.L. de C.V.','SUBDIRECTOR DE LOGISTICA','SUBDIRECCION DE DISTRIBUCION Y LOGISTICA','OF13340000',NULL,NULL,0,NULL),('10015149','10015149','ALDO EDIGAR GARCIA DE LA CERDA',NULL,NULL,'1987-03-02','vm2007123','2020-02-04',NULL,'amundo@globalsoftm.com',2,'True','Servicios Corporativos Control, S.A. de','COORDINADOR DE APERTURAS Y REMODELACIONE','DIRECCION DE OPERACIONES','1402000000',NULL,'de8df652-47a1-11ea-a953-92b550b0bcb8',0,'10019170'),('10019170','10019170','HECTOR SOSA DE LEON',NULL,NULL,'1986-04-25','vm2007123','2020-02-27',NULL,'prueba2@globalsoftm.com',3,'True','Servicios Corporativos Control, S.A. de','COORDINADOR DE PREVEN DE APERT. Y REMOD.','SEGURIDAD CORPORATIVA','1402010100',NULL,'bb1431c3-59bf-11ea-8724-92b550b0bcb8',0,'0'),('10019171','10019171','HORACIO HERNANDEZ RAMOS',NULL,NULL,'1973-04-06','vm2007123','2019-12-19',NULL,'amundo@globalsoftm.com',3,'True','SERVICIOS OPERATIVOS CONTROL, S.A. DE C.','GARROTERO','MANTENIMIENTO Y LIMPIEZA','C025040003',NULL,'f8ed1c67-22aa-11ea-8a12-92b550b0bcb8',0,'0'),('10034873','10034873','Mario Nava Rodríguez',NULL,NULL,'1980-04-13','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',4,'True','Servicios Corporativos Control, S.A. de','GERENTE DE OPERACIONES RESTAURANTE','OPERACIONES','12345',NULL,'e9e60bf4-026f-11ea-aab4-92b550b0bcb8',0,'10019170'),('10036280','10036280','MARIANA TELLO SALDAÑA',NULL,NULL,'1979-07-24','vm2007123','2020-02-11',NULL,'amundo@globalsoftm.com',5,'True','Servicios Corporativos Control, S.A. de','JEFE DE OFICINA PROYECTOS TI','NUEVOS PROYECTOS','1405010000',NULL,'f99a69e1-4d2e-11ea-8724-92b550b0bcb8',0,'10019170'),('10038070','10038070','DAVID EDUARDO PIMENTEL YANEZ',NULL,NULL,'1981-03-19','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',1,'True','Servicios Corporativos Control, S.A. de','GERENTE DE CONSUMOS INTERNOS','GERENCIA DE CONSUMOS INTERNOS','1402000100',NULL,'6294a4d0-026f-11ea-aab4-92b550b0bcb8',0,'10019170'),('10043872','10043872','VICTOR HUGO TOVAR TOVAR',NULL,NULL,'1982-03-06','vm2007123','2019-11-13',NULL,'amundo@globalsoftm.com',1,'True','Servicios Corporativos Control, S.A. de','ANALISTA DE INFORMATICA JR.','SISTEMAS FINANCIEROS','1405010302',NULL,'0af7c931-065e-11ea-aab4-92b550b0bcb8',0,'10019170'),('1960012','1960012','RODOLFO LOPEZ FLORES',NULL,NULL,'1983-08-05','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',1,'True','Servicios Corporativos Control, S.A. de','GERENTE DE MERCADEO','GERENCIA DE MERCADEO','1402010003',NULL,'4f1e7a84-026f-11ea-aab4-92b550b0bcb8',0,'10019170'),('280966','280966','JORGE ALBERTO URIBE CASTAÑEDA',NULL,NULL,'1970-12-07','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',1,'True','Control DOME S.A. de C.V.','GERENTE DE SOPORTE Y COMUNICACIONES','GERENCIA DE SOPORTE TECNICO Y COMUNICAC','OF11320000',NULL,'3594516e-026f-11ea-aab4-92b550b0bcb8',0,'10019170'),('341974','341974','GUILLERMO FIGUEROA VALDEZ',NULL,NULL,'1973-09-12','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',NULL,'True','Control DOME S.A. de C.V.','GERENTE DE SISTEMAS','GERENCIA DE SISTEMAS','OF11320000',NULL,'16b98951-026f-11ea-aab4-92b550b0bcb8',0,NULL),('448761','448761','AMERICA GUADALUPE GARCIA GARCIA',NULL,NULL,'1977-06-04','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',NULL,'True','Servicios Corporativos Control, S.A. de','GERENTE DE CONTRALORIA','GERENCIA DE CONTRALORIA','1405000100',NULL,'ee6f4768-026e-11ea-aab4-92b550b0bcb8',0,NULL),('825117','825117','JAMES ERIK GURROLA FLORES',NULL,NULL,'1977-02-10','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',1,'True','Servicios Corporativos Control, S.A. de','ADMINISTRACION DE INVENTARIOS','ADMINISTRACION DE INVENTARIOS','1405000104',NULL,'d727cc05-026e-11ea-aab4-92b550b0bcb8',0,'10019170'),('9013758','9013758','ANGEL IGNACIO MARTIN SCHEVENIN',NULL,NULL,'1975-03-16','vm2007123','2019-12-19',NULL,'amundo@globalsoftm.com',1,'True','Servicios Corporativos Control, S.A. de','CHEFF','OPERACIONES','1401020000',NULL,'402ab9a0-2298-11ea-8a12-92b550b0bcb8',0,'10019170'),('938548','938548','HECTOR RAUL ORTEGON PUGA',NULL,NULL,'1975-03-20','vm2007123','2019-11-08',NULL,'amundo@globalsoftm.com',NULL,'True','Servicios Corporativos Control, S.A. de','COORDINADOR DE INVENTARIOS FISICOS','INVENTARIOS','1405000104',NULL,'709b6229-026f-11ea-aab4-92b550b0bcb8',0,NULL);
/*!40000 ALTER TABLE `tblEmpleados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblEstatusSolicitud`
--

DROP TABLE IF EXISTS `tblEstatusSolicitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblEstatusSolicitud` (
  `intIDEstatus` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chrEstatus` varchar(45) DEFAULT NULL,
  `chrDescripcion` varchar(45) DEFAULT NULL,
  `chrColor` varchar(45) DEFAULT NULL,
  `intDelete` int(11) NOT NULL DEFAULT '1',
  `chrEsUsuario` varchar(45) DEFAULT NULL,
  `chrEsAdministrador` varchar(45) DEFAULT NULL,
  `chrEsAutorizador` varchar(45) DEFAULT NULL,
  `chrEsAuditor` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`intIDEstatus`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblEstatusSolicitud`
--

LOCK TABLES `tblEstatusSolicitud` WRITE;
/*!40000 ALTER TABLE `tblEstatusSolicitud` DISABLE KEYS */;
INSERT INTO `tblEstatusSolicitud` VALUES (1,'Pendiente','Cuando esta con la administradora y no ha co','#66ffff',1,'1,2','1,2,3,4,5','1,2,3,5,4','7'),(2,'Cotizada','Cuando la solicitud ha sido cotizada y enviad','#000080',1,'',NULL,NULL,NULL),(3,'Rechazada','Cuando por algún motivo administradora regres','#00ff80',1,NULL,NULL,NULL,NULL),(4,'En autorización.','Cuando autorizador tiene la solicitud pero no','#80ff00',1,NULL,NULL,NULL,NULL),(5,'Autorizado','Cuando autorizador a enviado solicitud con vo','#ff6fcf',1,NULL,NULL,NULL,NULL),(6,'Re cotización','Cuando usuario a regresado la solicitud a adm','#808080',1,NULL,NULL,NULL,NULL),(7,'Cerrada','Cuando la solicitud termina el proceso es dec','#f8f8f8f',1,NULL,NULL,NULL,NULL),(8,'Cancelada','Cuando la solicitud es  cancelada ','#ff0000',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tblEstatusSolicitud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblHistorialCambios`
--

DROP TABLE IF EXISTS `tblHistorialCambios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblHistorialCambios` (
  `intIDHistoricoCambios` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chrFolio` int(11) DEFAULT NULL,
  `intFKIDDetalleHotel` int(11) DEFAULT NULL,
  `intFKIDDetalleVuelo` int(11) DEFAULT NULL,
  `intNoOpcion` int(11) DEFAULT NULL,
  `intFKIDReservacionHotel` int(11) DEFAULT NULL,
  `intFKIDReservacionViaje` int(11) DEFAULT NULL,
  `chrNoEmpleado` int(11) DEFAULT NULL,
  `dtdFechaCambio` date DEFAULT NULL,
  `chrCambio` varchar(45) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDHistoricoCambios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblHistorialCambios`
--

LOCK TABLES `tblHistorialCambios` WRITE;
/*!40000 ALTER TABLE `tblHistorialCambios` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblHistorialCambios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblImagenesCotizar`
--

DROP TABLE IF EXISTS `tblImagenesCotizar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblImagenesCotizar` (
  `intIDImagenesCotizar` int(11) NOT NULL AUTO_INCREMENT,
  `chrNombre` varchar(255) DEFAULT NULL,
  `chrPath` varchar(255) DEFAULT NULL,
  `intFKIDServicio` int(11) DEFAULT NULL,
  `intFKIDSolicitud` int(11) DEFAULT NULL,
  `intNoOpcionAutorizador` int(11) DEFAULT NULL,
  `intNoOpcionUsuario` int(11) DEFAULT NULL,
  `chrEsRechazada` tinyint(4) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  `chrContentType` varchar(95) DEFAULT NULL,
  `chrEsArchivo` tinyint(4) NOT NULL DEFAULT '0',
  `chrEsCarta` tinyint(4) NOT NULL DEFAULT '0',
  `intFKIDDetalleSolicitudViajeHotel` int(11) DEFAULT NULL,
  PRIMARY KEY (`intIDImagenesCotizar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblImagenesCotizar`
--

LOCK TABLES `tblImagenesCotizar` WRITE;
/*!40000 ALTER TABLE `tblImagenesCotizar` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblImagenesCotizar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblLogSolicitudRechazada`
--

DROP TABLE IF EXISTS `tblLogSolicitudRechazada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblLogSolicitudRechazada` (
  `intIDLogSolciitud` int(11) NOT NULL AUTO_INCREMENT,
  `intFKIDSolicitud` int(11) DEFAULT NULL,
  `chrMotivoRechazo` text,
  `intCodigoEmpleado` varchar(50) DEFAULT NULL,
  `dtdFechaRechazo` date DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDLogSolciitud`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblLogSolicitudRechazada`
--

LOCK TABLES `tblLogSolicitudRechazada` WRITE;
/*!40000 ALTER TABLE `tblLogSolicitudRechazada` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblLogSolicitudRechazada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblOperacionesSolicitudAvion`
--

DROP TABLE IF EXISTS `tblOperacionesSolicitudAvion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblOperacionesSolicitudAvion` (
  `intIDOperacionesSolicitudesAvion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKDetalleSolicitudViaje` int(11) DEFAULT NULL,
  `intNoOpcion` int(11) DEFAULT NULL,
  `chrClaveReservacion` varchar(25) DEFAULT NULL,
  `dtdFechaRespuesta` datetime DEFAULT NULL,
  `chrLineaArea` varchar(150) DEFAULT NULL,
  `intFKIDTipoVuelo` int(11) DEFAULT NULL,
  `chrClaveOrigen` varchar(50) DEFAULT NULL,
  `chrClaveDestino` varchar(45) DEFAULT NULL,
  `dtdFechaSalida` datetime DEFAULT NULL,
  `dtdFechaRegreso` datetime DEFAULT NULL,
  `decCosto` decimal(10,0) NOT NULL DEFAULT '0',
  `chrEscala` int(11) DEFAULT NULL,
  `chrNoEscalas` int(11) DEFAULT NULL,
  `chrAceptada` int(11) DEFAULT NULL,
  `chrAutorizada` int(11) DEFAULT NULL,
  `chrReservada` int(11) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  `chrRFC` varchar(255) DEFAULT NULL,
  `decCostoExtraAvion` decimal(10,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`intIDOperacionesSolicitudesAvion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblOperacionesSolicitudAvion`
--

LOCK TABLES `tblOperacionesSolicitudAvion` WRITE;
/*!40000 ALTER TABLE `tblOperacionesSolicitudAvion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblOperacionesSolicitudAvion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblOperacionesSolicitudEscalasAvion`
--

DROP TABLE IF EXISTS `tblOperacionesSolicitudEscalasAvion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblOperacionesSolicitudEscalasAvion` (
  `intIDOperacionesSolicitudEscalaAvion` int(11) NOT NULL AUTO_INCREMENT,
  `intFKIDOperacionesSolicitudAvion` int(11) DEFAULT NULL,
  `chrClaveOrigenEscala` varchar(150) DEFAULT NULL,
  `chrClaveDestinoEscala` varchar(150) DEFAULT NULL,
  `dtdFechaSalidaEscala` datetime DEFAULT NULL,
  `intDetele` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDOperacionesSolicitudEscalaAvion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblOperacionesSolicitudEscalasAvion`
--

LOCK TABLES `tblOperacionesSolicitudEscalasAvion` WRITE;
/*!40000 ALTER TABLE `tblOperacionesSolicitudEscalasAvion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblOperacionesSolicitudEscalasAvion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblOperacionesSolicitudHotel`
--

DROP TABLE IF EXISTS `tblOperacionesSolicitudHotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblOperacionesSolicitudHotel` (
  `intIDOperacionesSolicitudHotel` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKIDDetalleSolicitudHotel` int(11) DEFAULT NULL,
  `intNoOpcion` int(11) DEFAULT NULL,
  `chrCodigoConfirmacion` varchar(25) DEFAULT NULL,
  `dtdFechaRespuesta` date DEFAULT NULL,
  `chrLineaHotel` varchar(150) DEFAULT NULL,
  `dtdFechaEntrada` date DEFAULT NULL,
  `dtdFechaSalidad` date DEFAULT NULL,
  `intNoHabitaciones` int(11) DEFAULT NULL,
  `intFKIDTipoHabitacion` int(11) DEFAULT NULL,
  `decCosto` decimal(10,0) NOT NULL DEFAULT '0',
  `intAceptada` int(11) DEFAULT NULL,
  `intAutorizada` int(11) DEFAULT NULL,
  `intReservada` int(11) DEFAULT NULL,
  `chrRFC` varchar(250) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  `decCostoExtraHotel` decimal(10,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`intIDOperacionesSolicitudHotel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblOperacionesSolicitudHotel`
--

LOCK TABLES `tblOperacionesSolicitudHotel` WRITE;
/*!40000 ALTER TABLE `tblOperacionesSolicitudHotel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblOperacionesSolicitudHotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblPantallas`
--

DROP TABLE IF EXISTS `tblPantallas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPantallas` (
  `intIDPantallas` int(11) NOT NULL AUTO_INCREMENT,
  `intIDModulo` varchar(45) NOT NULL DEFAULT '',
  `intIDSistema` varchar(45) NOT NULL DEFAULT '',
  `chrDescripcionModulo` varchar(150) DEFAULT NULL,
  `chrUrl` varchar(200) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  `intFKIDPerfil` int(11) DEFAULT NULL,
  `intIDModuloPadre` varchar(45) DEFAULT NULL,
  `chrMenuPrincipal` varchar(45) DEFAULT NULL,
  `chrImagenIcono` varchar(150) DEFAULT NULL,
  `chrValue` varchar(150) DEFAULT NULL,
  `chrCodigoEmpleado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`intIDPantallas`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblPantallas`
--

LOCK TABLES `tblPantallas` WRITE;
/*!40000 ALTER TABLE `tblPantallas` DISABLE KEYS */;
INSERT INTO `tblPantallas` VALUES (1,'1','1','Solicitudes','solicitud-de-reservaciones',1,2,'','SI','','00000','10015149'),(2,'3','1','Reportes','reportes',1,2,'','SI','','00000','10015149'),(3,'4','1','Administrador de solicitudes','administrador-solicitudes',1,2,'','SI','','00000','10015149'),(4,'6','1','Configuracion','configuracion',1,2,'','SI','','20000','10015149'),(5,'1','1','Solicitudes','solicitud-de-reservaciones',1,1,'','SI','','00000','1000029'),(6,'3','1','Reportes','reportes',1,1,'','SI','','00000','1000029'),(7,'2','1','Estatus de solicitud','detalle-vuelo-hospedaje',1,1,'','SI','','10000','1000029'),(8,'1','1','Solicitudes','solicitud-de-reservaciones',1,2,'','SI','','00000','1000031'),(9,'3','1','Reportes','reportes',1,2,'','SI','','00000','1000031'),(10,'4','1','Administrador de solicitudes','administrador-solicitudes',1,2,'','SI','','00000','1000031'),(11,'2','1','Estatus de solicitud','detalle-vuelo-hospedaje',1,2,'','SI','','10000','1000031'),(12,'1','1','Solicitudes','solicitud-de-reservaciones',1,1,'','SI','','00000','1000028'),(13,'3','1','Reportes','reportes',1,1,'','SI','','00000','1000028'),(14,'2','1','Estatus de solicitud','detalle-vuelo-hospedaje',1,1,'','SI','','10000','1000028'),(15,'1','1','Solicitudes','solicitud-de-reservaciones',1,3,'','SI','','00000','10019170'),(16,'3','1','Reportes','reportes',1,3,'','SI','','00000','10019170'),(17,'5','1','Autorizador solicitudes','autorizador-solicitudes',1,3,'','SI','','00000','10019170'),(18,'2','1','Estatus de solicitud','detalle-vuelo-hospedaje',1,3,'','SI','','10000','10019170');
/*!40000 ALTER TABLE `tblPantallas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblPerfiles`
--

DROP TABLE IF EXISTS `tblPerfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPerfiles` (
  `intIDPerfil` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chrDescripcion` varchar(150) DEFAULT NULL,
  `chrClave` varchar(5) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDPerfil`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblPerfiles`
--

LOCK TABLES `tblPerfiles` WRITE;
/*!40000 ALTER TABLE `tblPerfiles` DISABLE KEYS */;
INSERT INTO `tblPerfiles` VALUES (1,'Usuario','U',1),(2,'Administrador','A',1),(3,'Autorizador','D',1),(4,'Auditor','B',1),(5,'SuperAdmin','E',1);
/*!40000 ALTER TABLE `tblPerfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblReservacionHotel`
--

DROP TABLE IF EXISTS `tblReservacionHotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblReservacionHotel` (
  `intIDReservacionHotel` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKIDOperacionesSolicitudHotel` int(11) DEFAULT NULL,
  `intNoOpcion` int(11) DEFAULT NULL,
  `chrContabilizada` varchar(225) DEFAULT NULL,
  `chrTipoCompra` varchar(225) DEFAULT NULL,
  `chrCodigoReservacion` varchar(45) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intIDReservacionHotel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblReservacionHotel`
--

LOCK TABLES `tblReservacionHotel` WRITE;
/*!40000 ALTER TABLE `tblReservacionHotel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblReservacionHotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblReservacionesViaje`
--

DROP TABLE IF EXISTS `tblReservacionesViaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblReservacionesViaje` (
  `intIDReservacionViaje` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFKIDOperacionesSolicitudesAvion` int(11) DEFAULT NULL,
  `intNoOpcion` int(11) DEFAULT NULL,
  `chrContabilizada` varchar(225) DEFAULT NULL,
  `chrTipoCompra` varchar(225) DEFAULT NULL,
  `chrCodigoReservacion` varchar(225) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intIDReservacionViaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblReservacionesViaje`
--

LOCK TABLES `tblReservacionesViaje` WRITE;
/*!40000 ALTER TABLE `tblReservacionesViaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblReservacionesViaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblSemaforo`
--

DROP TABLE IF EXISTS `tblSemaforo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSemaforo` (
  `intIDSemaforo` int(11) NOT NULL AUTO_INCREMENT,
  `chrTiempo` int(11) DEFAULT NULL,
  `chrColor` varchar(45) DEFAULT NULL,
  `chrOperador` varchar(5) DEFAULT NULL,
  `intDelete` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`intIDSemaforo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblSemaforo`
--

LOCK TABLES `tblSemaforo` WRITE;
/*!40000 ALTER TABLE `tblSemaforo` DISABLE KEYS */;
INSERT INTO `tblSemaforo` VALUES (1,1,'danger','>=',1),(2,30,'success','<=',1),(3,NULL,'success','>',1);
/*!40000 ALTER TABLE `tblSemaforo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblSolicitudRechazada`
--

DROP TABLE IF EXISTS `tblSolicitudRechazada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSolicitudRechazada` (
  `intIDSolicitudesRechazadas` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFolio` int(11) DEFAULT NULL,
  `intClaveDestino` int(11) DEFAULT NULL,
  `dtdFechaSalidad` datetime DEFAULT NULL,
  `dtdFechaRegreso` varchar(45) DEFAULT NULL,
  `chrPresupuesto` decimal(10,0) DEFAULT NULL,
  `chrComentarios` text,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDSolicitudesRechazadas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblSolicitudRechazada`
--

LOCK TABLES `tblSolicitudRechazada` WRITE;
/*!40000 ALTER TABLE `tblSolicitudRechazada` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblSolicitudRechazada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblSolicitudes`
--

DROP TABLE IF EXISTS `tblSolicitudes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSolicitudes` (
  `intIDSolicitud` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `intFolio` int(11) DEFAULT NULL,
  `dtdFechaSolicitud` datetime DEFAULT NULL,
  `intNoSolicitante` varchar(50) DEFAULT NULL,
  `intNoEmpleadoCreador` varchar(50) DEFAULT NULL,
  `intFKIDEstatus` int(11) DEFAULT NULL,
  `chrComentarios` text,
  `intFKIDTipoSolicitud` int(11) DEFAULT NULL,
  `intIDAutorizadorSuplente` varchar(50) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  `intNumCotizadas` int(11) NOT NULL DEFAULT '0',
  `dtdFechaAutorizada` date DEFAULT NULL,
  `dtdFechaCotizada` datetime DEFAULT NULL,
  `intVisto` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`intIDSolicitud`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblSolicitudes`
--

LOCK TABLES `tblSolicitudes` WRITE;
/*!40000 ALTER TABLE `tblSolicitudes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblSolicitudes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTipoHabitacion`
--

DROP TABLE IF EXISTS `tblTipoHabitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTipoHabitacion` (
  `intIDTipoHabitacion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chrDescripcion` varchar(45) DEFAULT NULL,
  `intDelete` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDTipoHabitacion`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTipoHabitacion`
--

LOCK TABLES `tblTipoHabitacion` WRITE;
/*!40000 ALTER TABLE `tblTipoHabitacion` DISABLE KEYS */;
INSERT INTO `tblTipoHabitacion` VALUES (1,'Sencillas',1),(2,'Dobles',1),(7,'Casa duplex',0),(8,'Casa adosada',0),(9,'prueba edit',0),(10,'PRUEBA ELIMINAR',0),(11,'pruebacampo',0),(12,'campoaa',0),(13,'camponuevo',0),(14,'frfrfr',0);
/*!40000 ALTER TABLE `tblTipoHabitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTipoSolicitud`
--

DROP TABLE IF EXISTS `tblTipoSolicitud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTipoSolicitud` (
  `intIDTipoSolicitud` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chrDescripcion` varchar(45) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDTipoSolicitud`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTipoSolicitud`
--

LOCK TABLES `tblTipoSolicitud` WRITE;
/*!40000 ALTER TABLE `tblTipoSolicitud` DISABLE KEYS */;
INSERT INTO `tblTipoSolicitud` VALUES (1,'Vuelo + Hotel',0),(2,'Vuelo',0),(3,'Hospedaje',0);
/*!40000 ALTER TABLE `tblTipoSolicitud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTipoViaje`
--

DROP TABLE IF EXISTS `tblTipoViaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTipoViaje` (
  `intIDTipoViaje` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chrDescripcion` varchar(150) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDTipoViaje`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTipoViaje`
--

LOCK TABLES `tblTipoViaje` WRITE;
/*!40000 ALTER TABLE `tblTipoViaje` DISABLE KEYS */;
INSERT INTO `tblTipoViaje` VALUES (1,'Nacional',1),(2,'Internacional',1),(8,'Local',0),(9,'Local',0),(10,'Local',0),(11,'Locala',0),(12,'prueba edit',0);
/*!40000 ALTER TABLE `tblTipoViaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTipoVuelo`
--

DROP TABLE IF EXISTS `tblTipoVuelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTipoVuelo` (
  `intIDTipoVuelo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chrDescripcion` varchar(45) DEFAULT NULL,
  `intDelete` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intIDTipoVuelo`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTipoVuelo`
--

LOCK TABLES `tblTipoVuelo` WRITE;
/*!40000 ALTER TABLE `tblTipoVuelo` DISABLE KEYS */;
INSERT INTO `tblTipoVuelo` VALUES (1,'Sencillo ',1),(2,'Redondo',1),(17,'Prueba de vuelo',0),(18,'Local',0),(19,'Doble',0),(20,'Test 2',0),(21,'prueba edit',0),(22,'PRUEBA DOS',0);
/*!40000 ALTER TABLE `tblTipoVuelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'GS_CCCONTROL_VIAJES'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-04 17:16:56
