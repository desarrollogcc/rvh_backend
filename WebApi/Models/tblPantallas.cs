
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace WebApi.Models
{

using System;
    using System.Collections.Generic;
    
public partial class tblPantallas
{

    public int intIDPantallas { get; set; }

    public string intIDModulo { get; set; }

    public string intIDSistema { get; set; }

    public string chrDescripcionModulo { get; set; }

    public string chrUrl { get; set; }

    public sbyte intDelete { get; set; }

    public Nullable<int> intFKIDPerfil { get; set; }

    public string intIDModuloPadre { get; set; }

    public string chrMenuPrincipal { get; set; }

    public string chrImagenIcono { get; set; }

    public string chrValue { get; set; }

    public string chrCodigoEmpleado { get; set; }

}

}
