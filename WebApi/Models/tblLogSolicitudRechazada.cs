
//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------


namespace WebApi.Models
{

using System;
    using System.Collections.Generic;
    
public partial class tblLogSolicitudRechazada
{

    public int intIDLogSolciitud { get; set; }

    public Nullable<int> intFKIDSolicitud { get; set; }

    public string chrMotivoRechazo { get; set; }

    public string intCodigoEmpleado { get; set; }

    public Nullable<System.DateTime> dtdFechaRechazo { get; set; }

    public sbyte intDelete { get; set; }

}

}
