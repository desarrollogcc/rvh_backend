//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class sistemas
    {
        public sistemas()
        {
            this.correossoporte = new HashSet<correossoporte>();
            this.modulos = new HashSet<modulos>();
        }
    
        public string idSistema { get; set; }
        public string nombreSistema { get; set; }
        public string URL { get; set; }
        public Nullable<int> Filtro { get; set; }
    
        public virtual ICollection<correossoporte> correossoporte { get; set; }
        public virtual ICollection<modulos> modulos { get; set; }
    }
}
