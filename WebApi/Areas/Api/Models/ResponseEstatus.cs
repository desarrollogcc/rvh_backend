﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models
{
    public class ResponseEstatus
    {
        public int status { get; set; }
        public string message { get; set; }
        public object result { get; set; }
    }
}