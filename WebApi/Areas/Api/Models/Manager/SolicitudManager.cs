﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Models;
using WebApi.Utils;

namespace WebApi.Areas.Api.Models.Manager
{
    public class SolicitudManager
    {
        private GS_CCCONTROL_VIAJESEntities db;

        public SolicitudManager()
        {
            db = new GS_CCCONTROL_VIAJESEntities();
        }

        public ISolicitud SaveSolicitud(ISolicitud infoSolicitud)
        {

            tblSolicitudes solicitud;

            if (infoSolicitud != null)
            {
                if (infoSolicitud.intIDSolicitud <= 0)
                {

                    solicitud = new tblSolicitudes();

                    solicitud.intFKIDEstatus = infoSolicitud.intFKIDEstatus;
                    solicitud.intFKIDTipoSolicitud = infoSolicitud.intFKIDTipoSolicitud;
                    solicitud.intNoSolicitante = infoSolicitud.intNoSolicitante;
                    solicitud.intNoEmpleadoCreador = infoSolicitud.intNoEmpleadoCreador;
                    solicitud.chrComentarios = infoSolicitud.chrComentarios;
                    solicitud.dtdFechaSolicitud = infoSolicitud.dtdFechaSolicitud;
                    solicitud.intFolio = infoSolicitud.intFolio;
                    solicitud.intIDAutorizadorSuplente = infoSolicitud.intIDAutorizadorSuplente;
                    solicitud.intVisto = 0;
                    solicitud.intNumCotizadas = 0;
                    solicitud.intDelete = 1;
                    db.tblSolicitudes.Add(solicitud);

                }
                else
                {
                    solicitud = (from item in db.tblSolicitudes
                                 where item.intIDSolicitud == infoSolicitud.intIDSolicitud
                                 select item).FirstOrDefault();
                    if (solicitud != null)
                    {
                        solicitud.intFolio = (int)infoSolicitud.intIDSolicitud;
                        solicitud.intFKIDEstatus = infoSolicitud.intFKIDEstatus;
                        solicitud.intFKIDTipoSolicitud = infoSolicitud.intFKIDTipoSolicitud;
                        solicitud.intNoSolicitante = infoSolicitud.intNoSolicitante;
                        solicitud.intNoEmpleadoCreador = infoSolicitud.intNoEmpleadoCreador;
                        solicitud.chrComentarios = infoSolicitud.chrComentarios;
                        solicitud.dtdFechaSolicitud = infoSolicitud.dtdFechaSolicitud;
                        solicitud.intFolio = infoSolicitud.intFolio;
                        solicitud.intIDAutorizadorSuplente = infoSolicitud.intIDAutorizadorSuplente;

                    }

                }

                db.SaveChanges();
                infoSolicitud.intIDSolicitud = solicitud.intIDSolicitud;
            }




            return infoSolicitud;
        }

        public IDetalleSolicitudViaje DetalleSolicitudViaje(IDetalleSolicitudViaje infoDetalleSolictud)
        {
            tblDetalleSolicitudViaje detallesolcitudViajes;
            if (infoDetalleSolictud != null)
            {
                if (infoDetalleSolictud.intIDDetalleSolicitudViaje <= 0)
                {

                    detallesolcitudViajes = new tblDetalleSolicitudViaje();
                    detallesolcitudViajes.intFKIDSolicitud = infoDetalleSolictud.intFKIDSolicitud;
                    detallesolcitudViajes.intFKIDTipoVuelo = infoDetalleSolictud.intFKIDTipoVuelo;
                    detallesolcitudViajes.chrClaveOrigen = infoDetalleSolictud.chrClaveOrigen;
                    detallesolcitudViajes.chrClaveDestino = infoDetalleSolictud.chrClaveDestino;
                    detallesolcitudViajes.dtdFechaSalidad = infoDetalleSolictud.dtdFechaSalidad;
                    detallesolcitudViajes.dtdFechaRegreso = infoDetalleSolictud.dtdFechaRegreso;
                    detallesolcitudViajes.chrPresupuesto = infoDetalleSolictud.chrPresupuesto;
                    detallesolcitudViajes.chrComentarios = infoDetalleSolictud.chrComentarios;
                    detallesolcitudViajes.intFKIDTipoViaje = infoDetalleSolictud.intFKIDTipoViaje;
                    detallesolcitudViajes.chrCentroCostoViaje = infoDetalleSolictud.chrCentroCostoViaje;
                    detallesolcitudViajes.chrAerolinea = infoDetalleSolictud.chrAerolinea;
                    detallesolcitudViajes.chrMotivoViaje = infoDetalleSolictud.chrMotivoViaje;
                    detallesolcitudViajes.intDelete = 1;
                    db.tblDetalleSolicitudViaje.Add(detallesolcitudViajes);

                }
                else
                {
                    detallesolcitudViajes = (from item in db.tblDetalleSolicitudViaje
                                             where item.intIDDetalleSolicitudViaje == infoDetalleSolictud.intIDDetalleSolicitudViaje
                                             select item).FirstOrDefault();

                    if (detallesolcitudViajes != null)
                    {
                        detallesolcitudViajes.intFKIDSolicitud = infoDetalleSolictud.intFKIDSolicitud;
                        detallesolcitudViajes.intFKIDTipoVuelo = infoDetalleSolictud.intFKIDTipoVuelo;
                        detallesolcitudViajes.chrClaveOrigen = infoDetalleSolictud.chrClaveOrigen;
                        detallesolcitudViajes.chrClaveDestino = infoDetalleSolictud.chrClaveDestino;
                        detallesolcitudViajes.dtdFechaSalidad = infoDetalleSolictud.dtdFechaSalidad;
                        detallesolcitudViajes.dtdFechaRegreso = infoDetalleSolictud.dtdFechaRegreso;
                        detallesolcitudViajes.chrPresupuesto = infoDetalleSolictud.chrPresupuesto;
                        detallesolcitudViajes.chrComentarios = infoDetalleSolictud.chrComentarios;
                        detallesolcitudViajes.intFKIDTipoViaje = infoDetalleSolictud.intFKIDTipoViaje;
                        detallesolcitudViajes.chrCentroCostoViaje = infoDetalleSolictud.chrCentroCostoViaje;
                        detallesolcitudViajes.chrAerolinea = infoDetalleSolictud.chrAerolinea;
                        detallesolcitudViajes.chrMotivoViaje = infoDetalleSolictud.chrMotivoViaje;

                    }
                }
                db.SaveChanges();
                infoDetalleSolictud.intIDDetalleSolicitudViaje = detallesolcitudViajes.intIDDetalleSolicitudViaje;

            }

            return infoDetalleSolictud;

        }

        public IDetalleSolicitudHotel DetalleSolicitudHotel(IDetalleSolicitudHotel infoDetalleHotel)
        {

            tblDetalleSolicitudHotel solicitudHotel;

            if (infoDetalleHotel != null)
            {

                if (infoDetalleHotel.intIDDetalleSolicitudHotel <= 0)
                {
                    solicitudHotel = new tblDetalleSolicitudHotel();

                    solicitudHotel.intFKIDSolicitud = infoDetalleHotel.intFKIDSolicitud;
                    solicitudHotel.chrClaveDestino = infoDetalleHotel.chrClaveDestino;
                    solicitudHotel.chrClaveOrigen = infoDetalleHotel.chrClaveOrigen;
                    solicitudHotel.dtdFechaRegreso = infoDetalleHotel.dtdFechaRegreso;
                    solicitudHotel.dtdFechaSalida = infoDetalleHotel.dtdFechaSalidad;
                    solicitudHotel.chrPresupuesto = infoDetalleHotel.chrPresuspuesto;
                    solicitudHotel.chrComentarios = infoDetalleHotel.chrComentarios;
                    solicitudHotel.intFKIDTipoHabitacion = infoDetalleHotel.intFKIDTipoHabitacion;
                    solicitudHotel.intNoHabitaciones = infoDetalleHotel.intNoHabitaciones;
                    solicitudHotel.chrEsHotel = infoDetalleHotel.chrEsHotel;
                    solicitudHotel.chrCentroCostoHospedaje = infoDetalleHotel.chrCentroCostoHospedaje;
                    solicitudHotel.chrNochesReservadas = infoDetalleHotel.chrNochesReservadas;
                    solicitudHotel.intDelete = 1;

                    db.tblDetalleSolicitudHotel.Add(solicitudHotel);
                }
                else
                {

                    solicitudHotel = (from item in db.tblDetalleSolicitudHotel
                                      where item.intIDDetalleSolicitudHotel == infoDetalleHotel.intIDDetalleSolicitudHotel
                                      select item).FirstOrDefault();

                    if (solicitudHotel != null)
                    {
                        solicitudHotel.intFKIDSolicitud = infoDetalleHotel.intFKIDSolicitud;
                        solicitudHotel.chrClaveDestino = infoDetalleHotel.chrClaveDestino;
                        solicitudHotel.chrClaveOrigen = infoDetalleHotel.chrClaveOrigen;
                        solicitudHotel.dtdFechaRegreso = infoDetalleHotel.dtdFechaRegreso;
                        solicitudHotel.dtdFechaSalida = infoDetalleHotel.dtdFechaSalidad;
                        solicitudHotel.chrPresupuesto = infoDetalleHotel.chrPresuspuesto;
                        solicitudHotel.chrComentarios = infoDetalleHotel.chrComentarios;
                        solicitudHotel.intFKIDTipoHabitacion = infoDetalleHotel.intFKIDTipoHabitacion;
                        solicitudHotel.intNoHabitaciones = infoDetalleHotel.intNoHabitaciones;
                        solicitudHotel.chrEsHotel = infoDetalleHotel.chrEsHotel;
                        solicitudHotel.chrCentroCostoHospedaje = infoDetalleHotel.chrCentroCostoHospedaje;
                        solicitudHotel.chrNochesReservadas = infoDetalleHotel.chrNochesReservadas;
                    }


                }
                db.SaveChanges();
                infoDetalleHotel.intIDDetalleSolicitudHotel = solicitudHotel.intIDDetalleSolicitudHotel;

            }
            return infoDetalleHotel;

        }

        public IDetalleHuespedes DetalleHuespedes(IDetalleHuespedes infoDetalleHuespedes)
        {

            tblDetalleHuespedes detalleHuespedes;
            if (infoDetalleHuespedes != null)
            {
                if (infoDetalleHuespedes.intIDDetalleHuespedes <= 0)
                {
                    detalleHuespedes = new tblDetalleHuespedes();
                    detalleHuespedes.intFKIDEmpleado = infoDetalleHuespedes.intFKIDEmpleado;
                    detalleHuespedes.intFKIDDetalleSolicitudHotel = infoDetalleHuespedes.intFKIDDetalleSolicitudHotel;
                    detalleHuespedes.intDelete = 1;
                    db.tblDetalleHuespedes.Add(detalleHuespedes);


                }
                else
                {
                    detalleHuespedes = (from item in db.tblDetalleHuespedes
                                        where item.intIDDetalleHuespedes == infoDetalleHuespedes.intIDDetalleHuespedes
                                        && item.intFKIDDetalleSolicitudHotel == infoDetalleHuespedes.intFKIDDetalleSolicitudHotel
                                        select item).FirstOrDefault();

                    if (detalleHuespedes != null)
                    {
                        detalleHuespedes.intFKIDEmpleado = infoDetalleHuespedes.intFKIDEmpleado;
                        detalleHuespedes.intFKIDDetalleSolicitudHotel = infoDetalleHuespedes.intFKIDDetalleSolicitudHotel;

                    }
                }
                db.SaveChanges();
                infoDetalleHuespedes.intIDDetalleHuespedes = detalleHuespedes.intIDDetalleHuespedes;

            }


            return infoDetalleHuespedes;
        }

        public int DeleteDetalleHuespedes(int intFKIDDetalleSolicitudHotel, string chrCodigoEmpleado)
        {

            int intDelete = 0;
            if (intFKIDDetalleSolicitudHotel > 0)
            {

                var detalleHuespedes = (from _detalleHuespedes in db.tblDetalleHuespedes
                                        where _detalleHuespedes.intFKIDDetalleSolicitudHotel == intFKIDDetalleSolicitudHotel && _detalleHuespedes.intFKIDEmpleado == chrCodigoEmpleado
                                        select _detalleHuespedes).FirstOrDefault();

                if (detalleHuespedes != null)
                {
                    detalleHuespedes.intDelete = 0;

                }

                db.SaveChanges();

            }
            else
            {
                intDelete = 1;
            }

            return intDelete;
        }

        public IOperacionesSolicitudHotel OperacionesSolicitudHotel(IOperacionesSolicitudHotel infoOperacionesHotel)
        {

            tblOperacionesSolicitudHotel operacionesHotel;

            if (infoOperacionesHotel != null)
            {
                if (infoOperacionesHotel.intIDOperacionesSolicitudHotel <= 0)
                {
                    operacionesHotel = new tblOperacionesSolicitudHotel();
                    operacionesHotel.intFKIDDetalleSolicitudHotel = infoOperacionesHotel.intFKIDDetalleSolicitudHotel;
                    operacionesHotel.intNoOpcion = infoOperacionesHotel.intNoOpcion;
                    operacionesHotel.chrCodigoConfirmacion = infoOperacionesHotel.chrCodigoConfirmacion;
                    operacionesHotel.dtdFechaRespuesta = infoOperacionesHotel.dtdFechaRespuesta;
                    operacionesHotel.chrLineaHotel = infoOperacionesHotel.chrLineaHotel;
                    operacionesHotel.dtdFechaEntrada = infoOperacionesHotel.dtdFechaEntrada;
                    operacionesHotel.dtdFechaSalidad = infoOperacionesHotel.dtdFechaSalidad;
                    operacionesHotel.intNoHabitaciones = infoOperacionesHotel.intNoHabitaciones;
                    operacionesHotel.intFKIDTipoHabitacion = infoOperacionesHotel.intFKIDTipoHabitacion;
                    operacionesHotel.decCosto = infoOperacionesHotel.decCosto;
                    operacionesHotel.intAceptada = infoOperacionesHotel.intAceptada;
                    operacionesHotel.intAutorizada = infoOperacionesHotel.intAutorizada;
                    operacionesHotel.intReservada = infoOperacionesHotel.intReservada;
                    operacionesHotel.chrRFC = infoOperacionesHotel.chrRFC;
                    operacionesHotel.decCostoExtraHotel = infoOperacionesHotel.chrCostoExtraHotel;
                    operacionesHotel.intDelete = 1;
                    db.tblOperacionesSolicitudHotel.Add(operacionesHotel);


                }
                else
                {
                    operacionesHotel = (from item in db.tblOperacionesSolicitudHotel
                                        where item.intIDOperacionesSolicitudHotel == infoOperacionesHotel.intIDOperacionesSolicitudHotel
                                        select item).FirstOrDefault();

                    if (operacionesHotel != null)
                    {
                        operacionesHotel.intNoOpcion = infoOperacionesHotel.intNoOpcion;
                        operacionesHotel.chrCodigoConfirmacion = infoOperacionesHotel.chrCodigoConfirmacion;
                        operacionesHotel.dtdFechaRespuesta = infoOperacionesHotel.dtdFechaRespuesta;
                        operacionesHotel.chrLineaHotel = infoOperacionesHotel.chrLineaHotel;
                        operacionesHotel.dtdFechaEntrada = infoOperacionesHotel.dtdFechaEntrada;
                        operacionesHotel.dtdFechaSalidad = infoOperacionesHotel.dtdFechaSalidad;
                        operacionesHotel.intNoHabitaciones = infoOperacionesHotel.intNoHabitaciones;
                        operacionesHotel.intFKIDTipoHabitacion = infoOperacionesHotel.intFKIDTipoHabitacion;
                        operacionesHotel.decCosto = infoOperacionesHotel.decCosto;
                        operacionesHotel.intAceptada = infoOperacionesHotel.intAceptada;
                        operacionesHotel.intAutorizada = infoOperacionesHotel.intAutorizada;
                        operacionesHotel.intReservada = infoOperacionesHotel.intReservada;
                        operacionesHotel.chrRFC = infoOperacionesHotel.chrRFC;
                        operacionesHotel.decCostoExtraHotel = infoOperacionesHotel.chrCostoExtraHotel;

                    }

                }
                db.SaveChanges();
                infoOperacionesHotel.intIDOperacionesSolicitudHotel = operacionesHotel.intIDOperacionesSolicitudHotel;
            }


            return infoOperacionesHotel;
        }

        public IOperacionesSolicitudHotel OperacionesUpdateSolicitudHotel(IOperacionesSolicitudHotel infoOperacionesHotel)
        {

            tblOperacionesSolicitudHotel operacionesHotel;

            if (infoOperacionesHotel != null)
            {
                
                    operacionesHotel = (from item in db.tblOperacionesSolicitudHotel
                                        where item.intFKIDDetalleSolicitudHotel == infoOperacionesHotel.intFKIDDetalleSolicitudHotel
                                        select item).FirstOrDefault();

                    if (operacionesHotel != null)
                    {
                        
                        operacionesHotel.decCostoExtraHotel = infoOperacionesHotel.chrCostoExtraHotel;

                    }

                
                db.SaveChanges();
                infoOperacionesHotel.intIDOperacionesSolicitudHotel = operacionesHotel.intIDOperacionesSolicitudHotel;
            }


            return infoOperacionesHotel;
        }

        public IReservacionHotel ReservacionHotel(IReservacionHotel infoReservacionHotel)
        {
            tblReservacionHotel reservacionesHotel;

            if (infoReservacionHotel != null)
            {
                if (infoReservacionHotel.intIDReservacionHotel <= 0)
                {
                    reservacionesHotel = new tblReservacionHotel();
                    reservacionesHotel.intFKIDOperacionesSolicitudHotel = infoReservacionHotel.intFKIDOperacionesSolicitudHotel;
                    reservacionesHotel.intNoOpcion = infoReservacionHotel.intNoOpcion;
                    reservacionesHotel.chrContabilizada = infoReservacionHotel.chrContabilizada;
                    reservacionesHotel.chrTipoCompra = infoReservacionHotel.chrTipoCompra;
                    reservacionesHotel.chrCodigoReservacion = infoReservacionHotel.chrCodigoReservacion;
                    reservacionesHotel.intDelete = 1;

                    db.tblReservacionHotel.Add(reservacionesHotel);
                }
                else
                {
                    reservacionesHotel = (from item in db.tblReservacionHotel
                                          where item.intIDReservacionHotel == infoReservacionHotel.intIDReservacionHotel
                                          select item).FirstOrDefault();
                    if (reservacionesHotel != null)
                    {

                        reservacionesHotel.intFKIDOperacionesSolicitudHotel = infoReservacionHotel.intFKIDOperacionesSolicitudHotel;
                        reservacionesHotel.intNoOpcion = infoReservacionHotel.intNoOpcion;
                        reservacionesHotel.chrContabilizada = infoReservacionHotel.chrContabilizada;
                        reservacionesHotel.chrTipoCompra = infoReservacionHotel.chrTipoCompra;
                        reservacionesHotel.chrCodigoReservacion = infoReservacionHotel.chrCodigoReservacion;

                    }


                }

                var reservaciones = (from _reservaciones in db.tblReservacionHotel
                                     join tblOperacionesSolicitudHotel _operaciones in db.tblOperacionesSolicitudHotel
                                     on _reservaciones.intFKIDOperacionesSolicitudHotel equals (int)_operaciones.intIDOperacionesSolicitudHotel
                                     join tblDetalleSolicitudHotel _detallehotel in db.tblDetalleSolicitudHotel
                                     on _operaciones.intFKIDDetalleSolicitudHotel equals (int)_detallehotel.intIDDetalleSolicitudHotel
                                     where _reservaciones.intIDReservacionHotel == infoReservacionHotel.intIDReservacionHotel
                                     select _detallehotel).FirstOrDefault();

                if (reservaciones != null)
                {
                    int? intIDSolicitud = reservaciones.intFKIDSolicitud;

                    CerrarSolicitud((int)intIDSolicitud);
                }

                db.SaveChanges();
                infoReservacionHotel.intIDReservacionHotel = reservacionesHotel.intIDReservacionHotel;

            }
            return infoReservacionHotel;

        }

        public IDetalleViajeros DetalleViajeros(IDetalleViajeros infoDetalleViajeros)
        {

            tblDetalleViajeros detalleViajeros;

            if (infoDetalleViajeros != null)
            {
                if (infoDetalleViajeros.intIDDetalleViajes <= 0)
                {

                    detalleViajeros = new tblDetalleViajeros();
                    detalleViajeros.intFKIDEmpleado = infoDetalleViajeros.intFKIDEmpleado;
                    detalleViajeros.intFKIDDetalleSolicitudViaje = infoDetalleViajeros.intFKIDDetalleSolicitudViaje;
                    detalleViajeros.intDelete = 1;
                    db.tblDetalleViajeros.Add(detalleViajeros);
                }
                else
                {
                    detalleViajeros = (from item in db.tblDetalleViajeros
                                       where item.intIDDetalleViajes == infoDetalleViajeros.intIDDetalleViajes
                                       && item.intFKIDDetalleSolicitudViaje == infoDetalleViajeros.intFKIDDetalleSolicitudViaje
                                       select item).FirstOrDefault();
                    if (detalleViajeros != null)
                    {
                        detalleViajeros.intFKIDEmpleado = infoDetalleViajeros.intFKIDEmpleado;
                        detalleViajeros.intFKIDDetalleSolicitudViaje = infoDetalleViajeros.intFKIDDetalleSolicitudViaje;
                    }


                }
                db.SaveChanges();
                infoDetalleViajeros.intIDDetalleViajes = detalleViajeros.intIDDetalleViajes;

            }
            return infoDetalleViajeros;

        }

        public int DeleteDetalleViajeros(int intFKIDDetalleSolicitudViaje, string chrCodigoEmpleado)
        {

            int intDelete = 0;

            var detalleViajeID = (from detalleviajeid in db.tblDetalleViajeros
                                  where detalleviajeid.intFKIDDetalleSolicitudViaje == intFKIDDetalleSolicitudViaje
                                  && detalleviajeid.intFKIDEmpleado == chrCodigoEmpleado
                                  select detalleviajeid).FirstOrDefault();

            if (detalleViajeID.intIDDetalleViajes > 0)
            {

                detalleViajeID.intDelete = 0;

                db.SaveChanges();

            }
            else
            {
                intDelete = 1;
            }

            return intDelete;
        }

        public IOperacionesSolicitudAvion OperacionesSolicitudAvion(IOperacionesSolicitudAvion infoOperacionAvion)
        {

            tblOperacionesSolicitudAvion operacionSolicitudAvion;


            if (infoOperacionAvion != null)
            {
                if (infoOperacionAvion.intIDOperacionesSolicitudesAvion <= 0)
                {
                    operacionSolicitudAvion = new tblOperacionesSolicitudAvion();
                    operacionSolicitudAvion.intFKDetalleSolicitudViaje = infoOperacionAvion.intFKDetalleSolicitudViaje;
                    operacionSolicitudAvion.intNoOpcion = infoOperacionAvion.intNoOpcion;
                    operacionSolicitudAvion.chrClaveReservacion = infoOperacionAvion.chrClaveReservacion;
                    operacionSolicitudAvion.dtdFechaRespuesta = infoOperacionAvion.dtdFechaRespuesta;
                    operacionSolicitudAvion.chrLineaArea = infoOperacionAvion.chrLineaArea;
                    operacionSolicitudAvion.intFKIDTipoVuelo = infoOperacionAvion.intFKIDTipoVuelo;
                    operacionSolicitudAvion.chrClaveOrigen = infoOperacionAvion.chrClaveOrigen;
                    operacionSolicitudAvion.chrClaveDestino = infoOperacionAvion.chrClaveDestino;
                    operacionSolicitudAvion.dtdFechaSalida = infoOperacionAvion.dtdFechaSalida;
                    operacionSolicitudAvion.dtdFechaRegreso = infoOperacionAvion.dtdFechaRegreso;
                    operacionSolicitudAvion.decCosto = infoOperacionAvion.decCosto;
                    operacionSolicitudAvion.chrEscala = infoOperacionAvion.chrEscala;
                    operacionSolicitudAvion.chrNoEscalas = infoOperacionAvion.chrNoEscalas;
                    operacionSolicitudAvion.chrAceptada = infoOperacionAvion.chrAceptada;
                    operacionSolicitudAvion.chrAutorizada = infoOperacionAvion.chrAutorizada;
                    operacionSolicitudAvion.chrReservada = infoOperacionAvion.chrReservada;
                    operacionSolicitudAvion.intDelete = 1;
                    operacionSolicitudAvion.chrRFC = infoOperacionAvion.chrRFC;
                    operacionSolicitudAvion.decCostoExtraAvion = infoOperacionAvion.chrCostoExtraAvion;
                    db.tblOperacionesSolicitudAvion.Add(operacionSolicitudAvion);

                }
                else
                {
                    operacionSolicitudAvion = (from item in db.tblOperacionesSolicitudAvion
                                               where item.intIDOperacionesSolicitudesAvion == infoOperacionAvion.intIDOperacionesSolicitudesAvion
                                               select item).FirstOrDefault();
                    if (operacionSolicitudAvion != null)
                    {
                        operacionSolicitudAvion.intFKDetalleSolicitudViaje = infoOperacionAvion.intFKDetalleSolicitudViaje;
                        operacionSolicitudAvion.intNoOpcion = infoOperacionAvion.intNoOpcion;
                        operacionSolicitudAvion.chrClaveReservacion = infoOperacionAvion.chrClaveReservacion;
                        operacionSolicitudAvion.dtdFechaRespuesta = infoOperacionAvion.dtdFechaRespuesta;
                        operacionSolicitudAvion.chrLineaArea = infoOperacionAvion.chrLineaArea;
                        operacionSolicitudAvion.intFKIDTipoVuelo = infoOperacionAvion.intFKIDTipoVuelo;
                        operacionSolicitudAvion.chrClaveOrigen = infoOperacionAvion.chrClaveOrigen;
                        operacionSolicitudAvion.chrClaveDestino = infoOperacionAvion.chrClaveDestino;
                        operacionSolicitudAvion.dtdFechaSalida = infoOperacionAvion.dtdFechaSalida;
                        operacionSolicitudAvion.dtdFechaRegreso = infoOperacionAvion.dtdFechaRegreso;
                        operacionSolicitudAvion.decCosto = infoOperacionAvion.decCosto;
                        operacionSolicitudAvion.chrEscala = infoOperacionAvion.chrEscala;
                        operacionSolicitudAvion.chrNoEscalas = infoOperacionAvion.chrNoEscalas;
                        operacionSolicitudAvion.chrAceptada = infoOperacionAvion.chrAceptada;
                        operacionSolicitudAvion.chrAutorizada = infoOperacionAvion.chrAutorizada;
                        operacionSolicitudAvion.chrReservada = infoOperacionAvion.chrReservada;
                        operacionSolicitudAvion.chrRFC = infoOperacionAvion.chrRFC;
                        operacionSolicitudAvion.decCostoExtraAvion = infoOperacionAvion.chrCostoExtraAvion;
                    }



                }
                db.SaveChanges();
                infoOperacionAvion.intIDOperacionesSolicitudesAvion = operacionSolicitudAvion.intIDOperacionesSolicitudesAvion;
            }
            return infoOperacionAvion;

        }

        public IOperacionesSolicitudAvion OperacionesUpdateSolicitudAvion(IOperacionesSolicitudAvion infoOperacionAvion)
        {

            tblOperacionesSolicitudAvion operacionSolicitudAvion;


            if (infoOperacionAvion != null)
            {
                
                    operacionSolicitudAvion = (from item in db.tblOperacionesSolicitudAvion
                                               where item.intFKDetalleSolicitudViaje == infoOperacionAvion.intFKDetalleSolicitudViaje
                                               select item).FirstOrDefault();
                    if (operacionSolicitudAvion != null)
                    {
                    
                        operacionSolicitudAvion.decCostoExtraAvion = infoOperacionAvion.chrCostoExtraAvion;
                    }



                
                db.SaveChanges();
                infoOperacionAvion.intIDOperacionesSolicitudesAvion = operacionSolicitudAvion.intIDOperacionesSolicitudesAvion;
            }
            return infoOperacionAvion;

        }


        public bool SaveEscalasVuelo(List<IEscalasVuelo> infoEscala)
        {

            tblOperacionesSolicitudEscalasAvion escalasVuelo;

            if (infoEscala != null)
            {
                try
                {
                    foreach (IEscalasVuelo item in infoEscala)
                    {


                        if (item.intIDOperacionesSolicitudEscalaAvion <= 0)
                        {
                            escalasVuelo = new tblOperacionesSolicitudEscalasAvion();
                            escalasVuelo.chrClaveOrigenEscala = item.chrClaveOrigenEscala;
                            escalasVuelo.chrClaveDestinoEscala = item.chrClaveDestinoEscala;
                            escalasVuelo.dtdFechaSalidaEscala = item.dtdFechaSalidaEscala;
                            escalasVuelo.intFKIDOperacionesSolicitudAvion = item.intFKIDOperacionesSolicitudAvion;
                            escalasVuelo.intDetele = 1;
                            db.tblOperacionesSolicitudEscalasAvion.Add(escalasVuelo);
                        }
                        else
                        {
                            escalasVuelo = (from _escalas in db.tblOperacionesSolicitudEscalasAvion
                                            where _escalas.intIDOperacionesSolicitudEscalaAvion == item.intIDOperacionesSolicitudEscalaAvion
                                            select _escalas).FirstOrDefault();
                            if (escalasVuelo != null)
                            {

                                escalasVuelo.chrClaveOrigenEscala = item.chrClaveOrigenEscala;
                                escalasVuelo.chrClaveDestinoEscala = item.chrClaveDestinoEscala;
                                escalasVuelo.dtdFechaSalidaEscala = item.dtdFechaSalidaEscala;
                                escalasVuelo.intFKIDOperacionesSolicitudAvion = item.intFKIDOperacionesSolicitudAvion;

                            }

                        }
                        db.SaveChanges();
                    }

                }
                catch (Exception ex)
                {
                    ex.ToString();
                    return false;
                }


            }

            return true;
        }

        public IReservacionViaje ReservacionViaje(IReservacionViaje infoReservacionViaje)
        {
            tblReservacionesViaje reservacionViajes;

            if (infoReservacionViaje != null)
            {
                if (infoReservacionViaje.intIDReservacionViaje <= 0)
                {
                    reservacionViajes = new tblReservacionesViaje();
                    reservacionViajes.intFKIDOperacionesSolicitudesAvion = infoReservacionViaje.intFKIDOperacionesSolicitudesAvion;
                    reservacionViajes.intNoOpcion = infoReservacionViaje.intNoOpcion;
                    reservacionViajes.chrContabilizada = infoReservacionViaje.chrContabilizada;
                    reservacionViajes.chrTipoCompra = infoReservacionViaje.chrTipoCompra;
                    reservacionViajes.chrCodigoReservacion = infoReservacionViaje.chrCodigoReservacion;
                    reservacionViajes.intDelete = 1;

                    db.tblReservacionesViaje.Add(reservacionViajes);
                }
                else
                {
                    reservacionViajes = (from item in db.tblReservacionesViaje
                                         where item.intIDReservacionViaje == infoReservacionViaje.intIDReservacionViaje
                                         select item).FirstOrDefault();

                    if (reservacionViajes != null)
                    {
                        reservacionViajes.intFKIDOperacionesSolicitudesAvion = infoReservacionViaje.intFKIDOperacionesSolicitudesAvion;
                        reservacionViajes.intNoOpcion = infoReservacionViaje.intNoOpcion;
                        reservacionViajes.chrContabilizada = infoReservacionViaje.chrContabilizada;
                        reservacionViajes.chrTipoCompra = infoReservacionViaje.chrTipoCompra;
                        reservacionViajes.chrCodigoReservacion = infoReservacionViaje.chrCodigoReservacion;

                    }
                }

                var reservaciones = (from _reservaciones in db.tblReservacionesViaje
                                     join tblOperacionesSolicitudAvion _operaciones in db.tblOperacionesSolicitudAvion
                                     on _reservaciones.intFKIDOperacionesSolicitudesAvion equals (int)_operaciones.intIDOperacionesSolicitudesAvion
                                     join tblDetalleSolicitudViaje _detalleviaje in db.tblDetalleSolicitudViaje
                                     on _operaciones.intFKDetalleSolicitudViaje equals (int)_detalleviaje.intIDDetalleSolicitudViaje
                                     where _reservaciones.intIDReservacionViaje == infoReservacionViaje.intIDReservacionViaje
                                     select _detalleviaje).FirstOrDefault();

                if (reservaciones != null)
                {
                    int? intIDSolicitud = reservaciones.intFKIDSolicitud;

                    CerrarSolicitud((int)intIDSolicitud);
                }



                db.SaveChanges();
                infoReservacionViaje.intIDReservacionViaje = reservacionViajes.intIDReservacionViaje;
            }

            return infoReservacionViaje;

        }

        //public dynamic  GetResumenVueloPorEmpleado(string intIDEmpleado)
        public List<IResumenVuelo> GetResumenVueloPorEmpleado(string intIDEmpleado)
        {

            int intDelete = Convert.ToSByte(true);
            
            /*var lstSoliciud = (from item in db.tblSolicitudes
                                               join tblEstatusSolicitud estatus in db.tblEstatusSolicitud
                                                   on item.intFKIDEstatus equals (int)estatus.intIDEstatus
                                               join tblEmpleados item4 in db.tblEmpleados on item.intNoSolicitante equals item4.intIDEmpleado
                                               join tblDetalleSolicitudViaje item5 in db.tblDetalleSolicitudViaje on (int?)item.intIDSolicitud equals (int?)item5.intFKIDSolicitud 
                                               join tblTipoViaje item6 in db.tblTipoViaje on (int?)item5.intFKIDTipoViaje equals (int?)item6.intIDTipoViaje
                                               join tblTipoVuelo item7 in db.tblTipoVuelo on (int?)item5.intFKIDTipoVuelo equals (int?)item7.intIDTipoVuelo
                                               where item.intNoSolicitante == intIDEmpleado
                                               && item.intDelete == intDelete

                                               group new IResumenVuelo {
                                                   intIDSolicitud = (int)item.intIDSolicitud,
                                                   chrNoEmpleado = item4.chrCodigoEmpleado,
                                                   chrNombreEmpleado = item4.chrNombre,
                                                   chrApellidoPaterno = item4.chrApellidoPaterno,
                                                   chrApellidoMaterno = item4.chrApellidoMaterno,
                                                   chrCentroCosto = item5.chrCentroCostoViaje,
                                                   chrTipoViaje = item6.chrDescripcion,
                                                   chrTipoVuelo = item7.chrDescripcion,
                                                   chrAerolinea = item5.chrAerolinea,
                                                   dtdFechaNacimiento = item4.dtdFechaNacimiento,
                                                   chrOrigen = item5.chrClaveOrigen,
                                                   chrDestino = item5.chrClaveDestino,
                                                   dtdFechaSalidad = item5.dtdFechaSalidad,
                                                   dtdFechaRegreso = item5.dtdFechaRegreso,
                                                   intIDEstatus = (int) estatus.intIDEstatus,
                                                   chrEstatus = estatus.chrEstatus,
                                                   intFKIDDetalleSolicitudViaje = (int)item5.intIDDetalleSolicitudViaje,
                                                   intFKIDTipoSolicitud = (int)item.intFKIDTipoSolicitud
                                               } by item5.intFKIDSolicitud into g select g).ToList();*/

            List<IResumenVuelo> lstSoliciud = (from item in db.tblSolicitudes

                                               join tblEstatusSolicitud estatus in db.tblEstatusSolicitud
                                                   on item.intFKIDEstatus equals (int)estatus.intIDEstatus
                                               join tblEmpleados item4 in db.tblEmpleados on item.intNoSolicitante equals item4.intIDEmpleado
                                               join tblDetalleSolicitudViaje item5 in db.tblDetalleSolicitudViaje on (int?)item.intIDSolicitud equals (int?)item5.intFKIDSolicitud
                                               join tblTipoViaje item6 in db.tblTipoViaje on (int?)item5.intFKIDTipoViaje equals (int?)item6.intIDTipoViaje
                                               join tblTipoVuelo item7 in db.tblTipoVuelo on (int?)item5.intFKIDTipoVuelo equals (int?)item7.intIDTipoVuelo
                                               where item.intNoSolicitante == intIDEmpleado
                                               && item.intDelete == intDelete

                                               select new IResumenVuelo
                                               {
                                                   intIDSolicitud = (int)item.intIDSolicitud,
                                                   chrNoEmpleado = item4.chrCodigoEmpleado,
                                                   chrNombreEmpleado = item4.chrNombre,
                                                   chrApellidoPaterno = item4.chrApellidoPaterno,
                                                   chrApellidoMaterno = item4.chrApellidoMaterno,
                                                   chrCentroCosto = item5.chrCentroCostoViaje,
                                                   chrTipoViaje = item6.chrDescripcion,
                                                   chrTipoVuelo = item7.chrDescripcion,
                                                   chrAerolinea = item5.chrAerolinea,
                                                   dtdFechaNacimiento = item4.dtdFechaNacimiento,
                                                   chrOrigen = item5.chrClaveOrigen,
                                                   chrDestino = item5.chrClaveDestino,
                                                   dtdFechaSalidad = item5.dtdFechaSalidad,
                                                   dtdFechaRegreso = item5.dtdFechaRegreso,
                                                   intIDEstatus = (int)estatus.intIDEstatus,
                                                   chrEstatus = estatus.chrEstatus,
                                                   intFKIDDetalleSolicitudViaje = (int)item5.intIDDetalleSolicitudViaje,
                                                   intFKIDTipoSolicitud = (int)item.intFKIDTipoSolicitud
                                               }).ToList();

          


            db.Dispose();

            return lstSoliciud;
        }

        public List<IResumenHotel> GetResumenHotelPorEmpleado(string intIDEmpleado)
        {
            int intDelete = Convert.ToSByte(true);
            List<IResumenHotel> lstResumenHotel = (from solicitud in db.tblSolicitudes
                                                   join tblEmpleados empleados in db.tblEmpleados on solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                                   join tblDetalleSolicitudHotel detalleHotel in db.tblDetalleSolicitudHotel on (int?)solicitud.intIDSolicitud equals (int?)detalleHotel.intFKIDSolicitud
                                                   join tblTipoHabitacion tipoHabitacion in db.tblTipoHabitacion on detalleHotel.intFKIDTipoHabitacion equals (int?)tipoHabitacion.intIDTipoHabitacion
                                                   join tblEstatusSolicitud estatus in db.tblEstatusSolicitud
                                                   on solicitud.intFKIDEstatus equals (int)estatus.intIDEstatus
                                                   where solicitud.intNoSolicitante == intIDEmpleado
                                                   && solicitud.intDelete == intDelete
                                                   select new IResumenHotel
                                                   {
                                                       intIDSolicitud = (int)solicitud.intIDSolicitud,
                                                       chrNoEmpleado = empleados.chrCodigoEmpleado,
                                                       chrNombreEmpleado = empleados.chrNombre,
                                                       chrApellidoPaterno = empleados.chrApellidoPaterno,
                                                       chrApellidoMaterno = empleados.chrApellidoMaterno,
                                                       chrCentroCostoHotel = detalleHotel.chrCentroCostoHospedaje,
                                                       chrHotel = detalleHotel.chrEsHotel,
                                                       chrHabitaciones = detalleHotel.intNoHabitaciones,
                                                       chrTipoHabitacion = tipoHabitacion.chrDescripcion,
                                                       chrOrigen = detalleHotel.chrClaveOrigen,
                                                       chrDestino = detalleHotel.chrClaveDestino,
                                                       dtdFechaRegreso = detalleHotel.dtdFechaRegreso,
                                                       dtdFechaSalidad = detalleHotel.dtdFechaSalida,
                                                       chrNochesReservadas = detalleHotel.chrNochesReservadas,
                                                       intIDEstatus = (int)estatus.intIDEstatus,
                                                       chrEstatus = estatus.chrEstatus,
                                                       intFKIDDetalleSolicitudHotel = (int)detalleHotel.intIDDetalleSolicitudHotel,
                                                       intFKIDTipoSolicitud = (int)solicitud.intFKIDTipoSolicitud
                                                   }).ToList();
            db.Dispose();

            return lstResumenHotel;
        }

        public ISolicitud ObtieneDetallesSolicitud(int intIDSolicitud)
        {
            ISolicitud solicitud = (from item in db.tblSolicitudes
                                    where item.intIDSolicitud == intIDSolicitud
                                    select new ISolicitud
                                    {
                                        intIDSolicitud = (int)item.intIDSolicitud,
                                        intFKIDTipoSolicitud = (int) item.intFKIDTipoSolicitud

                                    }).FirstOrDefault();

            solicitud.detalleSolicitudViaje = (from _detalleSolicitudViaje in db.tblDetalleSolicitudViaje
                                               where _detalleSolicitudViaje.intFKIDSolicitud == solicitud.intIDSolicitud

                                               select new IDetalleSolicitudViaje
                                               {
                                                   intIDDetalleSolicitudViaje = (int)_detalleSolicitudViaje.intIDDetalleSolicitudViaje,
                                                   intFKIDSolicitud = (int)_detalleSolicitudViaje.intFKIDSolicitud,
                                                   
                                               }).ToList();

            solicitud.detalleSolicitudHotel = (from _detalleSolicitudHotel in db.tblDetalleSolicitudHotel
                                               where _detalleSolicitudHotel.intFKIDSolicitud == solicitud.intIDSolicitud

                                               select new IDetalleSolicitudHotel
                                               {
                                                   intIDDetalleSolicitudHotel = _detalleSolicitudHotel.intIDDetalleSolicitudHotel,
                                                
                                               }).ToList();



            return solicitud;
        }
        public ISolicitud MuestraDatosVueloHotel(int intIDSolicitud, int intFKIDDetalleSolicitudViaje)
        {
            

            int intDelete = Convert.ToSByte(true);
            ISolicitud solicitud = (from item in db.tblSolicitudes
                                    where item.intIDSolicitud == intIDSolicitud
                                    select new ISolicitud
                                    {
                                        intIDSolicitud = (int)item.intIDSolicitud,
                                        intFolio = (int)item.intFolio,
                                        dtdFechaSolicitud = (DateTime)item.dtdFechaSolicitud,
                                        intNoSolicitante = item.intNoSolicitante,
                                        intNoEmpleadoCreador = item.intNoEmpleadoCreador,
                                        intFKIDEstatus = item.intFKIDEstatus,
                                        chrComentarios = item.chrComentarios,
                                        intFKIDTipoSolicitud = item.intFKIDTipoSolicitud,
                                        intIDAutorizadorSuplente = item.intIDAutorizadorSuplente

                                    }).FirstOrDefault();

            solicitud.estatusSolicitud = (from _estatus in db.tblEstatusSolicitud
                                          where _estatus.intIDEstatus == solicitud.intFKIDEstatus
                                          select new IEstatusSolicitud
                                          {
                                              intIDEstatus = _estatus.intIDEstatus,
                                              chrEstatus = _estatus.chrEstatus,
                                              chrDescripcion = _estatus.chrDescripcion,
                                              chrColor = _estatus.chrColor

                                          }).FirstOrDefault();


            solicitud.detalleSolicitudViaje = (from _detalleSolicitudViaje in db.tblDetalleSolicitudViaje
                                               where _detalleSolicitudViaje.intFKIDSolicitud == solicitud.intIDSolicitud
                                               && (_detalleSolicitudViaje.intIDDetalleSolicitudViaje == intFKIDDetalleSolicitudViaje || intFKIDDetalleSolicitudViaje == 0)
                                               select new IDetalleSolicitudViaje
                                               {
                                                   intIDDetalleSolicitudViaje = (int)_detalleSolicitudViaje.intIDDetalleSolicitudViaje,
                                                   intFKIDSolicitud = (int)_detalleSolicitudViaje.intFKIDSolicitud,
                                                   intFKIDTipoVuelo = _detalleSolicitudViaje.intFKIDTipoVuelo,
                                                   chrClaveOrigen = _detalleSolicitudViaje.chrClaveOrigen,
                                                   chrClaveDestino = _detalleSolicitudViaje.chrClaveDestino,
                                                   dtdFechaSalidad = _detalleSolicitudViaje.dtdFechaSalidad,
                                                   dtdFechaRegreso = _detalleSolicitudViaje.dtdFechaRegreso,
                                                   chrPresupuesto = _detalleSolicitudViaje.chrPresupuesto,
                                                   chrComentarios = _detalleSolicitudViaje.chrComentarios,
                                                   intFKIDTipoViaje = _detalleSolicitudViaje.intFKIDTipoViaje,
                                                   chrCentroCostoViaje = _detalleSolicitudViaje.chrCentroCostoViaje,
                                                   chrAerolinea = _detalleSolicitudViaje.chrAerolinea,
                                                   chrMotivoViaje = _detalleSolicitudViaje.chrMotivoViaje
                                               }).ToList();



            for (int i = 0; i < solicitud.detalleSolicitudViaje.Count; i++)
            {
                var intIDDetalleSolicitudViaje = solicitud.detalleSolicitudViaje[i].intIDDetalleSolicitudViaje;
                solicitud.detalleSolicitudViaje[i].listaEmpleados = (from _empleadoViajeros in db.tblEmpleados
                                                                     join tblDetalleViajeros _detalleViajeros in db.tblDetalleViajeros on _empleadoViajeros.chrCodigoEmpleado equals _detalleViajeros.intFKIDEmpleado
                                                                     where _detalleViajeros.intFKIDDetalleSolicitudViaje == (int)intIDDetalleSolicitudViaje && _detalleViajeros.intDelete == intDelete

                                                                     select new IEmpleado
                                                                     {
                                                                         intIDEmpleado = _empleadoViajeros.intIDEmpleado,
                                                                         chrNombre = _empleadoViajeros.chrNombre,
                                                                         chrApellidoPaterno = _empleadoViajeros.chrApellidoPaterno,
                                                                         chrApellidoMaterno = _empleadoViajeros.chrApellidoMaterno,
                                                                         chrCodigoEmpleado = _empleadoViajeros.chrCodigoEmpleado,
                                                                         dtdFechaNacimiento = _empleadoViajeros.dtdFechaNacimiento,
                                                                         intIDDetalleViajeros = (int)_detalleViajeros.intIDDetalleViajes
                                                                     }).ToList();
            }

            solicitud.detalleSolicitudHotel = (from _detalleSolicitudHotel in db.tblDetalleSolicitudHotel
                                               where _detalleSolicitudHotel.intFKIDSolicitud == solicitud.intIDSolicitud
                                               && (_detalleSolicitudHotel.intIDDetalleSolicitudHotel == intFKIDDetalleSolicitudViaje || intFKIDDetalleSolicitudViaje == 0)
                                               select new IDetalleSolicitudHotel
                                               {
                                                   intIDDetalleSolicitudHotel = _detalleSolicitudHotel.intIDDetalleSolicitudHotel,
                                                   chrClaveDestino = _detalleSolicitudHotel.chrClaveDestino,
                                                   chrClaveOrigen = _detalleSolicitudHotel.chrClaveOrigen,
                                                   dtdFechaSalidad = (DateTime)_detalleSolicitudHotel.dtdFechaSalida,
                                                   dtdFechaRegreso = (DateTime)_detalleSolicitudHotel.dtdFechaRegreso,
                                                   chrPresuspuesto = (Decimal)_detalleSolicitudHotel.chrPresupuesto,
                                                   chrComentarios = _detalleSolicitudHotel.chrComentarios,
                                                   intFKIDTipoHabitacion = (int)_detalleSolicitudHotel.intFKIDTipoHabitacion,
                                                   chrEsHotel = _detalleSolicitudHotel.chrEsHotel,
                                                   intNoHabitaciones = (int)_detalleSolicitudHotel.intNoHabitaciones,
                                                   chrCentroCostoHospedaje = _detalleSolicitudHotel.chrCentroCostoHospedaje,
                                                   chrNochesReservadas = _detalleSolicitudHotel.chrNochesReservadas
                                               }).ToList();



            for (int j = 0; j < solicitud.detalleSolicitudHotel.Count; j++)
            {

                var intIDDetalleHotel = solicitud.detalleSolicitudHotel[j].intIDDetalleSolicitudHotel;
                solicitud.detalleSolicitudHotel[j].lstDtEmpleadoHuespedes = (from _empleado in db.tblEmpleados
                                                                             join tblDetalleHuespedes _huespedes in db.tblDetalleHuespedes on _empleado.chrCodigoEmpleado equals _huespedes.intFKIDEmpleado
                                                                             where _huespedes.intFKIDDetalleSolicitudHotel == intIDDetalleHotel && _huespedes.intDelete == intDelete
                                                                             select new IEmpleado
                                                                             {
                                                                                 intIDEmpleado = _empleado.intIDEmpleado,
                                                                                 chrNombre = _empleado.chrNombre,
                                                                                 chrApellidoPaterno = _empleado.chrApellidoPaterno,
                                                                                 chrApellidoMaterno = _empleado.chrApellidoMaterno,
                                                                                 chrCodigoEmpleado = _empleado.chrCodigoEmpleado,
                                                                                 dtdFechaNacimiento = (DateTime)_empleado.dtdFechaNacimiento,
                                                                                 intIDDetalleHuespedes = (int)_huespedes.intIDDetalleHuespedes

                                                                             }).ToList();


            }



            db.Dispose();

            return solicitud;

        }

        public int DeleteSolicitud(int intIDSolicitud)
        {

            int intDelete = 0;
            if (intIDSolicitud > 0)
            {

                var detalleSolicitud = (from solicitud in db.tblSolicitudes
                                        where solicitud.intIDSolicitud == intIDSolicitud
                                        select solicitud).FirstOrDefault();

                if (detalleSolicitud != null)
                {

                    detalleSolicitud.intDelete = 0;
                }

                var detalleSolicitudViaje = (from _detalleSolicitudViaje in db.tblDetalleSolicitudViaje
                                             where _detalleSolicitudViaje.intFKIDSolicitud == detalleSolicitud.intIDSolicitud
                                             select _detalleSolicitudViaje).FirstOrDefault();

                if (detalleSolicitudViaje != null)
                {
                    detalleSolicitudViaje.intDelete = 0;

                    var detalleViajeros = (from _detalleViajeros in db.tblDetalleViajeros
                                           where _detalleViajeros.intFKIDDetalleSolicitudViaje == detalleSolicitudViaje.intIDDetalleSolicitudViaje
                                           select _detalleViajeros).FirstOrDefault();

                    if (detalleViajeros != null)
                    {
                        detalleViajeros.intDelete = 0;
                    }
                }



                var detalleSolicitudHotel = (from _detalleSolicitudHotel in db.tblDetalleSolicitudHotel
                                             where _detalleSolicitudHotel.intFKIDSolicitud == detalleSolicitud.intIDSolicitud
                                             select _detalleSolicitudHotel).FirstOrDefault();
                if (detalleSolicitudHotel != null)
                {
                    detalleSolicitudHotel.intDelete = 0;

                    var detalleHuespedes = (from _detalleHuespedes in db.tblDetalleHuespedes
                                            where _detalleHuespedes.intFKIDDetalleSolicitudHotel == detalleSolicitudHotel.intIDDetalleSolicitudHotel
                                            select _detalleHuespedes).FirstOrDefault();

                    if (detalleHuespedes != null)
                    {
                        detalleHuespedes.intDelete = 0;

                    }
                }



                db.SaveChanges();

            }
            else
            {
                intDelete = 1;
            }

            return intDelete;
        }

        public List<IReporteSolicitudesCotizadas> ConsultaSolicitudesACotizar(int intIDFolio, DateTime? dtdFechaInicio, DateTime? dtdFechaFinal, int intFKIDDetalleSolicitudViaje)
        {
           
            int intDelete = Convert.ToSByte(true);
            List<IReporteSolicitudesCotizadas> lstReporteCotizadas = (from _solicitud in db.tblSolicitudes
                                                                      join tblEmpleados empleados in db.tblEmpleados on _solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                                                      join tblTipoSolicitud tipoSolicitud in db.tblTipoSolicitud
                                                                      on _solicitud.intFKIDTipoSolicitud equals (int)tipoSolicitud.intIDTipoSolicitud
                                                                      join tblEstatusSolicitud estatus in db.tblEstatusSolicitud
                                                                      on _solicitud.intFKIDEstatus equals (int)estatus.intIDEstatus

                                                                      join detalleViaje in db.tblDetalleSolicitudViaje on _solicitud.intIDSolicitud equals (int)detalleViaje.intFKIDSolicitud into FactDesc
                                                                      from fd in FactDesc.DefaultIfEmpty()

                                                                      join detalleHotel in db.tblDetalleSolicitudHotel on _solicitud.intIDSolicitud equals (int)detalleHotel.intFKIDSolicitud into DetallleHotelDesc
                                                                      from DH in DetallleHotelDesc.DefaultIfEmpty()
                                                                      where
                                                                      //_solicitud.intFKIDEstatus == 1
                                                                       _solicitud.intDelete == intDelete
                                                                       && (_solicitud.intIDSolicitud == intIDFolio || intIDFolio == 0) && (fd.intIDDetalleSolicitudViaje == intFKIDDetalleSolicitudViaje || DH.intIDDetalleSolicitudHotel == intFKIDDetalleSolicitudViaje || intFKIDDetalleSolicitudViaje == 0)
                                                                      //&& (string.IsNullOrEmpty(dateH1.ToString()) && ( DH.dtdFechaSalida >= dateH1) )
                                                                      //&& (string.IsNullOrEmpty(dateH2.ToString()) && ( DH.dtdFechaSalida <= dateH2))
                                                                     // group  _solicitud.intIDSolicitud by _solicitud.intIDSolicitud into s
                                                                      orderby DH.dtdFechaSalida, fd.dtdFechaSalidad ascending
                                                                      select new IReporteSolicitudesCotizadas
                                                                      {
                                                                          intIDSolicitud = (int)_solicitud.intIDSolicitud,
                                                                          chrNombreSolicitante = empleados.chrNombre,
                                                                          chrApellidoPaterno = empleados.chrApellidoPaterno,
                                                                          chrApellidoMaterno = empleados.chrApellidoMaterno,
                                                                          chrTipoSolicitud = tipoSolicitud.chrDescripcion,
                                                                          intNumCotizadas = (int)_solicitud.intNumCotizadas,
                                                                          dtdFechaSolicitud = _solicitud.dtdFechaSolicitud,
                                                                          intIDEstatus = (int)estatus.intIDEstatus,
                                                                          chrEstatus = estatus.chrEstatus,
                                                                          chrColorEstatus = estatus.chrColor,
                                                                          dtdFechaRegresoVuelo = fd.dtdFechaRegreso,
                                                                          dtdFechaSalidaVuelo = fd.dtdFechaSalidad,
                                                                          dtdFechaRegresoHospedaje = DH.dtdFechaRegreso,
                                                                          dtdFechaSalidaHospedaje = DH.dtdFechaSalida,
                                                                          intFKIDDetalleSolicitudViaje = (int)(fd.intIDDetalleSolicitudViaje != 0 ? fd.intIDDetalleSolicitudViaje : 0),
                                                                          intFKIDDetalleSolicitudHospedaje = (int)
 (DH.intIDDetalleSolicitudHotel != 0 ? DH.intIDDetalleSolicitudHotel : 0),
                                                                          intFKIDTipoSolicitud = _solicitud.intFKIDTipoSolicitud
                                                                      }).OrderBy(vuelo => new { vuelo.dtdFechaSalidaHospedaje, vuelo.dtdFechaSalidaVuelo }).ToList();






            return lstReporteCotizadas;

        }

        public bool cargaArchivos(ICargaArchivos item)
        {
            tblImagenesCotizar cargaImagenes;
            Boolean? esArchivo = false;
            bool exito = true;
            Boolean? chrEsCarta = false;


            if ((item.chrContentType == "text/xml" || item.chrContentType == "application/xml" || item.chrContentType == "application/pdf"))
            {
                esArchivo = true;
            }
            else
            {
                esArchivo = false;
            }

            if (item.intFKIDTipoSolicitud == 4)
            {
                chrEsCarta = true;
                cargaImagenes = (from item2 in db.tblImagenesCotizar
                                 where item2.intFKIDSolicitud == item.intFKIDSolicitud
                                 && item2.chrEsCarta == 1
                                 && item2.chrNombre == item.chrNombre 
                                 select item2).FirstOrDefault();
                if (cargaImagenes != null)
                {
                    item.intIDImagenesCotizar = cargaImagenes.intIDImagenesCotizar;
                    // cargaImagenes.intDelete = 0;
                    //db.SaveChanges();
                }

            }
            else
            {
                chrEsCarta = false;
            }

            sbyte sbCarta = Convert.ToSByte(chrEsCarta);
            sbyte sbArchivo = Convert.ToSByte(esArchivo);

            try
            {
                if (item != null)
                {
                    if (item.intIDImagenesCotizar <= 0)
                    {
                        cargaImagenes = new tblImagenesCotizar();
                        cargaImagenes.intIDImagenesCotizar = (int)item.intIDImagenesCotizar;
                        cargaImagenes.intFKIDServicio = item.intFKIDTipoSolicitud;
                        cargaImagenes.intFKIDSolicitud = item.intFKIDSolicitud;
                        cargaImagenes.chrNombre = item.chrNombre;
                        cargaImagenes.chrPath = item.chrPath;
                        cargaImagenes.intDelete = 1;
                        cargaImagenes.chrContentType = item.chrContentType;
                        cargaImagenes.chrEsArchivo = sbArchivo;
                        cargaImagenes.chrEsCarta = sbCarta;
                        cargaImagenes.intFKIDDetalleSolicitudViajeHotel = item.intFKIDDetalleSolicitudVueloHotel;
                        db.tblImagenesCotizar.Add(cargaImagenes);

                    }
                    else
                    {
                        cargaImagenes = (from imagen in db.tblImagenesCotizar
                                         where imagen.intIDImagenesCotizar == item.intIDImagenesCotizar
                                         select imagen).FirstOrDefault();
                        if (cargaImagenes != null)
                        {
                            cargaImagenes.intFKIDServicio = item.intFKIDTipoSolicitud;
                            cargaImagenes.intFKIDSolicitud = item.intFKIDSolicitud;
                            cargaImagenes.chrNombre = item.chrNombre;
                            cargaImagenes.chrPath = item.chrPath;
                            cargaImagenes.chrContentType = item.chrContentType;
                            cargaImagenes.chrEsArchivo = sbArchivo;
                            cargaImagenes.chrEsCarta = sbCarta;
                            cargaImagenes.intFKIDDetalleSolicitudViajeHotel = item.intFKIDDetalleSolicitudVueloHotel;
                        }
                    }

                    if (item.intFKIDSolicitud > 0)
                    {
                        var solicitud = (from _solicitud in db.tblSolicitudes
                                         where _solicitud.intIDSolicitud == item.intFKIDSolicitud
                                         select _solicitud).FirstOrDefault();
                        if (solicitud != null)
                        {
                            solicitud.dtdFechaCotizada = DateTime.Now;

                        }


                    }
                }


                db.SaveChanges();

            }
            catch (Exception ex)
            {
                exito = false;
            }

            return exito;
        }

        public int EliminaImagenes(int intIDImagen)
        {
            int intDelete = 0;
            string filename = String.Empty;
            string nombre = string.Empty;
            string directoryName = String.Empty;
            var path = HttpRuntime.AppDomainAppPath;

            if (intIDImagen > 0)
            {

                var imagen = (from _imagen in db.tblImagenesCotizar
                              where _imagen.intIDImagenesCotizar == intIDImagen
                              select _imagen).FirstOrDefault();
                if (imagen != null)
                {
                    imagen.intDelete = 0;
                    db.SaveChanges();
                    nombre = imagen.chrNombre;
                    directoryName = System.IO.Path.Combine(path, "Documentos");
                    filename = System.IO.Path.Combine(directoryName, nombre);

                    //Deletion exists file  
                    //if (File.Exists(filename))
                    //{
                    //    File.Delete(filename);
                    //}

                    intDelete = 0;
                }




            }
            else
            {
                intDelete = 1;
            }

            return intDelete;


        }

        public List<ICargaArchivos> ConsultaImagenes(int intIDSolicitud, int intFKIDDetalleSolicitudViajeHotel)
        {
            int intDelete = Convert.ToSByte(true);
            List<ICargaArchivos> lstArchivos = (from item in db.tblImagenesCotizar
                                                where item.intFKIDSolicitud == intIDSolicitud && (item.intFKIDDetalleSolicitudViajeHotel == intFKIDDetalleSolicitudViajeHotel  || intFKIDDetalleSolicitudViajeHotel == 0)
                                                && item.intDelete == intDelete && item.chrEsArchivo == 0

                                                select new ICargaArchivos
                                                {
                                                    intIDImagenesCotizar = item.intIDImagenesCotizar,
                                                    chrNombre = item.chrNombre,
                                                    chrPath = item.chrPath,
                                                    intFKIDSolicitud = item.intFKIDSolicitud,
                                                    intFKIDTipoSolicitud = item.intFKIDServicio,
                                                    intNoOpcionUsuario = item.intNoOpcionUsuario,
                                                    intNoOpcionAutorizador = item.intNoOpcionAutorizador,
                                                    intEsRechazado = item.chrEsRechazada,
                                                    intFKIDDetalleSolicitudVueloHotel = (int) item.intFKIDDetalleSolicitudViajeHotel
                                                }).ToList();


            db.Dispose();
            return lstArchivos;
        }

        public List<ICargaArchivos> ConsultaCarga(int intIDSolicitud)
        {
            int intDelete = Convert.ToSByte(true);
            List<ICargaArchivos> lstArchivos = (from item in db.tblImagenesCotizar
                                                where item.intFKIDSolicitud == intIDSolicitud
                                                && item.intDelete == intDelete && item.chrEsCarta == 1
                                                select new ICargaArchivos
                                                {
                                                    intIDImagenesCotizar = item.intIDImagenesCotizar,
                                                    chrNombre = item.chrNombre,
                                                    chrPath = item.chrPath,
                                                    intFKIDSolicitud = item.intFKIDSolicitud,
                                                    intFKIDTipoSolicitud = item.intFKIDServicio,
                                                    intNoOpcionUsuario = item.intNoOpcionUsuario,
                                                    intNoOpcionAutorizador = item.intNoOpcionAutorizador,
                                                    intEsRechazado = item.chrEsRechazada
                                                }).ToList();


            db.Dispose();
            return lstArchivos;
        }

        public List<ICargaArchivos> ConsultaArchivos(int intIDSolicitud)
        {
            int intDelete = Convert.ToSByte(true);
            List<ICargaArchivos> lstArchivos = (from item in db.tblImagenesCotizar
                                                where item.intFKIDSolicitud == intIDSolicitud
                                                && item.intDelete == intDelete && item.chrEsArchivo == 1
                                                join tipoSolicitud in db.tblTipoSolicitud on item.intFKIDServicio equals (int)tipoSolicitud.intIDTipoSolicitud into tiposolicitud
                                                from TS in tiposolicitud.DefaultIfEmpty()
                                                select new ICargaArchivos
                                                {
                                                    intIDImagenesCotizar = item.intIDImagenesCotizar,
                                                    chrNombre = item.chrNombre,
                                                    chrPath = item.chrPath,
                                                    intFKIDSolicitud = item.intFKIDSolicitud,
                                                    intFKIDTipoSolicitud = item.intFKIDServicio,
                                                    intNoOpcionUsuario = item.intNoOpcionUsuario,
                                                    intNoOpcionAutorizador = item.intNoOpcionAutorizador,
                                                    intEsRechazado = item.chrEsRechazada,
                                                    chrContentType = item.chrContentType,
                                                    chrTipoArchivo = TS.chrDescripcion
                                                }).ToList();


            db.Dispose();
            return lstArchivos;
        }

        public bool PrioridadCotizacionesUsuario(List<ICargaArchivos> lista)
        {



            if (lista != null)
            {
                try
                {
                    foreach (ICargaArchivos item in lista)
                    {
                        var cargaarchivos = (from _imagnes in db.tblImagenesCotizar
                                             where _imagnes.intIDImagenesCotizar == item.intIDImagenesCotizar
                                             select _imagnes).FirstOrDefault();

                        if (cargaarchivos != null)
                        {
                            cargaarchivos.intNoOpcionUsuario = item.intNoOpcionUsuario;
                            db.SaveChanges();
                        }

                    }


                }
                catch (Exception e)
                {
                    e.ToString();
                    return false;
                }

            }

            return true;
        }

        public bool PrioridadCotizacionesAutorizador(List<ICargaArchivos> lista)
        {
            int intIDSolicitud = 0;
            if (lista != null)
            {
                try
                {
                    foreach (ICargaArchivos item in lista)
                    {
                        var cargaarchivos = (from _imagnes in db.tblImagenesCotizar
                                             where _imagnes.intIDImagenesCotizar == item.intIDImagenesCotizar
                                             select _imagnes).FirstOrDefault();

                        if (cargaarchivos != null)
                        {
                            cargaarchivos.intNoOpcionAutorizador = item.intNoOpcionUsuario;

                            db.SaveChanges();
                        }
                        intIDSolicitud = (int)cargaarchivos.intFKIDSolicitud;

                    }
                    var solicitud = (from item in db.tblSolicitudes
                                     where item.intIDSolicitud == intIDSolicitud
                                     select item).FirstOrDefault();
                    if (solicitud != null)
                    {
                        solicitud.dtdFechaAutorizada = DateTime.Now;
                        db.SaveChanges();
                    }


                }
                catch (Exception e)
                {
                    e.ToString();
                    return false;
                }

            }

            return true;
        }

        public List<IReporteSolicitudAutorizados> ConsultaSolicitudesAutorizar(string intIDAutorizador, int? intFKIDEstatus, int? intFKIDTipoSolicitud, string intFKIDSolicitante, int intFKIDDetalleSolicitudVueloHotel)
        {

            int intDelete = Convert.ToSByte(true);
            List<IReporteSolicitudAutorizados> lstAutorizados = (from _solicitud in db.tblSolicitudes
                                                                 join tblEmpleados empleados in db.tblEmpleados
                                                                 on _solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                                                 join tblTipoSolicitud _tipoSolicitud in db.tblTipoSolicitud on
                                                                 _solicitud.intFKIDTipoSolicitud equals (int)_tipoSolicitud.intIDTipoSolicitud
                                                                 join tblEstatusSolicitud _estatus in db.tblEstatusSolicitud
                                                                 on _solicitud.intFKIDEstatus equals (int)_estatus.intIDEstatus
                                                                 join detalleViaje in db.tblDetalleSolicitudViaje on _solicitud.intIDSolicitud equals (int)detalleViaje.intFKIDSolicitud into DetalleViaje
                                                                 from DV in DetalleViaje.DefaultIfEmpty()
                                                                 join detalleHotel in db.tblDetalleSolicitudHotel on _solicitud.intIDSolicitud equals (int)detalleHotel.intFKIDSolicitud into DetallleHotelDesc
                                                                 from DH in DetallleHotelDesc.DefaultIfEmpty()
                                                                 where _solicitud.intDelete == intDelete
                                                                 // && (_solicitud.dtdFechaSolicitud == listaAutorizadas.dtdFechaEnvioHora) 
                                                                 && (_solicitud.intIDAutorizadorSuplente == intIDAutorizador || empleados.intFKIDEmpleadoAutorizador == intIDAutorizador)
                                                                 && (DH.intIDDetalleSolicitudHotel == intFKIDDetalleSolicitudVueloHotel || DV.intIDDetalleSolicitudViaje == intFKIDDetalleSolicitudVueloHotel || intFKIDDetalleSolicitudVueloHotel == 0)
                                                                 && (_solicitud.intFKIDEstatus == intFKIDEstatus || intFKIDEstatus == 0)
                                                                 && (_solicitud.intFKIDTipoSolicitud == intFKIDTipoSolicitud || intFKIDTipoSolicitud == 0)
                                                                 && (_solicitud.intNoSolicitante == intFKIDSolicitante || intFKIDSolicitante != null)
                                                                 orderby _solicitud.intFKIDEstatus descending
                                                                 select new IReporteSolicitudAutorizados
                                                                 {
                                                                     intIDSolicitud = (int)_solicitud.intIDSolicitud,
                                                                     dtdFechaEnvioHora = _solicitud.dtdFechaSolicitud,
                                                                     dtdFechaCotizacion = _solicitud.dtdFechaCotizada,
                                                                     dtdFechaAutorizacion = _solicitud.dtdFechaAutorizada,
                                                                     intNumSolicitud = _solicitud.intFolio,
                                                                     chrNombreSolicitante = empleados.chrNombre,
                                                                     chrApellidoPaternoSolicitante = empleados.chrApellidoPaterno,
                                                                     chrApellidoMaternoSolicitante = empleados.chrApellidoMaterno,
                                                                     chrTipoSolicitud = _tipoSolicitud.chrDescripcion,
                                                                     chrMotivoViaje = DV.chrMotivoViaje,
                                                                     chrMotivoHotel = DH.chrComentarios,
                                                                     chrEstatus = _estatus.chrEstatus,
                                                                     intNoCotizaciones = _solicitud.intNumCotizadas,
                                                                     intFKIDDetalleSolicitudHotel = (int)(DH.intIDDetalleSolicitudHotel != 0 ? DH.intIDDetalleSolicitudHotel : 0),
                                                                     intFKIDDetalleSolicitudViaje = (int)(DV.intIDDetalleSolicitudViaje != 0 ? DV.intIDDetalleSolicitudViaje : 0),
                                                                     intFKIDTipoSolicitud = _solicitud.intFKIDTipoSolicitud
                                                                 }).ToList();


            return lstAutorizados;
        }

        public List<INotificacionesUsuarios> NotificacionesSolicitudes(string intCodigoEmpleado)
        {
            int intDelete = Convert.ToSByte(true);
            var condicion = string.Empty;
            List<INotificacionesUsuarios> listSolcitud;
            var empleado = (from _empleado in db.tblEmpleados
                            where _empleado.intIDEmpleado == intCodigoEmpleado
                            select _empleado).FirstOrDefault();

            if (empleado.intIDPerfil == 1)
            {
                listSolcitud = (from _solicitudes in db.tblSolicitudes
                                join _empledo in db.tblEmpleados on _solicitudes.intNoSolicitante equals _empledo.intIDEmpleado
                                join _estatus in db.tblEstatusSolicitud on (int)_solicitudes.intFKIDEstatus equals _estatus.intIDEstatus
                                where _solicitudes.intDelete == intDelete && (_solicitudes.intVisto == null || _solicitudes.intVisto == 0) && _solicitudes.intNoSolicitante == intCodigoEmpleado
                                select new INotificacionesUsuarios
                                {
                                    intIDSolicitud = (int)_solicitudes.intIDSolicitud,
                                    chrNombre = _empledo.chrNombre,
                                    chrApellidPaterno = _empledo.chrApellidoPaterno,
                                    chrApellidoMaterno = _empledo.chrApellidoMaterno,
                                    dtdFechaSolicitud = _solicitudes.dtdFechaSolicitud,
                                    intFKIdEstatus = (int)_estatus.intIDEstatus,
                                    chrEstatus = _estatus.chrEstatus,
                                    chrColor = _estatus.chrColor
                                }).ToList();


            }
            else if (empleado.intIDPerfil == 3)
            {
                listSolcitud = (from _solicitudes in db.tblSolicitudes
                                join _empledo in db.tblEmpleados on _solicitudes.intNoSolicitante equals _empledo.intIDEmpleado
                                join _estatus in db.tblEstatusSolicitud on (int)_solicitudes.intFKIDEstatus equals _estatus.intIDEstatus
                                where _solicitudes.intDelete == intDelete && (_solicitudes.intVisto == null || _solicitudes.intVisto == 0) && (_solicitudes.intIDAutorizadorSuplente == intCodigoEmpleado || _empledo.intFKIDEmpleadoAutorizador == intCodigoEmpleado)
                                select new INotificacionesUsuarios
                                {
                                    intIDSolicitud = (int)_solicitudes.intIDSolicitud,
                                    chrNombre = _empledo.chrNombre,
                                    chrApellidPaterno = _empledo.chrApellidoPaterno,
                                    chrApellidoMaterno = _empledo.chrApellidoMaterno,
                                    dtdFechaSolicitud = _solicitudes.dtdFechaSolicitud,
                                    intFKIdEstatus = (int)_estatus.intIDEstatus,
                                    chrEstatus = _estatus.chrEstatus,
                                    chrColor = _estatus.chrColor
                                }).ToList();


            }
            else
            {
                listSolcitud = (from _solicitudes in db.tblSolicitudes
                                join _empledo in db.tblEmpleados on _solicitudes.intNoSolicitante equals _empledo.intIDEmpleado
                                join _estatus in db.tblEstatusSolicitud on (int)_solicitudes.intFKIDEstatus equals _estatus.intIDEstatus
                                where _solicitudes.intDelete == intDelete && (_solicitudes.intVisto == null || _solicitudes.intVisto == 0)
                                select new INotificacionesUsuarios
                                {
                                    intIDSolicitud = (int)_solicitudes.intIDSolicitud,
                                    chrNombre = _empledo.chrNombre,
                                    chrApellidPaterno = _empledo.chrApellidoPaterno,
                                    chrApellidoMaterno = _empledo.chrApellidoMaterno,
                                    dtdFechaSolicitud = _solicitudes.dtdFechaSolicitud,
                                    intFKIdEstatus = (int)_estatus.intIDEstatus,
                                    chrEstatus = _estatus.chrEstatus,
                                    chrColor = _estatus.chrColor
                                }).ToList();
            }

            if (empleado != null)
            {


            }


            return listSolcitud;
        }

        public int MarcarSolicitudVisto(int intIDSolicitud)
        {
            int intVisto = 0;
            var solicitud = (from _solicitud in db.tblSolicitudes
                             where _solicitud.intIDSolicitud == intIDSolicitud
                             select _solicitud).FirstOrDefault();

            if (solicitud != null)
            {
                solicitud.intVisto = 1;
                db.SaveChanges();

            }
            else
            {
                intVisto = 1;

            }

            return intVisto;

        }

        public int CerrarSolicitud(int intIDSolicitud)
        {
            NotificacionesManager notificaciones = new NotificacionesManager();
            var intChandeStatus = 0;
            int intFKIDEstatusCerrado = Convert.ToInt32(WebConfigurationManager.AppSettings["intFKIDEstatusCerrado"]);

            var solicitud = (from _solicitud in db.tblSolicitudes
                             where _solicitud.intIDSolicitud == intIDSolicitud
                             select _solicitud).FirstOrDefault();

            if (solicitud != null)
            {
                solicitud.intFKIDEstatus = intFKIDEstatusCerrado;
                db.SaveChanges();
                notificaciones.EnviaCorreoUsuario((int)solicitud.intIDSolicitud);
            }
            else
            {
                intChandeStatus = 1;
            }

            return intChandeStatus;

        }

        public List<IOperacionesSolicitudAvion> ConsultaOperacionAvion(int intIDSolciitud)
        {
            int intDelete = Convert.ToSByte(true);
            List<IOperacionesSolicitudAvion> consultaOperacionAvion = null;

            var detalleSolicitud = (from _detalleAvion in db.tblDetalleSolicitudViaje
                                    where _detalleAvion.intFKIDSolicitud == intIDSolciitud
                                    select _detalleAvion).FirstOrDefault();


            if (detalleSolicitud != null)
            {
                consultaOperacionAvion = (from _operacionAvion in db.tblOperacionesSolicitudAvion
                                          where _operacionAvion.intFKDetalleSolicitudViaje == detalleSolicitud.intIDDetalleSolicitudViaje
                                          where _operacionAvion.intDelete == intDelete
                                          select new IOperacionesSolicitudAvion
                                          {
                                              intIDOperacionesSolicitudesAvion = _operacionAvion.intIDOperacionesSolicitudesAvion,
                                              intFKDetalleSolicitudViaje = (int)_operacionAvion.intFKDetalleSolicitudViaje,
                                              intNoOpcion = (int)_operacionAvion.intNoOpcion,
                                              chrClaveReservacion = _operacionAvion.chrClaveReservacion,
                                              dtdFechaRespuesta = (DateTime)_operacionAvion.dtdFechaRespuesta,
                                              chrLineaArea = _operacionAvion.chrLineaArea,
                                              intFKIDTipoVuelo = (int)_operacionAvion.intFKIDTipoVuelo,
                                              chrClaveOrigen = _operacionAvion.chrClaveOrigen,
                                              chrClaveDestino = _operacionAvion.chrClaveDestino,
                                              dtdFechaSalida = (DateTime)_operacionAvion.dtdFechaSalida,
                                              dtdFechaRegreso = (DateTime)_operacionAvion.dtdFechaRegreso,
                                              decCosto = _operacionAvion.decCosto,
                                              chrAceptada = _operacionAvion.chrAceptada,
                                              chrAutorizada = _operacionAvion.chrAutorizada,
                                              chrReservada = _operacionAvion.chrReservada,
                                              chrRFC = _operacionAvion.chrRFC,
                                              chrCostoExtraAvion = _operacionAvion.decCostoExtraAvion
                                          }).ToList();


            }


            return consultaOperacionAvion;
        }

        public List<IEscalasVuelo> ConsultaEscalasAvion(int intIDOperacionesSolicitudAvion)
        {
            int intDelete = Convert.ToSByte(true);
            List<IEscalasVuelo> consultaEscalas = (from _escalas in db.tblOperacionesSolicitudEscalasAvion
                                                   where _escalas.intFKIDOperacionesSolicitudAvion == intIDOperacionesSolicitudAvion
                                                   && _escalas.intDetele == intDelete
                                                   select new IEscalasVuelo
                                                   {
                                                       intIDOperacionesSolicitudEscalaAvion = _escalas.intIDOperacionesSolicitudEscalaAvion,
                                                       intFKIDOperacionesSolicitudAvion = _escalas.intFKIDOperacionesSolicitudAvion,
                                                       chrClaveOrigenEscala = _escalas.chrClaveOrigenEscala,
                                                       chrClaveDestinoEscala = _escalas.chrClaveDestinoEscala,
                                                       dtdFechaSalidaEscala = _escalas.dtdFechaSalidaEscala,

                                                   }).ToList();

            return consultaEscalas;

        }

        public IReservacionViaje ConsultaReservacionViaje(int intIDOperacionesSolicitudesAvion)
        {
            int intDelete = Convert.ToSByte(true);
            IReservacionViaje reservacionViaje = (from _reservacionHotel in db.tblReservacionesViaje
                                                  where _reservacionHotel.intFKIDOperacionesSolicitudesAvion == (int)intIDOperacionesSolicitudesAvion && _reservacionHotel.intDelete == intDelete
                                                  select new IReservacionViaje
                                                  {
                                                      intIDReservacionViaje = _reservacionHotel.intIDReservacionViaje,
                                                      intFKIDOperacionesSolicitudesAvion = _reservacionHotel.intFKIDOperacionesSolicitudesAvion,
                                                      intNoOpcion = _reservacionHotel.intNoOpcion,
                                                      chrContabilizada = _reservacionHotel.chrContabilizada,
                                                      chrTipoCompra = _reservacionHotel.chrTipoCompra,
                                                      chrCodigoReservacion = _reservacionHotel.chrCodigoReservacion,

                                                  }).FirstOrDefault();

            return reservacionViaje;
        }

        public List<IOperacionesSolicitudHotel> ConsultaOperacionHotel(int intIDSolicitud)
        {
            int intDelete = Convert.ToSByte(true);

            List<IOperacionesSolicitudHotel> operacionesHotel = null;
            var detalleHotel = (from _detalleHotel in db.tblDetalleSolicitudHotel
                                where _detalleHotel.intFKIDSolicitud == intIDSolicitud
                                select _detalleHotel).FirstOrDefault();
            if (detalleHotel != null)
            {
                operacionesHotel = (from _detalleOperacionesHotel in db.tblOperacionesSolicitudHotel
                                    where _detalleOperacionesHotel.intDelete == intDelete &&
                                    _detalleOperacionesHotel.intFKIDDetalleSolicitudHotel == detalleHotel.intIDDetalleSolicitudHotel
                                    select new IOperacionesSolicitudHotel
                                    {
                                        intIDOperacionesSolicitudHotel = _detalleOperacionesHotel.intIDOperacionesSolicitudHotel,
                                        intFKIDDetalleSolicitudHotel = _detalleOperacionesHotel.intFKIDDetalleSolicitudHotel,
                                        intNoOpcion = _detalleOperacionesHotel.intNoOpcion,
                                        chrCodigoConfirmacion = _detalleOperacionesHotel.chrCodigoConfirmacion,
                                        dtdFechaRespuesta = _detalleOperacionesHotel.dtdFechaRespuesta,
                                        chrLineaHotel = _detalleOperacionesHotel.chrLineaHotel,
                                        dtdFechaEntrada = _detalleOperacionesHotel.dtdFechaEntrada,
                                        dtdFechaSalidad = _detalleOperacionesHotel.dtdFechaSalidad,
                                        intNoHabitaciones = _detalleOperacionesHotel.intNoHabitaciones,
                                        intFKIDTipoHabitacion = _detalleOperacionesHotel.intFKIDTipoHabitacion,
                                        decCosto = _detalleOperacionesHotel.decCosto,
                                        intAceptada = _detalleOperacionesHotel.intAceptada,
                                        intAutorizada = _detalleOperacionesHotel.intAutorizada,
                                        intReservada = _detalleOperacionesHotel.intReservada,
                                        chrRFC = _detalleOperacionesHotel.chrRFC,
                                        chrCostoExtraHotel = _detalleOperacionesHotel.decCostoExtraHotel
                                    }).ToList();


            }
            return operacionesHotel;


        }

        public IReservacionHotel ConsultaReservacionHotel(int intIDOperacionesSolicitudesHotel)
        {
            int intDelete = Convert.ToSByte(true);
            IReservacionHotel reservacionesHotel = (from _reservacionesHotel in db.tblReservacionHotel
                                                    where _reservacionesHotel.intFKIDOperacionesSolicitudHotel == intIDOperacionesSolicitudesHotel && _reservacionesHotel.intDelete == intDelete
                                                    select new IReservacionHotel
                                                    {
                                                        intIDReservacionHotel = _reservacionesHotel.intIDReservacionHotel,
                                                        intFKIDOperacionesSolicitudHotel = _reservacionesHotel.intFKIDOperacionesSolicitudHotel,
                                                        intNoOpcion = _reservacionesHotel.intNoOpcion,
                                                        chrContabilizada = _reservacionesHotel.chrContabilizada,
                                                        chrTipoCompra = _reservacionesHotel.chrTipoCompra,
                                                        chrCodigoReservacion = _reservacionesHotel.chrCodigoReservacion,


                                                    }).FirstOrDefault();

            return reservacionesHotel;

        }

        public List<IEmpleado> ConsultaEmpleadosSolicitud(int intIDSolicitud)
        {

            List<IEmpleado> listaEmpleados;

            var solicitud = (from _solicitud in db.tblSolicitudes
                             where _solicitud.intIDSolicitud == intIDSolicitud
                             select _solicitud).FirstOrDefault();


            if (solicitud.intFKIDTipoSolicitud == 1 || solicitud.intFKIDTipoSolicitud == 2) // vuelo  y vuelo mas hospedaje
            {
                var detalleViaje = (from _detalleViaje in db.tblDetalleSolicitudViaje
                                    where _detalleViaje.intFKIDSolicitud == solicitud.intIDSolicitud
                                    select _detalleViaje).FirstOrDefault();

                listaEmpleados = (from _empleados in db.tblEmpleados
                                  join tblDetalleViajeros detalleviajeros in db.tblDetalleViajeros
                                  on _empleados.intIDEmpleado equals detalleviajeros.intFKIDEmpleado
                                  where detalleviajeros.intFKIDDetalleSolicitudViaje == detalleViaje.intIDDetalleSolicitudViaje
                                  select new IEmpleado
                                  {
                                      intIDEmpleado = _empleados.intIDEmpleado,
                                      chrNombre = _empleados.chrNombre,
                                      chrApellidoPaterno = _empleados.chrApellidoPaterno,
                                      chrApellidoMaterno = _empleados.chrApellidoMaterno,
                                      chrCorreo = _empleados.chrCorreo,
                                      chrCentroCosto = _empleados.chrCentroCosto,
                                      chkEliminar = detalleviajeros.intDelete
                                  }).ToList();



            }
            else
            {
                var detallehotel = (from _detallehotel in db.tblDetalleSolicitudHotel
                                    where _detallehotel.intFKIDSolicitud == solicitud.intIDSolicitud
                                    select _detallehotel).FirstOrDefault();
                listaEmpleados = (from _empleados in db.tblEmpleados
                                  join tblDetalleHuespedes detalleHotel in db.tblDetalleHuespedes
                                  on _empleados.intIDEmpleado equals detalleHotel.intFKIDEmpleado
                                  where detalleHotel.intFKIDDetalleSolicitudHotel == detallehotel.intIDDetalleSolicitudHotel
                                  select new IEmpleado
                                  {
                                      intIDEmpleado = _empleados.intIDEmpleado,
                                      chrNombre = _empleados.chrNombre,
                                      chrApellidoPaterno = _empleados.chrApellidoPaterno,
                                      chrApellidoMaterno = _empleados.chrApellidoMaterno,
                                      chrCorreo = _empleados.chrCorreo,
                                      chrCentroCosto = _empleados.chrCentroCosto,
                                      chkEliminar = detalleHotel.intDelete
                                  }).ToList();


            }

            return listaEmpleados;



        }



    }
}