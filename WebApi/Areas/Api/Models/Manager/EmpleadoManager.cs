﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using WebApi.Areas.Api.Controllers;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Models;
using WebApi.Utils;

namespace WebApi.Areas.Api.Models.Manager
{
    public class EmpleadoManager
    {
        private GS_CCCONTROL_VIAJESEntities db;
        private sfseguridadEntities dbSeguridad;
        GS_MAIL enviarCorreo = new GS_MAIL();

        UtilsGS utils = new UtilsGS();
        public EmpleadoManager()
        {
            db = new GS_CCCONTROL_VIAJESEntities();
            dbSeguridad = new sfseguridadEntities();
        }

        public List<IPantallaUsuario> GetMenuUsuario(string chrSID)
        {
            string strConexion = ConfigurationManager.ConnectionStrings["sfSeguridad"].ConnectionString;
            MySqlConnection conn = new MySqlConnection(strConexion);
            MySqlDataAdapter proLoginDataAdapter;
            DataSet dsProcPermisosSesion = new DataSet();


            try
            {
                conn.Open();

                string sql = "CALL procPermisosSesion('" + chrSID + "')";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                proLoginDataAdapter = new MySqlDataAdapter(cmd);
                proLoginDataAdapter.Fill(dsProcPermisosSesion);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }

            List<IPantallaUsuario> pantallasUsuario = null;
            if (dsProcPermisosSesion.Tables.Count >= 2)
            {
                pantallasUsuario = new List<IPantallaUsuario>();
                DataTable tblMenus = dsProcPermisosSesion.Tables[1];
                IPantallaUsuario pantalla;
                string chrIDSistema = GetIdSistema();
                for (int i = 0; i < tblMenus.Rows.Count; i++)
                {
                    string idSistema = tblMenus.Rows[i]["idSistema"].ToString();
                    if (idSistema.Equals(chrIDSistema))
                    {
                        pantalla = new IPantallaUsuario();
                        pantalla.intIDSistema = idSistema;
                        pantalla.intIDModulo = tblMenus.Rows[i]["idModulo"].ToString();
                        pantalla.intIDSistema = tblMenus.Rows[i]["idSistema"].ToString();
                        pantalla.chrDescripcionModulo = tblMenus.Rows[i]["descripcionModulo"].ToString();
                        pantalla.intIDModuloPadre = tblMenus.Rows[i]["idModuloPadre"].ToString();
                        pantalla.chrUrl = tblMenus.Rows[i]["URL"].ToString();
                        pantalla.intDelete = 1;
                        pantalla.chrImagenIcono = tblMenus.Rows[i]["imagenIcono"].ToString();
                        pantalla.chrValue = tblMenus.Rows[i]["Value"].ToString();
                        pantallasUsuario.Add(pantalla);
                    }
                }

            }

            return pantallasUsuario;

        }

        public IProloginResult GetProloginInfo(string chrCodigoEmpleado, string chrPassword)
        {
            string strConexion = ConfigurationManager.ConnectionStrings["sfSeguridad"].ConnectionString;
            MySqlConnection conn = new MySqlConnection(strConexion);
            MySqlDataAdapter proLoginDataAdapter;
            DataSet dsProLogin = new DataSet();
            try
            {
                conn.Open();

                string sql = "CALL procLogin('" + chrCodigoEmpleado + "', '" + chrPassword + "')";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                proLoginDataAdapter = new MySqlDataAdapter(cmd);
                proLoginDataAdapter.Fill(dsProLogin);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }

            IProloginResult result = null;

            IntegrationWebServicesController integrationWebService = new IntegrationWebServicesController();


            if (dsProLogin.Tables.Count >= 3)
            {
                result = new IProloginResult();
                DataTable tblAutorizacion = dsProLogin.Tables[0];
                DataTable tblPrivilegio = dsProLogin.Tables[1];
                DataTable tblUsuario = dsProLogin.Tables[2];

                result.autorizacion = tblAutorizacion.Rows[0]["autorizacion"].ToString();
                result.SID = tblAutorizacion.Rows[0]["SID"].ToString();
                result.privilegio = tblPrivilegio.Rows[0]["Privilegio"].ToString();
                result.idUsuario = tblUsuario.Rows[0]["idUsuario"].ToString();
                result.contrasena = tblUsuario.Rows[0]["contrasena"].ToString();
                result.iDTipoUsuario = tblUsuario.Rows[0]["iDTipoUsuario"].ToString();
                result.correoElectronico = tblUsuario.Rows[0]["correoElectronico"].ToString();
                result.activo = tblUsuario.Rows[0]["activo"].ToString();
                result.fechaRegistro = (DateTime)tblUsuario.Rows[0]["fechaRegistro"];
                result.privilegio = tblUsuario.Rows[0]["privilegio"].ToString();
                result.nombreUsuario = tblUsuario.Rows[0]["nombreUsuario"].ToString();
                result.noEmpleado = tblUsuario.Rows[0]["noEmpleado"].ToString();
                result.IngSesion = tblUsuario.Rows[0]["IngSesion"].ToString();
                result.Lectura = tblUsuario.Rows[0]["Lectura"].ToString();
                result.idUsuarioAutoriza = tblUsuario.Rows[0]["idUsuarioAutoriza"].ToString();
                result.CentroCostos = tblUsuario.Rows[0]["CentroCostos"].ToString();
                result.Puesto = tblUsuario.Rows[0]["Puesto"].ToString();
                result.Descripcion = tblUsuario.Rows[0]["Descripcion"].ToString();

            }

            return result;

        }
        public IEmpleado Login(ILogin infoLogin)
        {

            IEmpleado empleado = null;
            tblEmpleados empleados;

            IProloginResult proLoginResult = GetProloginInfo(infoLogin.ChrCodigoEmpleado.ToString(), infoLogin.ChrPassword);

            List<IPantallaUsuario> pantallasMenu = GetMenuUsuario(proLoginResult.SID);

            var perfil = (from item in db.tblPerfiles
                          where item.chrClave == proLoginResult.privilegio
                          select item).FirstOrDefault();
            int intIDPerfilUser = (int)perfil.intIDPerfil;

            InsertaPantalla(pantallasMenu, proLoginResult.noEmpleado, intIDPerfilUser);

            if (proLoginResult != null && proLoginResult.autorizacion.Equals("AUTORIZADO") && !proLoginResult.SID.Equals(""))
            {


                var empleadogs = (from _empleado in db.tblEmpleados
                                  where _empleado.chrCodigoEmpleado == proLoginResult.noEmpleado
                                  select _empleado).FirstOrDefault();

                if (empleadogs == null)
                {
                    empleados = new tblEmpleados();
                    empleados.intIDEmpleado = proLoginResult.noEmpleado;
                    empleados.chrCodigoEmpleado = proLoginResult.noEmpleado;
                    empleados.chrEstatus = Convert.ToString(proLoginResult.activo);
                    empleados.chrCentroCosto = proLoginResult.CentroCostos;
                    empleados.chrNombre = proLoginResult.nombreUsuario;
                    empleados.chrCorreo = proLoginResult.correoElectronico;
                    empleados.chrPuesto = proLoginResult.Puesto;
                    empleados.intIDPerfil = Convert.ToInt32(intIDPerfilUser);
                    empleados.chrPassword = proLoginResult.contrasena;
                    empleados.dtdFechaAlta = DateTime.Now;
                    empleados.intFKIDEmpleadoAutorizador = proLoginResult.idUsuarioAutoriza;
                    db.tblEmpleados.Add(empleados);


                }
                else
                {
                    empleados = (from item in db.tblEmpleados
                                 where item.chrCodigoEmpleado == proLoginResult.noEmpleado
                                 select item).FirstOrDefault();

                    if (empleados.intIDEmpleado != null)
                    {
                        empleados.chrCodigoEmpleado = proLoginResult.noEmpleado;
                        empleados.chrEstatus = Convert.ToString(proLoginResult.activo);
                        empleados.chrCentroCosto = proLoginResult.CentroCostos;
                        empleados.chrNombre = proLoginResult.nombreUsuario;
                        empleados.chrCorreo = proLoginResult.correoElectronico;
                        empleados.chrPuesto = proLoginResult.Puesto;
                        empleados.intIDPerfil = Convert.ToInt32(intIDPerfilUser);
                        empleados.chrPassword = proLoginResult.contrasena;
                        empleados.dtdFechaAlta = DateTime.Now;
                        empleados.intFKIDEmpleadoAutorizador = proLoginResult.idUsuarioAutoriza;
                    }

                }
                db.SaveChanges();


                empleado = (from item in db.tblEmpleados
                            where item.chrCodigoEmpleado == empleados.chrCodigoEmpleado

                            select new IEmpleado
                            {
                                intIDEmpleado = item.intIDEmpleado,
                                chrCodigoEmpleado = item.chrCodigoEmpleado,
                                chrNombre = item.chrNombre,
                                chrApellidoPaterno = item.chrApellidoPaterno,
                                chrApellidoMaterno = item.chrApellidoMaterno,
                                chrCorreo = item.chrCorreo,
                                chrEstatus = item.chrEstatus,
                                intIDPerfil = item.intIDPerfil,
                                chrSociedad = item.chrSociedad,
                                chrPuesto = proLoginResult.Puesto,
                                chrDepartamento = item.chrDepartamento,
                                chrCentroCosto = item.chrCentroCosto,
                                chrPasaporte = item.chrPasaporte,
                                intFKIDEmpleadoAutorizador = item.intFKIDEmpleadoAutorizador

                            }).FirstOrDefault();

                empleado.perfilEmpleado = (from _perfil in db.tblPerfiles
                                           where _perfil.intIDPerfil == empleado.intIDPerfil
                                           select new IPerfil
                                           {
                                               intIDPerfil = (int)_perfil.intIDPerfil,
                                               chrDescripcion = _perfil.chrDescripcion
                                           }).ToList();

                empleado.empledoAutorizador = (from _empleadoAutorizador in db.tblEmpleados
                                               where _empleadoAutorizador.intIDEmpleado == empleado.intFKIDEmpleadoAutorizador
                                               select new IEmpleado
                                               {
                                                   intIDEmpleado = _empleadoAutorizador.intIDEmpleado,
                                                   chrNombre = _empleadoAutorizador.chrNombre,
                                                   chrApellidoPaterno = _empleadoAutorizador.chrApellidoPaterno,
                                                   chrApellidoMaterno = _empleadoAutorizador.chrApellidoMaterno,
                                                   chrDepartamento = _empleadoAutorizador.chrDepartamento
                                               }).ToList();
                empleado.pantallasUsuarios = (from item in db.tblPantallas
                                              where item.intFKIDPerfil == empleado.intIDPerfil
                                              && item.chrCodigoEmpleado == empleado.chrCodigoEmpleado
                                              && item.chrMenuPrincipal == "SI"
                                              select new IPantallaUsuario
                                              {
                                                  intIDPantallas = item.intIDPantallas,
                                                  intIDModulo = item.intIDModulo,
                                                  intIDSistema = item.intIDSistema,
                                                  chrDescripcionModulo = item.chrDescripcionModulo,
                                                  chrUrl = item.chrUrl + "?SID=" + proLoginResult.SID + "&L=" + proLoginResult.Lectura,
                                                  chrMenuPrincipal = item.chrMenuPrincipal,
                                                  intIDModuloPadre = item.intIDModuloPadre
                                              }).ToList();
            }




            if (empleado != null)
                SetNewSession(empleado, empleado.chrCodigoEmpleado, proLoginResult.SID);


            return empleado;
        }

        private void SetNewSession(IEmpleado empleado, string chrCodigoEmpleado, string chrSession)
        {
            tblEmpleados empleoyee = (from item in db.tblEmpleados
                                      where item.intIDEmpleado == chrCodigoEmpleado
                                      && item.chrCodigoEmpleado == chrCodigoEmpleado
                                      select item).FirstOrDefault();
            if (empleoyee != null)
            {
                empleoyee.chrSession = chrSession;
                db.SaveChanges();
                empleado.chrSession = chrSession;

            }
            db.Dispose();
        }

        public List<IEmpleado> GetEmpleados()
        {

            List<IEmpleado> lstEmpleados = (from item in db.tblEmpleados
                                            select new IEmpleado
                                            {
                                                intIDEmpleado = item.intIDEmpleado,
                                                chrCodigoEmpleado = item.chrCodigoEmpleado,
                                                chrNombre = item.chrNombre,
                                                chrApellidoPaterno = item.chrApellidoPaterno,
                                                chrApellidoMaterno = item.chrApellidoMaterno,
                                                dtdFechaNacimiento = item.dtdFechaNacimiento,
                                                chrDepartamento = item.chrDepartamento
                                            }).ToList();
            db.Dispose();
            return lstEmpleados;

        }

        public int NotificaContrasenaOlvido(string correo)
        {
            string url = WebConfigurationManager.AppSettings["DocsUrl"];
            int exito = 1;

            if (correo == null || correo.Trim().Equals(""))
                return 0;

            if (ExisteUsuario(correo))
            {
                try
                {

                    string codigo = GetPassword(correo);


                    string mensaje = "<table width='600px'>"
                                      + "<td width = '50%' >"
                                      + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                      + " </td>"
                                      + "  </tr>"
                                      + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                      + "  <td>"
                                      + "<h1> Control de viajes</ h1 >"
                                      + "</td>"
                                      + "</tr>"
                                      + "<tr>"
                                      + "<td>"
                                      + "<p> Estimado " + correo + "</p>"
                                      + "<p> Se le envia su contraseña del sistema: " + codigo + "</p>"
                                      + "Puede acceder desde  <a href='" + url + "'>aquí</a>"
                                      + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                      + " </td></tr> <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";


                    string asuntoCorreo = "Recuperar Contraseña";

                    string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);

                    if (!error.Equals(""))
                    {
                        throw new System.ArgumentException(error, "original");
                        return 3;
                    }



                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }
            return exito;
        }

        public bool ExisteUsuario(string correo)
        {
            bool existe = true;

            try
            {
                tblEmpleados empl = (from item in db.tblEmpleados
                                     where item.chrCorreo == correo
                                     select item).FirstOrDefault();
                return (empl != null);

            }
            catch (Exception e)
            {
                existe = false;
            }

            return existe;
        }

        public string GetPassword(string chrCorreo)
        {
            string chrPwd = string.Empty;

            if (ExisteUsuario(chrCorreo))
            {
                chrPwd = (from item in db.tblEmpleados
                          where item.chrCorreo == chrCorreo
                          select item.chrPassword).FirstOrDefault();
            }

            return chrPwd;
        }

        public IEmpleado RegistroEmpleado(IEmpleado infoEmpleado)
        {
            tblEmpleados empledo;
            if (infoEmpleado != null)
            {
                if (infoEmpleado.intIDEmpleado == null || infoEmpleado.intIDEmpleado == "")
                {
                    empledo = new tblEmpleados();
                    empledo.chrCodigoEmpleado = infoEmpleado.chrCodigoEmpleado;
                    empledo.chrNombre = infoEmpleado.chrNombre;
                    empledo.chrApellidoPaterno = infoEmpleado.chrApellidoPaterno;
                    empledo.chrApellidoMaterno = infoEmpleado.chrApellidoMaterno;
                    empledo.dtdFechaAlta = infoEmpleado.dtdFechaAlta;
                    empledo.dtdFechaBaja = infoEmpleado.dtdFechaBaja;
                    empledo.chrPassword = infoEmpleado.chrPassword;
                    empledo.chrCorreo = infoEmpleado.chrCorreo;
                    empledo.intIDPerfil = (int)infoEmpleado.intIDPerfil;
                    empledo.chrEstatus = infoEmpleado.chrEstatus;
                    empledo.chrSociedad = infoEmpleado.chrSociedad;
                    empledo.chrPuesto = infoEmpleado.chrPuesto;
                    empledo.chrDepartamento = infoEmpleado.chrDepartamento;
                    empledo.chrCentroCosto = infoEmpleado.chrCentroCosto;
                    empledo.chrPasaporte = infoEmpleado.chrPasaporte;
                    db.tblEmpleados.Add(empledo);


                }
                else
                {
                    empledo = (from item in db.tblEmpleados
                               where item.intIDEmpleado == infoEmpleado.intIDEmpleado
                               select item).FirstOrDefault();
                    if (empledo != null)
                    {
                        empledo.chrCodigoEmpleado = infoEmpleado.chrCodigoEmpleado;
                        empledo.chrNombre = infoEmpleado.chrNombre;
                        empledo.chrApellidoPaterno = infoEmpleado.chrApellidoPaterno;
                        empledo.chrApellidoMaterno = infoEmpleado.chrApellidoMaterno;
                        empledo.dtdFechaAlta = infoEmpleado.dtdFechaAlta;
                        empledo.dtdFechaBaja = infoEmpleado.dtdFechaBaja;
                        empledo.chrPassword = infoEmpleado.chrPassword;
                        empledo.chrCorreo = infoEmpleado.chrCorreo;
                        empledo.intIDPerfil = (int)infoEmpleado.intIDPerfil;
                        empledo.chrEstatus = infoEmpleado.chrEstatus;
                        empledo.chrSociedad = infoEmpleado.chrSociedad;
                        empledo.chrPuesto = infoEmpleado.chrPuesto;
                        empledo.chrDepartamento = infoEmpleado.chrDepartamento;
                        empledo.chrCentroCosto = infoEmpleado.chrCentroCosto;
                        empledo.chrPasaporte = infoEmpleado.chrPasaporte;

                    }

                }

                db.SaveChanges();

            }
            return infoEmpleado;

        }

        public List<IEmpleado> UsuariosAutorizador(IEmpleado inforEmpleado)
        {
            string chrPrivilegio = WebConfigurationManager.AppSettings["PrivilegioAutorizador"].ToString();
            string strConexion = ConfigurationManager.ConnectionStrings["sfSeguridad"].ConnectionString;
            MySqlConnection conn = new MySqlConnection(strConexion);
            MySqlDataAdapter getAutorizadorDataAdapter;
            DataSet dsAutorizador = new DataSet();


            try
            {
                conn.Open();

                string sql = "SELECT * FROM usuarios where  privilegio = '"+ chrPrivilegio +"'; ";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                getAutorizadorDataAdapter = new MySqlDataAdapter(cmd);
                getAutorizadorDataAdapter.Fill(dsAutorizador);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                conn.Close();
            }

            List<IEmpleado> usuarioAutorizador = null;

            usuarioAutorizador = new List<IEmpleado>();
            DataTable tblEmpleadosAutozador = dsAutorizador.Tables[0];
            IEmpleado empleados = null;

            for (int i = 0; i < tblEmpleadosAutozador.Rows.Count; i++)
            {
                empleados = new IEmpleado();
                empleados.intIDEmpleado = tblEmpleadosAutozador.Rows[i]["idUsuario"].ToString();
                empleados.chrCodigoEmpleado = tblEmpleadosAutozador.Rows[i]["idUsuario"].ToString();
                empleados.chrNombre = tblEmpleadosAutozador.Rows[i]["nombreUsuario"].ToString();
                empleados.chrDepartamento = tblEmpleadosAutozador.Rows[i]["Puesto"].ToString();
                usuarioAutorizador.Add(empleados);

            }

            return usuarioAutorizador;

        }

        public int ActualizaEmpleado(string info)
        {

            int exito = 0;
            if (info != null)
            {

                IEmpleadoIntegracion m = JsonConvert.DeserializeObject<IEmpleadoIntegracion>(info);

                var empleado = (from _item in db.tblEmpleados
                                where _item.intIDEmpleado == m.empleado
                                select _item).FirstOrDefault();

                if (empleado != null)
                {
                    empleado.chrNombre = m.nombre;
                    empleado.intIDEmpleado = m.empleado;
                    empleado.chrCodigoEmpleado = m.empleado;
                    empleado.dtdFechaNacimiento = m.fechanac;
                    empleado.chrCentroCosto = m.centrocosto;
                    empleado.chrPuesto = m.puesto;
                    empleado.chrSociedad = m.razonsocial;
                    empleado.chrDepartamento = m.departamento;

                }
                else
                {
                    empleado = new tblEmpleados();
                    empleado.chrNombre = m.nombre;
                    empleado.intIDEmpleado = m.empleado;
                    empleado.chrCodigoEmpleado = m.empleado;
                    empleado.dtdFechaNacimiento = m.fechanac;
                    empleado.chrCentroCosto = m.centrocosto;
                    empleado.chrPuesto = m.puesto;
                    empleado.chrSociedad = m.razonsocial;
                    empleado.chrDepartamento = m.departamento;
                    db.tblEmpleados.Add(empleado);

                    exito = 1;

                }
                db.SaveChanges();



            }
            else
            {
                exito = 1;
            }

            return exito;
        }

        public int InsertaPantalla(List<IPantallaUsuario> objetoPantallas, string chrCodiEmpleado, int intFKIDPerfil)
        {
            tblPantallas pantallas;
            List<IPantallaUsuario> pantalla = (from _pantalla in db.tblPantallas
                                               where _pantalla.chrCodigoEmpleado == chrCodiEmpleado
                                               select new IPantallaUsuario
                                               {
                                                   intIDPantallas = _pantalla.intIDPantallas,
                                                   chrUrl = _pantalla.chrUrl
                                               }).ToList();

            if ((objetoPantallas.Count() != pantalla.Count()))
            {
                foreach (IPantallaUsuario item in pantalla)
                {
                    pantallas = new tblPantallas();
                    pantallas.intIDPantallas = (int)item.intIDPantallas;
                    db.tblPantallas.Attach(pantallas);
                    db.tblPantallas.Remove(pantallas);
                    db.SaveChanges();
                }
            }
            if ((objetoPantallas.Count() != pantalla.Count()) || pantalla.Count() < 0)
            {

                foreach (IPantallaUsuario item in objetoPantallas)
                {
                    string[] url = item.chrUrl.Split('/');
                    string menuprincipal = "";
                    string[] separaUrl = url[4].Split('?');
                    if (separaUrl[0].Equals("solicitud-de-reservaciones")
                        || separaUrl[0].Equals("detalle-vuelo-hospedaje")
                        || separaUrl[0].Equals("reportes")
                        || separaUrl[0].Equals("administrador-solicitudes")
                        || separaUrl[0].Equals("autorizador-solicitudes")
                        || separaUrl[0].Equals("configuracion"))
                    {
                        menuprincipal = "SI";
                    }
                    else
                    {
                        menuprincipal = "NO";
                    }

                    pantallas = new tblPantallas();
                    pantallas.intIDModulo = item.intIDModulo;
                    pantallas.intIDSistema = item.intIDSistema;
                    pantallas.chrDescripcionModulo = item.chrDescripcionModulo;
                    pantallas.chrUrl = separaUrl[0];
                    pantallas.intDelete = 1;
                    pantallas.intFKIDPerfil = intFKIDPerfil;
                    pantallas.intIDModuloPadre = item.intIDModuloPadre;
                    pantallas.chrMenuPrincipal = menuprincipal;
                    pantallas.chrImagenIcono = item.chrImagenIcono;
                    pantallas.chrValue = item.chrValue;
                    pantallas.chrCodigoEmpleado = chrCodiEmpleado;
                    db.tblPantallas.Add(pantallas);
                    db.SaveChanges();
                }
            }
            return 0;
        }

        public string GetIdSistema()
        {
            string chrIDSistema = WebConfigurationManager.AppSettings["chrIDSistema"].ToString();

            return chrIDSistema;
        }



    }
}