﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Models;

namespace WebApi.Areas.Api.Models.Manager
{
    public class ReportesManager
    {
        private GS_CCCONTROL_VIAJESEntities db;
        public ReportesManager()
        {
            db = new GS_CCCONTROL_VIAJESEntities();
        }

        public List<IResumenVuelo> GetResumenVueloPorEmpleado(string intIDEmpleado)
        {
            int intDelete = Convert.ToSByte(true);


            List<IResumenVuelo> lstSoliciud = (from item in db.tblSolicitudes

                                               join tblEmpleados item4 in db.tblEmpleados on item.intNoSolicitante equals item4.intIDEmpleado
                                               join tblDetalleSolicitudViaje item5 in db.tblDetalleSolicitudViaje on (int?)item.intIDSolicitud equals (int?)item5.intFKIDSolicitud
                                               join tblTipoViaje item6 in db.tblTipoViaje on (int?)item5.intFKIDTipoViaje equals (int?)item6.intIDTipoViaje
                                               join tblTipoVuelo item7 in db.tblTipoVuelo on (int?)item5.intFKIDTipoVuelo equals (int?)item7.intIDTipoVuelo
                                               join item8 in db.tblOperacionesSolicitudAvion on item5.intIDDetalleSolicitudViaje equals (int)item8.intFKDetalleSolicitudViaje into OperacionViaje
                                               from OV in OperacionViaje.DefaultIfEmpty()
                                               where item.intNoSolicitante == intIDEmpleado
                                               && item.intDelete == intDelete
                                               select new IResumenVuelo
                                               {
                                                   intIDSolicitud = (int)item.intIDSolicitud,
                                                   chrNoEmpleado = item4.chrCodigoEmpleado,
                                                   chrNombreEmpleado = item4.chrNombre,
                                                   chrApellidoPaterno = item4.chrApellidoPaterno,
                                                   chrApellidoMaterno = item4.chrApellidoMaterno,
                                                   chrCentroCosto = item5.chrCentroCostoViaje,
                                                   chrTipoViaje = item6.chrDescripcion,
                                                   chrTipoVuelo = item7.chrDescripcion,
                                                   chrAerolinea = item5.chrAerolinea,
                                                   dtdFechaNacimiento = (DateTime)item4.dtdFechaNacimiento,
                                                   chrOrigen = item5.chrClaveOrigen,
                                                   chrDestino = item5.chrClaveDestino,
                                                   dtdFechaSalidad = (DateTime)item5.dtdFechaSalidad,
                                                   dtdFechaRegreso = (DateTime)item5.dtdFechaRegreso,
                                                   decImporte = (OV.decCosto + OV.decCostoExtraAvion)
                                               }).ToList();


            db.Dispose();

            return lstSoliciud;
        }

        public List<IResumenHotel> GetResumenHotelPorEmpleado(string intIDEmpleado)
        {
            List<IResumenHotel> lstResumenHotel = (from solicitud in db.tblSolicitudes
                                                   join tblEmpleados empleados in db.tblEmpleados on solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                                   join tblDetalleSolicitudHotel detalleHotel in db.tblDetalleSolicitudHotel on (int?)solicitud.intIDSolicitud equals (int?)detalleHotel.intFKIDSolicitud
                                                   join tblTipoHabitacion tipoHabitacion in db.tblTipoHabitacion on detalleHotel.intFKIDTipoHabitacion equals (int?)tipoHabitacion.intIDTipoHabitacion
                                                   join item8 in db.tblOperacionesSolicitudHotel on detalleHotel.intIDDetalleSolicitudHotel equals (int)item8.intFKIDDetalleSolicitudHotel into OperacionHotel
                                                   from OH in OperacionHotel.DefaultIfEmpty()
                                                   where solicitud.intNoSolicitante == intIDEmpleado
                                                   && solicitud.intDelete > 0
                                                   select new IResumenHotel
                                                   {
                                                       intIDSolicitud = (int)solicitud.intIDSolicitud,
                                                       chrNoEmpleado = empleados.chrCodigoEmpleado,
                                                       chrNombreEmpleado = empleados.chrNombre,
                                                       chrApellidoPaterno = empleados.chrApellidoPaterno,
                                                       chrApellidoMaterno = empleados.chrApellidoMaterno,
                                                       chrCentroCostoHotel = detalleHotel.chrCentroCostoHospedaje,
                                                       chrHotel = detalleHotel.chrEsHotel,
                                                       chrHabitaciones = (int)detalleHotel.intNoHabitaciones,
                                                       chrTipoHabitacion = tipoHabitacion.chrDescripcion,
                                                       chrOrigen = detalleHotel.chrClaveOrigen,
                                                       chrDestino = detalleHotel.chrClaveDestino,
                                                       dtdFechaRegreso = (DateTime)detalleHotel.dtdFechaRegreso,
                                                       dtdFechaSalidad = (DateTime)detalleHotel.dtdFechaSalida,
                                                       chrNochesReservadas = detalleHotel.chrNochesReservadas ?? 0,
                                                       decImporte = (OH.decCosto + OH.decCostoExtraHotel)
                                                   }).ToList();
            db.Dispose();

            return lstResumenHotel;
        }

        public List<IResumenGeneral> ConsultaSolicitudesGeneral(string intIDCodigoEmpleado)
        {
            int intDelete = Convert.ToSByte(true);
            int intFKIDEstatusCerrado = Convert.ToInt32(WebConfigurationManager.AppSettings["intFKIDEstatusCerrado"]);

            List<IResumenGeneral> lstReporteCotizadas;


            var empleado = (from _empleado in db.tblEmpleados
                            where _empleado.intIDEmpleado == intIDCodigoEmpleado
                            select _empleado).FirstOrDefault();

            if (empleado.intIDPerfil == 3)
            {
                lstReporteCotizadas = (from _solicitud in db.tblSolicitudes
                                       join tblEmpleados empleados in db.tblEmpleados on _solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                       join tblTipoSolicitud tipoSolicitud in db.tblTipoSolicitud
                                       on _solicitud.intFKIDTipoSolicitud equals (int)tipoSolicitud.intIDTipoSolicitud
                                       join _estatus in db.tblEstatusSolicitud on (int)_solicitud.intFKIDEstatus equals _estatus.intIDEstatus
                                       join detalleViaje in db.tblDetalleSolicitudViaje on _solicitud.intIDSolicitud equals (int)detalleViaje.intFKIDSolicitud into FactDesc
                                       from fd in FactDesc.DefaultIfEmpty()

                                       join detalleHotel in db.tblDetalleSolicitudHotel on _solicitud.intIDSolicitud equals (int)detalleHotel.intFKIDSolicitud into DetallleHotelDesc
                                       from DH in DetallleHotelDesc.DefaultIfEmpty()

                                       join item8 in db.tblOperacionesSolicitudAvion on fd.intIDDetalleSolicitudViaje equals (int)item8.intFKDetalleSolicitudViaje into OperacionViaje
                                       from OV in OperacionViaje.DefaultIfEmpty()
                                       join item8 in db.tblOperacionesSolicitudHotel on DH.intIDDetalleSolicitudHotel equals (int)item8.intFKIDDetalleSolicitudHotel into OperacionHotel
                                       from OH in OperacionHotel.DefaultIfEmpty()
                                       where
                                        _solicitud.intDelete == intDelete && _solicitud.intFKIDEstatus == intFKIDEstatusCerrado
                                         && (_solicitud.intIDAutorizadorSuplente == intIDCodigoEmpleado || empleado.intFKIDEmpleadoAutorizador == intIDCodigoEmpleado)
                                       orderby DH.dtdFechaSalida, fd.dtdFechaSalidad ascending
                                       select new IResumenGeneral
                                       {
                                           intIDSolicitud = (int)_solicitud.intIDSolicitud,
                                           
                                           chrNombreEmpleado = empleados.chrNombre,
                                           chrApellidoPaterno = empleados.chrApellidoPaterno,
                                           chrApellidoMaterno = empleados.chrApellidoMaterno,
                                           chrTipoSolicitud = tipoSolicitud.chrDescripcion,
                                           chrCompania = empleados.chrSociedad,
                                           chrDepartamento = empleados.chrDepartamento,
                                           chrNoEmpleado = empleado.intIDEmpleado,
                                           dtdFechaSolicitud = _solicitud.dtdFechaSolicitud,
                                           chrMotivoHotel = DH.chrComentarios,
                                           chrMotivoViaje = fd.chrMotivoViaje,
                                           decImporteHotel = (OH.decCosto + OH.decCostoExtraHotel),
                                           decImporteVuelo = (OV.decCosto + OV.decCostoExtraAvion),
                                           chrEstatus = _estatus.chrEstatus,
                                           chrRazonSocialHotel = OH.chrLineaHotel,
                                           chrRazonSocialVuelo = OV.chrLineaArea,
                                           chrRFCHotel = OH.chrRFC,
                                           chrRFCVuelo = OV.chrRFC
                                       }).OrderBy(vuelo => new { vuelo.dtdFechaSolicitud }).ToList();

            }
            else if (empleado.intIDPerfil == 1)
            {
                lstReporteCotizadas = (from _solicitud in db.tblSolicitudes
                                       join tblEmpleados empleados in db.tblEmpleados on _solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                       join _estatus in db.tblEstatusSolicitud on (int)_solicitud.intFKIDEstatus equals _estatus.intIDEstatus
                                       join tblTipoSolicitud tipoSolicitud in db.tblTipoSolicitud
                                       on _solicitud.intFKIDTipoSolicitud equals (int)tipoSolicitud.intIDTipoSolicitud
                                       join detalleViaje in db.tblDetalleSolicitudViaje on _solicitud.intIDSolicitud equals (int)detalleViaje.intFKIDSolicitud into FactDesc
                                       from fd in FactDesc.DefaultIfEmpty()
                                       join detalleHotel in db.tblDetalleSolicitudHotel on _solicitud.intIDSolicitud equals (int)detalleHotel.intFKIDSolicitud into DetallleHotelDesc
                                       from DH in DetallleHotelDesc.DefaultIfEmpty()
                                       join item8 in db.tblOperacionesSolicitudAvion on fd.intIDDetalleSolicitudViaje equals (int)item8.intFKDetalleSolicitudViaje into OperacionViaje
                                       from OV in OperacionViaje.DefaultIfEmpty()
                                       join item8 in db.tblOperacionesSolicitudHotel on DH.intIDDetalleSolicitudHotel equals (int)item8.intFKIDDetalleSolicitudHotel into OperacionHotel
                                       from OH in OperacionHotel.DefaultIfEmpty()
                                       where
                                        _solicitud.intDelete == intDelete && _solicitud.intFKIDEstatus == intFKIDEstatusCerrado
                                        && _solicitud.intNoEmpleadoCreador == intIDCodigoEmpleado
                                       orderby DH.dtdFechaSalida, fd.dtdFechaSalidad ascending
                                       select new IResumenGeneral
                                       {
                                          
                                           intIDSolicitud = (int)_solicitud.intIDSolicitud,
                                           chrNombreEmpleado = empleados.chrNombre,
                                           chrApellidoPaterno = empleados.chrApellidoPaterno,
                                           chrApellidoMaterno = empleados.chrApellidoMaterno,
                                           chrTipoSolicitud = tipoSolicitud.chrDescripcion,
                                           chrCompania = empleados.chrSociedad,
                                           chrDepartamento = empleados.chrDepartamento,
                                           chrNoEmpleado = empleado.intIDEmpleado,
                                           dtdFechaSolicitud = _solicitud.dtdFechaSolicitud,
                                           chrMotivoHotel = DH.chrComentarios,
                                           chrMotivoViaje = fd.chrMotivoViaje,
                                           decImporteHotel = (OH.decCosto + OH.decCostoExtraHotel),
                                           decImporteVuelo = (OV.decCosto + OV.decCostoExtraAvion),
                                           chrEstatus = _estatus.chrEstatus,
                                           chrRazonSocialHotel = OH.chrLineaHotel,
                                           chrRazonSocialVuelo = OV.chrLineaArea,
                                           chrRFCHotel = OH.chrRFC,
                                           chrRFCVuelo = OV.chrRFC
                                       }).OrderBy(vuelo => new { vuelo.dtdFechaSolicitud }).ToList();

            }
            else
            {
                lstReporteCotizadas = (from _solicitud in db.tblSolicitudes
                                       join tblEmpleados empleados in db.tblEmpleados on _solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                       join _estatus in db.tblEstatusSolicitud on (int)_solicitud.intFKIDEstatus equals _estatus.intIDEstatus
                                       join tblTipoSolicitud tipoSolicitud in db.tblTipoSolicitud
                                       on _solicitud.intFKIDTipoSolicitud equals (int)tipoSolicitud.intIDTipoSolicitud
                                       join detalleViaje in db.tblDetalleSolicitudViaje on _solicitud.intIDSolicitud equals (int)detalleViaje.intFKIDSolicitud into FactDesc
                                       from fd in FactDesc.DefaultIfEmpty()
                                       join detalleHotel in db.tblDetalleSolicitudHotel on _solicitud.intIDSolicitud equals (int)detalleHotel.intFKIDSolicitud into DetallleHotelDesc
                                       from DH in DetallleHotelDesc.DefaultIfEmpty()
                                       join item8 in db.tblOperacionesSolicitudAvion on fd.intIDDetalleSolicitudViaje equals (int)item8.intFKDetalleSolicitudViaje into OperacionViaje
                                       from OV in OperacionViaje.DefaultIfEmpty()
                                       join item8 in db.tblOperacionesSolicitudHotel on DH.intIDDetalleSolicitudHotel equals (int)item8.intFKIDDetalleSolicitudHotel into OperacionHotel
                                       from OH in OperacionHotel.DefaultIfEmpty()
                                       where
                                        _solicitud.intDelete == intDelete && _solicitud.intFKIDEstatus == intFKIDEstatusCerrado

                                       orderby DH.dtdFechaSalida, fd.dtdFechaSalidad ascending
                                       select new IResumenGeneral
                                       {
                                         

                                           intIDSolicitud = (int)_solicitud.intIDSolicitud,
                                           chrNombreEmpleado = empleados.chrNombre,
                                           chrApellidoPaterno = empleados.chrApellidoPaterno,
                                           chrApellidoMaterno = empleados.chrApellidoMaterno,
                                           chrTipoSolicitud = tipoSolicitud.chrDescripcion,
                                           chrCompania = empleados.chrSociedad,
                                           chrDepartamento = empleados.chrDepartamento,
                                           chrNoEmpleado = empleado.intIDEmpleado,
                                           dtdFechaSolicitud = _solicitud.dtdFechaSolicitud,
                                           chrMotivoHotel = DH.chrComentarios,
                                           chrMotivoViaje = fd.chrMotivoViaje,
                                           decImporteHotel = (OH.decCosto + OH.decCostoExtraHotel),
                                           decImporteVuelo = (OV.decCosto + OV.decCostoExtraAvion),
                                           chrEstatus = _estatus.chrEstatus,
                                           chrRazonSocialHotel = OH.chrLineaHotel,
                                           chrRazonSocialVuelo = OV.chrLineaArea,
                                           chrRFCHotel = OH.chrRFC,
                                           chrRFCVuelo = OV.chrRFC
                                       }).OrderBy(vuelo => new { vuelo.dtdFechaSolicitud }).ToList();



            }

            return lstReporteCotizadas;

        }
        public List<IReporteSolicitudAutorizados> ConsultaSolicitudesAutorizar(string intIDAutorizador, int? intFKIDEstatus, int? intFKIDTipoSolicitud, string intFKIDSolicitante)
        {

            int intDelete = Convert.ToSByte(true);
            List<IReporteSolicitudAutorizados> lstAutorizados = (from _solicitud in db.tblSolicitudes
                                                                 join tblEmpleados empleados in db.tblEmpleados
                                                                 on _solicitud.intNoSolicitante equals empleados.intIDEmpleado
                                                                 join tblTipoSolicitud _tipoSolicitud in db.tblTipoSolicitud on
                                                                 _solicitud.intFKIDTipoSolicitud equals (int)_tipoSolicitud.intIDTipoSolicitud
                                                                 join tblEstatusSolicitud _estatus in db.tblEstatusSolicitud
                                                                 on _solicitud.intFKIDEstatus equals (int)_estatus.intIDEstatus
                                                                 join detalleViaje in db.tblDetalleSolicitudViaje on _solicitud.intIDSolicitud equals (int)detalleViaje.intFKIDSolicitud into DetalleViaje
                                                                 from DV in DetalleViaje.DefaultIfEmpty()
                                                                 join detalleHotel in db.tblDetalleSolicitudHotel on _solicitud.intIDSolicitud equals (int)detalleHotel.intFKIDSolicitud into DetallleHotelDesc
                                                                 from DH in DetallleHotelDesc.DefaultIfEmpty()
                                                                 where _solicitud.intDelete == intDelete
                                                                 // && (_solicitud.dtdFechaSolicitud == listaAutorizadas.dtdFechaEnvioHora) 
                                                                 && (_solicitud.intIDAutorizadorSuplente == intIDAutorizador || empleados.intFKIDEmpleadoAutorizador == intIDAutorizador)
                                                                 && (_solicitud.intFKIDEstatus == intFKIDEstatus || intFKIDEstatus == 0)
                                                                 && (_solicitud.intFKIDTipoSolicitud == intFKIDTipoSolicitud || intFKIDTipoSolicitud == 0)
                                                                 && (_solicitud.intNoSolicitante == intFKIDSolicitante || intFKIDSolicitante != null)
                                                                 orderby _solicitud.intFKIDEstatus descending
                                                                 select new IReporteSolicitudAutorizados
                                                                 {
                                                                     intIDSolicitud = (int)_solicitud.intIDSolicitud,
                                                                     dtdFechaEnvioHora = _solicitud.dtdFechaSolicitud,
                                                                     dtdFechaCotizacion = _solicitud.dtdFechaCotizada,
                                                                     dtdFechaAutorizacion = _solicitud.dtdFechaAutorizada,
                                                                     intNumSolicitud = _solicitud.intFolio,
                                                                     chrNombreSolicitante = empleados.chrNombre,
                                                                     chrApellidoPaternoSolicitante = empleados.chrApellidoPaterno,
                                                                     chrApellidoMaternoSolicitante = empleados.chrApellidoMaterno,
                                                                     chrTipoSolicitud = _tipoSolicitud.chrDescripcion,
                                                                     chrMotivoViaje = DV.chrMotivoViaje,
                                                                     chrMotivoHotel = DH.chrComentarios,
                                                                     chrEstatus = _estatus.chrEstatus,
                                                                     intNoCotizaciones = _solicitud.intNumCotizadas
                                                                 }).ToList();


            return lstAutorizados;
        }

    }
}