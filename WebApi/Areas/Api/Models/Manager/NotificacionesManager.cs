﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Models;
using WebApi.Utils;

namespace WebApi.Areas.Api.Models.Manager
{

    public class NotificacionesManager
    {

        private GS_CCCONTROL_VIAJESEntities db;
        GS_MAIL enviarCorreo = new GS_MAIL();
        UtilsGS utils = new UtilsGS();

        public NotificacionesManager()
        {
            db = new GS_CCCONTROL_VIAJESEntities();

        }

        public int EnviarSolicitudAdministrador(int intIDSolicitud)
        {
            int exito = 1;
            string url = WebConfigurationManager.AppSettings["DocsUrl"];
            string perfilAdmistrador = WebConfigurationManager.AppSettings["PerfilAdminitrador"];

            int perfil = Convert.ToInt32(perfilAdmistrador);

            string correo = "";
            string nombre = "";

            if (intIDSolicitud == null || intIDSolicitud < 0)
            {
                return 0;
            }

            var solicitud = (from item in db.tblSolicitudes
                             where item.intIDSolicitud == intIDSolicitud &&
                             item.intDelete == 1
                             select item).FirstOrDefault();

            int numeroSolicitud = (int)solicitud.intIDSolicitud;

            var adminsitadores = (from _empleados in db.tblEmpleados
                                  where _empleados.intIDPerfil == perfil
                                  && _empleados.intDelete == 1
                                  select _empleados).ToList();


            if (solicitud != null)
            {
                try
                {
                    for (int i = 0; i < adminsitadores.Count; i++)
                    {
                        correo = adminsitadores[i].chrCorreo;
                        nombre = adminsitadores[i].chrNombre + " " + adminsitadores[i].chrApellidoPaterno + " " + adminsitadores[i].chrApellidoMaterno;

                        string mensaje = "<table width='600px'>"
                                       + "<td width = '50%' >"
                                       + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                       + " </td>"
                                       + "  </tr>"
                                       + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                       + "  <td>"
                                       + "<h1> Control de viajes</h1>"
                                       + "</td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<p> Estimado " + adminsitadores[i].chrNombre + " " + adminsitadores[i].chrApellidoPaterno + " " + adminsitadores[i].chrApellidoMaterno + "</p>"
                                       + "<p> Usted tiene una solicitud " + numeroSolicitud + " para cotización, favor de dar seguimiento accesando al módulo de reservaciones </p>"
                                       + "Puede acceder desde  <a href='" + url + "detalle-solicitudes/'>aqui</a>"
                                       + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                       + " </td></tr> <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";


                        string asuntoCorreo = "Solicitud a Cotizar No." + numeroSolicitud;
                        string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);

                        if (!error.Equals(""))
                        {
                            throw new System.ArgumentException(error, "Original");
                            return 3;
                        }


                    }



                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }

            return exito;
        }

        public int EnviaCotizacionUsuario(int intIDSolicitud)
        {
            int exito = 1;
            string url = WebConfigurationManager.AppSettings["DocsUrl"];
            string tempDocUrl = WebConfigurationManager.AppSettings["DocsUrl"];

            if (intIDSolicitud == null || intIDSolicitud < 0)
            {
                return 0;
            }
            /*
            var dtlImagenesHotel = (from item in db.tblDetalleSolicitudHotel
                                where item.intFKIDSolicitud == intIDSolicitud
                                select new {
                                    intIDDetalleSolicitud = item.intIDDetalleSolicitudHotel,
                                    intCountImages = (from img in db.tblImagenesCotizar
                                                    where img.intFKIDDetalleSolicitudViajeHotel == item.intIDDetalleSolicitudHotel
                                                    select img).ToList().Count
                                }).ToList();

            var dtlImagenesVuelo = (from item in db.tblDetalleSolicitudViaje
                                    where item.intFKIDSolicitud == intIDSolicitud
                                    select new
                                    {
                                        intIDDetalleSolicitud = item.intIDDetalleSolicitudViaje,
                                        intCountImages = (from img in db.tblImagenesCotizar
                                                          where img.intFKIDDetalleSolicitudViajeHotel == item.intIDDetalleSolicitudViaje
                                                          select img).ToList().Count
                                    }).ToList();

            bool faltanImagenes = false;

            for (int i = 0; i < dtlImagenesHotel.Count; i++)
            {
                if (dtlImagenesHotel[i].intCountImages <= 0)
                {
                    faltanImagenes = true;
                    break;
                }
            }

            for (int i = 0; i < dtlImagenesVuelo.Count; i++)
            {
                if (dtlImagenesVuelo[i].intCountImages <= 0)
                {
                    faltanImagenes = true;
                    break;
                }
            }

            if (!faltanImagenes)
            {

            }*/




            /*  bool cambiarEstatusSolicitud = ((from item in db.tblImagenesCotizar
                                               where item.intFKIDSolicitud == intIDSolicitud
                                               && (item.intNoOpcionUsuario == null || item.intNoOpcionUsuario <= 0)
                                               select item).ToList().Count > 0) ? false : true;*/
            // if (cambiarEstatusSolicitud)
            var solicitud = (from _solicitud in db.tblSolicitudes
                             where _solicitud.intIDSolicitud == intIDSolicitud
                             select _solicitud).FirstOrDefault();

            if (solicitud != null)
            {
                
                if (solicitud != null)
                {
                    solicitud.intFKIDEstatus = 2;
                    db.SaveChanges();
                }

                var empleado = (from _empelado in db.tblEmpleados
                                where _empelado.intIDEmpleado == solicitud.intNoSolicitante
                                select _empelado).FirstOrDefault();
                // if (solicitud != null)
                //{
                try
                {

                    string correo = empleado.chrCorreo;
                    string mensaje = "<table width='600px'>"
                                      + "<td width = '50%' >"
                                      + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                      + " </td>"
                                      + " </tr>"
                                      + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                      + "  <td>"
                                      + "<h1> Control de viajes</h1>"
                                      + "</td>"
                                      + "</tr>"
                                      + "<tr>"
                                      + "<td>"
                                      + "<p> Estimado(a) " + empleado.chrNombre + " " + empleado.chrApellidoPaterno + " " + empleado.chrApellidoMaterno + "</p>"
                                      + "<p>Se le informa que su solicitud <b>No." + solicitud.intIDSolicitud + " a sido cotizada, favor de dar" + "seguimiento accesando al módulo de reservaciones <a href='" + tempDocUrl + "detalle-vuelo-hospedaje'"
                                      + " >aquí</a>. <br/></p>"
                                      + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                      + " </td></tr> <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";





                    string asuntoCorreo = "Cotización de solicitud No." + solicitud.intIDSolicitud;
                    string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);

                    var solicitudAuto = (from item in db.tblSolicitudes
                                         where item.intIDSolicitud == intIDSolicitud
                                         select item).FirstOrDefault();

                    if (solicitudAuto != null)
                    {
                        solicitudAuto.intNumCotizadas = solicitud.intNumCotizadas + 1;
                        db.SaveChanges();
                    }
                    if (!error.Equals(""))
                    {
                        throw new System.ArgumentException(error, "Original");
                        return 3;
                    }
                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }

            }else
             {
                exito = 2;
            }


            return exito;

        }

        public int CancelarCotizacionAdministrador(int intIDSolicitud, string chrMotivoCancelacion)
        {
            int exito = 1;
            string url = WebConfigurationManager.AppSettings["DocsUrl"];


            if (intIDSolicitud == null || intIDSolicitud < 0)
            {
                return 0;
            }
            var solicitud = (from _solicitud in db.tblSolicitudes
                             where _solicitud.intIDSolicitud == intIDSolicitud
                             select _solicitud).FirstOrDefault();
            if (solicitud != null)
            {
                solicitud.intFKIDEstatus = 3;
                db.SaveChanges();
            }

            var empleado = (from _empelado in db.tblEmpleados
                            where _empelado.intIDEmpleado == solicitud.intNoSolicitante
                            select _empelado).FirstOrDefault();

            if (solicitud != null)
            {


                try
                {

                    string correo = empleado.chrCorreo;
                    string mensaje = "<table width='600px'>"
                                       + "<td width = '50%' >"
                                       + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                       + " </td>"
                                       + "  </tr>"
                                       + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                       + "  <td>"
                                       + "<h1> Control de viajes</h1>"
                                       + "</td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<p> Estimado(a) " + empleado.chrNombre + " " + empleado.chrApellidoPaterno + " " + empleado.chrApellidoMaterno + "</p>"
                                       + "<p>Se le informa que su solicitud No. " + solicitud.intIDSolicitud + " ha sido <b>Rechazada</b>. El motivo de su cancelación es la siguiente: <b>" + chrMotivoCancelacion + "</b>. </p>"
                                       + "Puede acceder desde  <a href='" + url + "detalle-vuelo-hospedaje/'>aqui, para darle seguimiento.</a>"
                                       + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                       + " </td></tr><tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";




                    string asuntoCorreo = "Cancelación solicitud No." + solicitud.intIDSolicitud + " por Administrador";
                    string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);


                    GuardaLogSolicitud(intIDSolicitud, chrMotivoCancelacion, (string)solicitud.intNoEmpleadoCreador);

                    if (!error.Equals(""))
                    {
                        throw new System.ArgumentException(error, "Original");
                        return 3;
                    }
                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }


            return exito;

        }

        public int CancelarCotizacionUsuario(int intIDSolicitud, string chrMotivoCancelacion)
        {
            int exito = 1;
            string url = WebConfigurationManager.AppSettings["DocsUrl"];


            if (intIDSolicitud == null || intIDSolicitud < 0)
            {
                return 0;
            }
            var solicitud = (from _solicitud in db.tblSolicitudes
                             where _solicitud.intIDSolicitud == intIDSolicitud
                             select _solicitud).FirstOrDefault();
            if (solicitud != null)
            {
                solicitud.intFKIDEstatus = 3;
                db.SaveChanges();

                List<ICargaArchivos> imagen = (from _imagen in db.tblImagenesCotizar
                                               where _imagen.intFKIDSolicitud == intIDSolicitud
                                               && (_imagen.chrEsRechazada == 0 || _imagen.chrEsRechazada == null)
                                               select new ICargaArchivos
                                               {
                                                   intFKIDSolicitud = _imagen.intFKIDSolicitud,
                                                   intIDImagenesCotizar = _imagen.intIDImagenesCotizar,
                                                   intEsRechazado = _imagen.chrEsRechazada
                                               }).ToList();

                if (imagen != null)
                {
                    foreach (ICargaArchivos item in imagen)
                    {

                        var imagenesRechazadas = (from _imagnes in db.tblImagenesCotizar
                                                  where _imagnes.intIDImagenesCotizar == item.intIDImagenesCotizar
                                                  select _imagnes).FirstOrDefault();

                        imagenesRechazadas.chrEsRechazada = 1;
                        db.SaveChanges();

                    }

                }
            }

            var empleado = (from _empelado in db.tblEmpleados
                            where _empelado.intIDEmpleado == solicitud.intNoSolicitante
                            select _empelado).FirstOrDefault();

            var responsable = (from _responsable in db.tblEmpleados
                               where _responsable.intIDEmpleado == empleado.intFKIDEmpleadoAutorizador
                               select _responsable).FirstOrDefault();



            GuardaLogSolicitud(intIDSolicitud, chrMotivoCancelacion, (string)solicitud.intNoEmpleadoCreador);


            if (solicitud != null)
            {
                try
                {

                    string correo = empleado.chrCorreo;

                    string mensaje = "<table width='600px'>"
                   + "<td width = '50%' >"
                   + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                   + " </td>"
                   + "  </tr>"
                   + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                   + "  <td>"
                   + "<h1> Control de viajes</h1>"
                   + "</td>"
                   + "</tr>"
                   + "<tr>"
                   + "<td>"
                   + "<p> Estimado(a) <b>Administrador</b></p>"
                   + "<p>Se le informa que su solicitud No. " + solicitud.intIDSolicitud + " ha sido <b>Rechazada</b>, por el usuarios: " + empleado.chrNombre + " " + empleado.chrApellidoPaterno + " " + empleado.chrApellidoMaterno + ". El motivo de su cancelación es el siguiente: <b>" + chrMotivoCancelacion + "</b>. </p>"
                   + "Puede acceder desde  <a href='" + url + "detalle-solicitudes/'>aqui, para darle seguimiento.</a>"
                   + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                   + " </td></tr><tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";

                    string asuntoCorreo = "Cancelación solicitud No." + solicitud.intIDSolicitud + " por Usuario";
                    string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);




                    if (!error.Equals(""))
                    {
                        throw new System.ArgumentException(error, "Original");
                        return 3;
                    }
                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }


            return exito;

        }

        public int EnviarSolicitudUsuarioAuditorizador(int intIDSolicitud)
        {
            int exito = 1;
            string url = WebConfigurationManager.AppSettings["DocsUrl"];
            string CorreoSuplente = "";
            int intFKIDEstatusEnAutorizacion  = Convert.ToInt32( WebConfigurationManager.AppSettings["intFKIDEstatusEnAutorizado"]);

            string CorreoAutorizador = "";
            string nombreSuplente = "";
            string nombreAutorizador = "";
            string correo = "";
            string nombre = "";

            if (intIDSolicitud == null || intIDSolicitud < 0)
            {
                return 0;
            }
            bool cambiarEstatusSolicitud = ((from item in db.tblImagenesCotizar
                                             where item.intFKIDSolicitud == intIDSolicitud
                                             && (item.intNoOpcionUsuario == null || item.intNoOpcionUsuario <= 0)
                                             select item).ToList().Count > 0) ? false : true;

            var solicitud = (from item in db.tblSolicitudes
                             join tblEmpleados _empleados in db.tblEmpleados on item.intNoSolicitante equals _empleados.intIDEmpleado
                             where item.intIDSolicitud == intIDSolicitud &&
                             item.intDelete == 1
                             select item).FirstOrDefault();

            var nombreSolicitante = (from _empleadoSolicitante in db.tblEmpleados
                                     where _empleadoSolicitante.intIDEmpleado == solicitud.intNoSolicitante
                                     select _empleadoSolicitante).FirstOrDefault();

            if (!solicitud.intIDAutorizadorSuplente.Equals(""))
            {
                var correoSuplente = (from _empleados in db.tblEmpleados
                                      where _empleados.intIDEmpleado == solicitud.intIDAutorizadorSuplente
                                      select _empleados).FirstOrDefault();

                CorreoSuplente = correoSuplente.chrCorreo;
                nombreSuplente = correoSuplente.chrNombre + " " + correoSuplente.chrApellidoPaterno + " " + correoSuplente.chrApellidoMaterno;
            }
            else
            {
                var datosAutorizador = (from _autorizador in db.tblEmpleados
                                        where _autorizador.intIDEmpleado == solicitud.intNoSolicitante
                                        select _autorizador).FirstOrDefault();

                var correoAutorizador = (from _autorizador in db.tblEmpleados
                                         where _autorizador.intIDEmpleado == datosAutorizador.intFKIDEmpleadoAutorizador
                                         select _autorizador).FirstOrDefault();

                CorreoAutorizador = correoAutorizador.chrCorreo;
                nombreAutorizador = correoAutorizador.chrNombre + " " + correoAutorizador.chrApellidoPaterno + " " + correoAutorizador.chrApellidoMaterno;
            }

            //if (cambiarEstatusSolicitud)
            if (intIDSolicitud > 0)
            {
                try
                {


                    if (!CorreoAutorizador.Equals(""))
                    {
                        correo = CorreoAutorizador;
                        nombre = nombreAutorizador;
                    }
                    else
                    {
                        correo = CorreoSuplente;
                        nombre = nombreSuplente;
                    }
                    string mensaje = "<table width='600px'>"
                                      + "<td width = '50%' >"
                                      + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                      + " </td>"
                                      + "  </tr>"
                                      + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                      + "  <td>"
                                      + "<h1> Control de viajes</ h1 >"
                                      + "</td>"
                                      + "</tr>"
                                      + "<tr>"
                                      + "<td>"
                                      + "<p> Estimado " + nombre + "</p>"
                                      + "<p> El usuario " + nombreSolicitante.chrNombre + " " + nombreSolicitante.chrApellidoPaterno + " " + nombreSolicitante.chrApellidoMaterno + " ha elegido las prioridades de la  solicitud No." + intIDSolicitud + ", favor de revisar y agregar las prioridades de usted.</p>"
                                      + "Puede acceder desde  <a href='" + url + "prioridad-autorizador/'>aquí</a>"
                                      + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                      + " </td></tr> <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";



                    string asuntoCorreo = "Solicitud a Autorizar No." + solicitud.intIDSolicitud;
                    string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);

                    var solicitudPriorida = (from itemsolicitudPrioridadUser in db.tblSolicitudes
                                             where itemsolicitudPrioridadUser.intIDSolicitud == intIDSolicitud
                                             select itemsolicitudPrioridadUser).FirstOrDefault();
                    if (solicitudPriorida != null)
                    {
                        solicitudPriorida.intFKIDEstatus = (int)intFKIDEstatusEnAutorizacion;
                        db.SaveChanges();
                    }
                    if (!error.Equals(""))
                    {
                        throw new System.ArgumentException(error, "Original");
                        return 3;
                    }
                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }

            return exito;
        }

        public int EnviaSolicitudAuditorizadorAdministrador(int intIDSolicitud)
        {
            int exito = 1;
            string url = WebConfigurationManager.AppSettings["DocsUrl"];
            string perfilAdmistrador = WebConfigurationManager.AppSettings["PerfilAdminitrador"];
            int intFKIDEstustusAutorizado = Convert.ToInt32(WebConfigurationManager.AppSettings["intFKIDEstatusAutorizado"]);
            int perfil = Convert.ToInt32(perfilAdmistrador);

            string correo = "";
            string nombre = "";
            string CorreoSuplente = "";
            string nombreSuplente = "";
            string CorreoAutorizador = "";
            string nombreAutorizador = "";
            string correoAutoSuple = "";
            string nombreAutoSuple = "";

            if (intIDSolicitud == null || intIDSolicitud < 0)
            {
                return 0;
            }

            var solicitud = (from item in db.tblSolicitudes
                             where item.intIDSolicitud == intIDSolicitud &&
                             item.intDelete == 1
                             select item).FirstOrDefault();

            var prioridadesautorizador = (from itemprioridades in db.tblImagenesCotizar
                                          where itemprioridades.intFKIDSolicitud == intIDSolicitud
                                          select itemprioridades).ToList();
            int numeroSolicitud = (int)solicitud.intIDSolicitud;

            var adminsitadores = (from _empleados in db.tblEmpleados
                                  where _empleados.intIDPerfil == perfil
                                  && _empleados.intDelete == 0
                                  select _empleados).ToList();

            if (!solicitud.intIDAutorizadorSuplente.Equals(""))
            {
                var correoSuplente = (from _empleados in db.tblEmpleados
                                      where _empleados.intIDEmpleado == solicitud.intIDAutorizadorSuplente
                                      select _empleados).FirstOrDefault();

                CorreoSuplente = correoSuplente.chrCorreo;
                nombreSuplente = correoSuplente.chrNombre + " " + correoSuplente.chrApellidoPaterno + " " + correoSuplente.chrApellidoMaterno;
            }
            else
            {
                var datosAutorizador = (from _autorizador in db.tblEmpleados
                                        where _autorizador.intIDEmpleado == solicitud.intNoSolicitante
                                        select _autorizador).FirstOrDefault();

                var correoAutorizador = (from _autorizador in db.tblEmpleados
                                         where _autorizador.intIDEmpleado == datosAutorizador.intFKIDEmpleadoAutorizador
                                         select _autorizador).FirstOrDefault();

                CorreoAutorizador = correoAutorizador.chrCorreo;
                nombreAutorizador = correoAutorizador.chrNombre + " " + correoAutorizador.chrApellidoPaterno + " " + correoAutorizador.chrApellidoMaterno;
            }

            if (!CorreoAutorizador.Equals(""))
            {
                correoAutoSuple = CorreoAutorizador;
                nombreAutoSuple = nombreAutorizador;
            }
            else
            {
                correoAutoSuple = CorreoSuplente;
                nombreAutoSuple = nombreSuplente;
            }

            if (solicitud != null)
            {
                try
                {

                    //for (int j = 0; j <= prioridadesautorizador.Count(); j++)
                    //{

                    //    if (prioridadesautorizador[j].intNoOpcionAutorizador != null || prioridadesautorizador[j].intNoOpcionAutorizador != ' ')
                    //    {
                    for (int i = 0; i < adminsitadores.Count; i++)
                    {
                        correo = adminsitadores[i].chrCorreo;
                        nombre = adminsitadores[i].chrNombre + " " + adminsitadores[i].chrApellidoPaterno + " " + adminsitadores[i].chrApellidoMaterno;

                        string mensaje = "<table width='600px'>"
                                       + "<td width = '50%' >"
                                       + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                       + " </td>"
                                       + "  </tr>"
                                       + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                       + "  <td>"
                                       + "<h1> Control de viajes</ h1 >"
                                       + "</td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<p> Estimado(a) " + adminsitadores[i].chrNombre + " " + adminsitadores[i].chrApellidoPaterno + " " + adminsitadores[i].chrApellidoMaterno + "</p>"
                                       + "<p> El autorizador(a) " + nombreAutoSuple + " ha agregado las prioridades a la solicitud No." + numeroSolicitud + ", favor de dar seguimiento accesando al módulo de reservaciones </p>"
                                       + "Puede acceder desde  <a href='" + url + "detalle-solicitudes/'>aqui</a>"
                                       + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                       + " </td></tr> <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";


                        string asuntoCorreo = "Solicitud a autorizar No." + numeroSolicitud;


                        string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);

                        var solicitudPriorida = (from itemsolicitudPrioridadUser in db.tblSolicitudes
                                                 where itemsolicitudPrioridadUser.intIDSolicitud == intIDSolicitud
                                                 select itemsolicitudPrioridadUser).FirstOrDefault();
                        if (solicitudPriorida != null)
                        {
                            solicitudPriorida.intFKIDEstatus = intFKIDEstustusAutorizado;
                            db.SaveChanges();
                        }

                        if (!error.Equals(""))
                        {
                            throw new System.ArgumentException(error, "Original");
                            return 3;
                        }


                    }
                    //    }

                    //}
                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }

            return exito;
        }

        public void GuardaLogSolicitud(int intIDSolicitud, string chrMotivo, string intCodigoEmpleado)
        {
            if (intIDSolicitud > 0)
            {
                var solicitudLog = new tblLogSolicitudRechazada();
                solicitudLog.intFKIDSolicitud = (int)intIDSolicitud;
                solicitudLog.dtdFechaRechazo = DateTime.Now;
                solicitudLog.chrMotivoRechazo = chrMotivo;
                solicitudLog.intCodigoEmpleado = intCodigoEmpleado;
                solicitudLog.intDelete = 0;
                db.tblLogSolicitudRechazada.Add(solicitudLog);
                db.SaveChanges();
            }

        }
        public int EnviaCorreoHotel(int intIDSolicitud, string chrNombreHotel, string chrCorreo, string chrEmpleados, string pathCarta)
        {
            int exito = 1;
            string filename = String.Empty;
            string directoryName = String.Empty;
            GS_MAIL enviarEmail = new GS_MAIL();
            string url = WebConfigurationManager.AppSettings["DocsUrl"];
            string UrlArchivos = WebConfigurationManager.AppSettings["UrlArchivos"];


            List<Attachment> adjuntos = new List<Attachment>();
            string correo = "";
            string nombre = "";
            var path = HttpRuntime.AppDomainAppPath;
            directoryName = System.IO.Path.Combine(path, UrlArchivos);
            filename = System.IO.Path.Combine(directoryName, pathCarta);

            if (chrCorreo == null || chrCorreo.Equals(""))
            {
                return 0;
            }

            if (chrCorreo != null)
            {

                if (pathCarta != null && pathCarta != "")
                    adjuntos.Add(enviarEmail.getAttachment(filename));

                try
                {

                    correo = chrCorreo;

                    string mensaje = "<table width='600px'>"
                                   + "<td width = '50%' >"
                                   + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                   + " </td>"
                                   + "  </tr>"
                                   + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                   + "  <td>"
                                   + "<h1> Control de viajes</h1>"
                                   + "</td>"
                                   + "</tr>"
                                   + "<tr>"
                                   + "<td>"
                                   + "<p> Estimado " + chrNombreHotel + "</p>"
                                   + "<p> Se le adjunta la carta del la reservación, de los colaboradores " + chrEmpleados + " de una habitación. </a>"
                                   + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                   + " </td></tr> <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";


                    string asuntoCorreo = "Envio de carta por reservación " + chrNombreHotel;
                    string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo, null, adjuntos);

                    adjuntos.Clear();
                    if (!error.Equals(""))
                    {
                        throw new System.ArgumentException(error, "Original");
                        return 3;
                    }

                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }

            return exito;
        }

        public int EnviaCorreoUsuario(int intIDSolicitud)
        {
            string correo, nombre;
            int exito = 1;
            string url = WebConfigurationManager.AppSettings["DocsUrl"];
            List<IEmpleado> correoEmpleado;
            var solicitud = (from item in db.tblSolicitudes
                             where item.intIDSolicitud == intIDSolicitud
                             select item).FirstOrDefault();

            if (solicitud.intFKIDTipoSolicitud == 1) // vuelo +hospedaje 
            {
                correoEmpleado = (from detalleHotel in db.tblDetalleSolicitudHotel
                                  join tblDetalleHuespedes detallehuespedes in db.tblDetalleHuespedes
                                  on detalleHotel.intIDDetalleSolicitudHotel equals (int)detallehuespedes.intFKIDDetalleSolicitudHotel
                                  join tblEmpleados empleados in db.tblEmpleados
                                   on detallehuespedes.intFKIDEmpleado equals empleados.intIDEmpleado
                                  where detalleHotel.intFKIDSolicitud == solicitud.intIDSolicitud
                                  select new IEmpleado
                                  {
                                      intIDEmpleado = empleados.intIDEmpleado,
                                      chrNombre = empleados.chrNombre,
                                      chrApellidoMaterno = empleados.chrApellidoMaterno,
                                      chrApellidoPaterno = empleados.chrApellidoPaterno,
                                      chrCorreo = empleados.chrCorreo,
                                  }).ToList();
            }
            else if (solicitud.intFKIDTipoSolicitud == 2) // vuelo
            {
                correoEmpleado = (from detalleVuelo in db.tblDetalleSolicitudViaje
                                  join tblDetalleViajeros detalleViajeros in db.tblDetalleViajeros
                                  on detalleVuelo.intIDDetalleSolicitudViaje equals (int)detalleViajeros.intFKIDDetalleSolicitudViaje
                                  join tblEmpleados empleados in db.tblEmpleados
                                   on detalleViajeros.intFKIDEmpleado equals empleados.intIDEmpleado
                                  where detalleVuelo.intFKIDSolicitud == solicitud.intIDSolicitud
                                  select new IEmpleado
                                  {
                                      intIDEmpleado = empleados.intIDEmpleado,
                                      chrNombre = empleados.chrNombre,
                                      chrApellidoMaterno = empleados.chrApellidoMaterno,
                                      chrApellidoPaterno = empleados.chrApellidoPaterno,
                                      chrCorreo = empleados.chrCorreo,
                                  }).ToList();
            }
            else // hotel
            {
                correoEmpleado = (from detalleHotel in db.tblDetalleSolicitudHotel
                                  join tblDetalleHuespedes detallehuespedes in db.tblDetalleHuespedes
                                  on detalleHotel.intIDDetalleSolicitudHotel equals (int)detallehuespedes.intFKIDDetalleSolicitudHotel
                                  join tblEmpleados empleados in db.tblEmpleados
                                   on detallehuespedes.intFKIDEmpleado equals empleados.intIDEmpleado
                                  where detalleHotel.intFKIDSolicitud == solicitud.intIDSolicitud
                                  select new IEmpleado
                                  {
                                      intIDEmpleado = empleados.intIDEmpleado,
                                      chrNombre = empleados.chrNombre,
                                      chrApellidoMaterno = empleados.chrApellidoMaterno,
                                      chrApellidoPaterno = empleados.chrApellidoPaterno,
                                      chrCorreo = empleados.chrCorreo,
                                  }).ToList();
            }

            if (correoEmpleado != null)
            {
                try
                {


                    for (int i = 0; i < correoEmpleado.Count; i++)
                    {
                        correo = correoEmpleado[i].chrCorreo;
                        nombre = correoEmpleado[i].chrNombre + " " + correoEmpleado[i].chrApellidoPaterno + " " + correoEmpleado[i].chrApellidoMaterno;

                        string mensaje = "<table width='600px'>"
                                       + "<td width = '50%' >"
                                       + " <img src = '" + url + "/img/grupo-comercial-control.f33cd29b.png' />"
                                       + " </td>"
                                       + "  </tr>"
                                       + " <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;'>"
                                       + "  <td>"
                                       + "<h1> Control de viajes</ h1 >"
                                       + "</td>"
                                       + "</tr>"
                                       + "<tr>"
                                       + "<td>"
                                       + "<p> Estimado(a) " + nombre + "</p>"
                                       + "<p> Se le informa que la solicitud No." + solicitud.intIDSolicitud + ", ha sido concluida. Favor de dar seguimiento accesando al módulo de reservaciones </p>"
                                       + "Puede acceder desde  <a href='" + url + "'>aqui</a>"
                                       + "<p><b> ***Favor de no responder a este correo, el cual es enviado de un proceso automático.</b></p>"
                                       + " </td></tr> <tr style = 'background-image: linear-gradient(180deg,#ed6c1e,#f7a540); color: #FFF; text-align: center;' ><td><p> Todos los derechos reservados por control de viajes </p> </td> </tr></table> ";


                        string asuntoCorreo = "Solicitud No." + solicitud.intIDSolicitud + " concluida";
                        string error = enviarCorreo.FM_ENVIA_CORREO(asuntoCorreo, mensaje, correo);



                        if (!error.Equals(""))
                        {
                            throw new System.ArgumentException(error, "Original");
                            return 3;
                        }


                    }


                }
                catch (Exception e)
                {
                    throw new System.ArgumentException(e.Message, "Original");
                    return 0;
                }
            }
            else
            {
                exito = 2;
            }

            return exito;

        }
    }
}