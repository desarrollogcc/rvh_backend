﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Models;

namespace WebApi.Areas.Api.Models.Manager
{
    public class CatalogosManager
    {
        private GS_CCCONTROL_VIAJESEntities db;

        public CatalogosManager()
        {
            db = new GS_CCCONTROL_VIAJESEntities();

        }

        public List<ITipoVuelo> GetTipoVuelo()
        {

            List<ITipoVuelo> lstTipoVuelo = (from item in db.tblTipoVuelo
                                             where item.intDelete == 1
                                             select new ITipoVuelo
                                             {
                                                 intIDTipoVuelo = (int)item.intIDTipoVuelo,
                                                 chrDescripcion = item.chrDescripcion,

                                             }).ToList();
            db.Dispose();
            return lstTipoVuelo;

        }

        public ITipoVuelo SaveTipoVuelo(ITipoVuelo infoTipoVuelo)
        {
            tblTipoVuelo tipovuelo;
            if (infoTipoVuelo != null)
            {

                if (infoTipoVuelo.intIDTipoVuelo <= 0)
                {
                    tipovuelo = new tblTipoVuelo();
                    tipovuelo.chrDescripcion = infoTipoVuelo.chrDescripcion;
                    tipovuelo.intDelete = 1;
                    db.tblTipoVuelo.Add(tipovuelo);
                }
                else
                {
                    tipovuelo = (from item in db.tblTipoVuelo
                                 where item.intIDTipoVuelo == infoTipoVuelo.intIDTipoVuelo
                                 select item).FirstOrDefault();

                    if (tipovuelo != null)
                    {
                        tipovuelo.chrDescripcion = infoTipoVuelo.chrDescripcion;

                    }

                }
                db.SaveChanges();
                infoTipoVuelo.intIDTipoVuelo = (int)tipovuelo.intIDTipoVuelo;
            }

            return infoTipoVuelo;
        }

        public int DeleteTipovuelo(int intIDVuelo)
        {

            var eliminado = 0;


            var vuelo = (from item in db.tblTipoVuelo
                         where item.intIDTipoVuelo == intIDVuelo
                         select item).FirstOrDefault();

            if (intIDVuelo > 0)
            {
                if (vuelo != null)
                {
                    vuelo.intDelete = 0;
                    db.SaveChanges();
                }

                eliminado = 0;
            }
            else
            {
                eliminado = 1;
            }


            return eliminado;
        }

        /*Servicios de tipo de hanitación */
        public List<ITipoHabitacion> GetTipoHabitacion()
        {

            List<ITipoHabitacion> lstTipoHabitacion = (from item in db.tblTipoHabitacion
                                                       where item.intDelete == 1
                                                       select new ITipoHabitacion
                                                       {
                                                           intIDTipoHabitacion = item.intIDTipoHabitacion,
                                                           chrDescripcion = item.chrDescripcion

                                                       }).ToList();
            db.Dispose();
            return lstTipoHabitacion;

        }

        public ITipoHabitacion SaveTipoHabitacion(ITipoHabitacion infoTipoHabitacion)
        {
            tblTipoHabitacion tipoHabitacion;

            if (infoTipoHabitacion != null)
            {
                if (infoTipoHabitacion.intIDTipoHabitacion <= 0)
                {

                    tipoHabitacion = new tblTipoHabitacion();
                    tipoHabitacion.chrDescripcion = infoTipoHabitacion.chrDescripcion;
                    tipoHabitacion.intDelete = 1;
                    db.tblTipoHabitacion.Add(tipoHabitacion);

                }
                else
                {
                    tipoHabitacion = (from item in db.tblTipoHabitacion
                                      where item.intIDTipoHabitacion == infoTipoHabitacion.intIDTipoHabitacion
                                      select item).FirstOrDefault();

                    if (tipoHabitacion != null)
                    {
                        tipoHabitacion.chrDescripcion = infoTipoHabitacion.chrDescripcion;
                    }
                }

                db.SaveChanges();
                infoTipoHabitacion.intIDTipoHabitacion = tipoHabitacion.intIDTipoHabitacion;
            }

            return infoTipoHabitacion;
        }

        public int DeleteTipoHabitacion(int intIDTipoHabitacion)
        {

            int eliminado = 0;

            if (intIDTipoHabitacion > 0)
            {
                var tipohabitacion = (from item in db.tblTipoHabitacion
                                      where item.intIDTipoHabitacion == intIDTipoHabitacion
                                      select item).FirstOrDefault();

                if (tipohabitacion != null)
                {

                    tipohabitacion.intDelete = eliminado;
                    db.SaveChanges();
                }

            }
            else
            {
                eliminado = 0;
            }

            return eliminado;
        }
        /*Servicio de tipo de viaje*/
        public List<ITipoViaje> GetTipoViaje()
        {

            List<ITipoViaje> lstTipoViaje = (from item in db.tblTipoViaje
                                             where item.intDelete == 1
                                             select new ITipoViaje
                                             {
                                                 intIDTipoViajes = item.intIDTipoViaje,
                                                 chrDescripcion = item.chrDescripcion

                                             }).ToList();
            db.Dispose();
            return lstTipoViaje;

        }

        public ITipoViaje SaveTipoViaje(ITipoViaje infoTipoViaje)
        {

            tblTipoViaje tipoviajes;

            if (infoTipoViaje != null)
            {
                if (infoTipoViaje.intIDTipoViajes <= 0)
                {
                    tipoviajes = new tblTipoViaje();
                    tipoviajes.chrDescripcion = infoTipoViaje.chrDescripcion;
                    tipoviajes.intDelete = 1;
                    db.tblTipoViaje.Add(tipoviajes);
                }
                else
                {
                    tipoviajes = (from item in db.tblTipoViaje
                                  where item.intIDTipoViaje == infoTipoViaje.intIDTipoViajes
                                  select item).FirstOrDefault();
                    if (tipoviajes != null)
                    {
                        tipoviajes.chrDescripcion = infoTipoViaje.chrDescripcion;
                    }
                }
                db.SaveChanges();
                infoTipoViaje.intIDTipoViajes = tipoviajes.intIDTipoViaje;
            }

            return infoTipoViaje;
        }

        public int DeteleTipoviajes(int intIDTipoViaje)
        {

            int eliminado = 0;
            if (intIDTipoViaje > 0)
            {
                var tipoViajes = (from item in db.tblTipoViaje
                                  where item.intIDTipoViaje == intIDTipoViaje
                                  select item).FirstOrDefault();

                if (tipoViajes != null)
                {
                    tipoViajes.intDelete = 0;
                    db.SaveChanges();

                }

            }
            else
            {

                eliminado = 1;
            }

            return eliminado;
        }
        /*Servicio de Estatus solicitud*/
        public List<IEstatusSolicitud> GetEstatusSolicitud()
        {
            List<IEstatusSolicitud> lstEstatusSolicitud = (from item in db.tblEstatusSolicitud
                                                           where item.intDelete == 1
                                                           select new IEstatusSolicitud
                                                           {
                                                               intIDEstatus = item.intIDEstatus,
                                                               chrEstatus = item.chrEstatus,
                                                               chrDescripcion = item.chrDescripcion,
                                                               chrColor = item.chrColor
                                                           }).ToList();

            db.Dispose();
            return lstEstatusSolicitud;
        }

        public IEstatusSolicitud SaveEstatusSolicitud(IEstatusSolicitud infoEstatus)
        {
            tblEstatusSolicitud estatussolicitud;
            if (infoEstatus != null)
            {
                if (infoEstatus.intIDEstatus <= 0)
                {
                    estatussolicitud = new tblEstatusSolicitud();
                    estatussolicitud.chrEstatus = infoEstatus.chrEstatus;
                    estatussolicitud.chrDescripcion = infoEstatus.chrDescripcion;
                    estatussolicitud.chrColor = infoEstatus.chrColor;
                    estatussolicitud.intDelete = 1;
                    db.tblEstatusSolicitud.Add(estatussolicitud);

                }
                else
                {
                    estatussolicitud = (from item in db.tblEstatusSolicitud
                                        where item.intIDEstatus == infoEstatus.intIDEstatus
                                        select item).FirstOrDefault();

                    if (estatussolicitud != null)
                    {
                        estatussolicitud.chrEstatus = infoEstatus.chrEstatus;
                        estatussolicitud.chrDescripcion = infoEstatus.chrDescripcion;
                        estatussolicitud.chrColor = infoEstatus.chrColor;

                    }
                }
                db.SaveChanges();
                infoEstatus.intIDEstatus = estatussolicitud.intIDEstatus;
            }

            return infoEstatus;
        }
        public List<IEstatusSolicitud> GetEstatusPorPerfiles()
        {

            List<IEstatusSolicitud> lstEsatusPorPerfil = (from item in db.tblEstatusSolicitud
                                                          where item.intDelete == 1
                                                          select new IEstatusSolicitud
                                                          {
                                                              intIDEstatus = item.intIDEstatus,
                                                              chrDescripcion = item.chrDescripcion,
                                                              chrEstatus = item.chrEstatus,
                                                              chrColor = item.chrColor,
                                                              chrEsUsuario = item.chrEsUsuario,
                                                              chrEsAutorizador = item.chrEsAutorizador,
                                                              chrEsAuditor = item.chrEsAuditor,
                                                              chrEsAdministrador = item.chrEsAdministrador
                                                          }).ToList();

            return lstEsatusPorPerfil;
        }

        public int DeleteEstatusSolicitud(int intIDEstatusSolicitud)
        {

            int eliminado = 0;

            if (intIDEstatusSolicitud > 0)
            {

                var estatussolcitud = (from item in db.tblEstatusSolicitud
                                       where item.intIDEstatus == intIDEstatusSolicitud
                                       select item).FirstOrDefault();

                if (estatussolcitud != null)
                {

                    estatussolcitud.intDelete = 0;
                    db.SaveChanges();
                }
            }
            else
            {
                eliminado = 1;
            }

            return eliminado;

        }
        /*Servciio de tipo de solicitud*/
        public List<ITipoSolicitud> GetTipoSolicitud()
        {
            List<ITipoSolicitud> lstEstatusSolicitud = (from item in db.tblTipoSolicitud
                                                        select new ITipoSolicitud
                                                        {
                                                            intIDTipoSolicitud = item.intIDTipoSolicitud,
                                                            chrDescripcion = item.chrDescripcion
                                                        }).ToList();

            db.Dispose();
            return lstEstatusSolicitud;
        }

        public ITipoSolicitud SaveTipoSolicitud(ITipoSolicitud infoTipoSolicitud)
        {
            tblTipoSolicitud tipoSolicitudes;
            if (infoTipoSolicitud != null)
            {
                if (infoTipoSolicitud.intIDTipoSolicitud <= 0)
                {
                    tipoSolicitudes = new tblTipoSolicitud();
                    tipoSolicitudes.chrDescripcion = infoTipoSolicitud.chrDescripcion;
                    db.tblTipoSolicitud.Add(tipoSolicitudes);
                }
                else
                {
                    tipoSolicitudes = (from item in db.tblTipoSolicitud
                                       where item.intIDTipoSolicitud == infoTipoSolicitud.intIDTipoSolicitud
                                       select item).FirstOrDefault();
                    if (tipoSolicitudes != null)
                    {
                        tipoSolicitudes.chrDescripcion = infoTipoSolicitud.chrDescripcion;

                    }
                }
                db.SaveChanges();
                infoTipoSolicitud.intIDTipoSolicitud = tipoSolicitudes.intIDTipoSolicitud;
            }

            return infoTipoSolicitud;
        }

        /*Servicios de cuenta bacaria */

        public IDatosBancarios SaveCuentaBancaria(IDatosBancarios infoDatosBancarios)
        {

            tblDatosBancarios datosCuentasBancaria;
            if (infoDatosBancarios != null)
            {

                if (infoDatosBancarios.intIDDatosBancarios <= 0)
                {
                    datosCuentasBancaria = new tblDatosBancarios();
                    datosCuentasBancaria.chrNombreBanco = infoDatosBancarios.chrNombreBanco;
                    datosCuentasBancaria.chrNombreBeneficiario = infoDatosBancarios.chrNombreBeneficiario;
                    datosCuentasBancaria.chrCuenta = infoDatosBancarios.chrCuenta;
                    datosCuentasBancaria.chrAnioMesVencimiento = infoDatosBancarios.chrAnioMesVencimiento;
                    datosCuentasBancaria.intDelete = 1;
                    db.tblDatosBancarios.Add(datosCuentasBancaria);
                }
                else
                {
                    datosCuentasBancaria = (from _datosCuentasBancaria in db.tblDatosBancarios
                                            where _datosCuentasBancaria.intIDDatosBancarios == infoDatosBancarios.intIDDatosBancarios
                                            select _datosCuentasBancaria).FirstOrDefault();
                    if (datosCuentasBancaria != null)
                    {
                        datosCuentasBancaria.chrNombreBanco = infoDatosBancarios.chrNombreBanco;
                        datosCuentasBancaria.chrNombreBeneficiario = infoDatosBancarios.chrNombreBeneficiario;
                        datosCuentasBancaria.chrCuenta = infoDatosBancarios.chrCuenta;
                        datosCuentasBancaria.chrAnioMesVencimiento = infoDatosBancarios.chrAnioMesVencimiento;
                    }

                }
                db.SaveChanges();
                infoDatosBancarios.intIDDatosBancarios = datosCuentasBancaria.intIDDatosBancarios;
            }

            return infoDatosBancarios;
        }

        public List<IDatosBancarios> ConsultaDatosBancarios()
        {
            List<IDatosBancarios> datosBancarios = (from _datosBancarios in db.tblDatosBancarios
                                                    where _datosBancarios.intDelete == 1
                                                    select new IDatosBancarios
                                                    {
                                                        intIDDatosBancarios = _datosBancarios.intIDDatosBancarios,
                                                        chrCuenta = _datosBancarios.chrCuenta,
                                                        chrNombreBanco = _datosBancarios.chrNombreBanco,
                                                        chrNombreBeneficiario = _datosBancarios.chrNombreBeneficiario,
                                                        chrAnioMesVencimiento = _datosBancarios.chrAnioMesVencimiento,
                                                        chrNumSeguridad = _datosBancarios.chrNumSeguridad
                                                    }
                                                    ).ToList();

            db.Dispose();

            return datosBancarios;
        }

        public int DeleteCuentaBancaria(int intIDCuentaBancaria)
        {

            int eliminado = 0;
            if (intIDCuentaBancaria > 0)
            {
                var datosbancarios = (from _datosBancarios in db.tblDatosBancarios
                                      where _datosBancarios.intIDDatosBancarios == intIDCuentaBancaria
                                      select _datosBancarios).FirstOrDefault();

                if (datosbancarios != null)
                {
                    datosbancarios.intDelete = 0;
                    db.SaveChanges();

                }

            }
            else
            {
                eliminado = 1;
            }

            return eliminado;
        }

        /* Servicio de guarda datos fiscales */

        public IDatosFiscales SaveDatosFiscales(IDatosFiscales infoDatosFiscales)
        {
            tblDatosFiscales datosFiscales;
            if (infoDatosFiscales != null)
            {
                if (infoDatosFiscales.intIDDatosFiscales <= 0)
                {
                    datosFiscales = new tblDatosFiscales();
                    datosFiscales.chrRazonSocial = infoDatosFiscales.chrRazonSocial;
                    datosFiscales.chrRFC = infoDatosFiscales.chrRFC;
                    datosFiscales.chrDireccion = infoDatosFiscales.chrDireccion;
                    datosFiscales.chrCiudad = infoDatosFiscales.chrCiudad;
                    db.tblDatosFiscales.Add(datosFiscales);
                }
                else
                {
                    datosFiscales = (from _datosFiscales in db.tblDatosFiscales
                                     where _datosFiscales.intIDDatosFiscales == infoDatosFiscales.intIDDatosFiscales
                                     select _datosFiscales).FirstOrDefault();

                    if (datosFiscales != null)
                    {
                        datosFiscales.chrRazonSocial = infoDatosFiscales.chrRazonSocial;
                        datosFiscales.chrRFC = infoDatosFiscales.chrRFC;
                        datosFiscales.chrDireccion = infoDatosFiscales.chrDireccion;
                        datosFiscales.chrCiudad = infoDatosFiscales.chrCiudad;
                    }

                }
                db.SaveChanges();

                infoDatosFiscales.intIDDatosFiscales = datosFiscales.intIDDatosFiscales;
            }

            return infoDatosFiscales;
        }

        public List<IDatosFiscales> GetDatosFiscales()
        {

            List<IDatosFiscales> lstDatosFiscales = (from _datosFiscales in db.tblDatosFiscales
                                                     select new IDatosFiscales
                                                     {
                                                         intIDDatosFiscales = _datosFiscales.intIDDatosFiscales,
                                                         chrRazonSocial = _datosFiscales.chrRazonSocial,
                                                         chrRFC = _datosFiscales.chrRFC,
                                                         chrDireccion = _datosFiscales.chrDireccion,
                                                         chrCiudad = _datosFiscales.chrCiudad
                                                     }).ToList();

            db.Dispose();
            return lstDatosFiscales;

        }

        public IConfiguracion SaveConfiguracion(IConfiguracion data)
        {
            sbyte intDelete = Convert.ToSByte(true);
            tblConfiguracion configuracion;
            string stringModulo = "clogin";
            if (data != null)
            {
                if (data.intIDConfiguracion <= 0)
                {
                    configuracion = new tblConfiguracion();
                    configuracion.chrTitulo = data.chrTitulo;
                    configuracion.chrSubTitulo = data.chrSubtitulo;
                    configuracion.chrModulo = stringModulo;
                    configuracion.intDelete = intDelete;
                    db.tblConfiguracion.Add(configuracion);

                }
                else
                {
                    configuracion = (from item in db.tblConfiguracion
                                     where item.intIDConfiguraccion == data.intIDConfiguracion
                                     && item.chrModulo == stringModulo
                                     select item).FirstOrDefault();
                    if (configuracion != null)
                    {
                        configuracion.chrTitulo = data.chrTitulo;
                        configuracion.chrSubTitulo = data.chrSubtitulo;
                        configuracion.chrModulo = stringModulo;
                    }

                }
                db.SaveChanges();
                data.intIDConfiguracion = configuracion.intIDConfiguraccion;
            }
            return data;
        }

        public List<IConfiguracion> GetConfiguracion()
        {
            string chrModulo = "clogin";
            List<IConfiguracion> configuracion = (from item in db.tblConfiguracion
                                                  where item.chrModulo == chrModulo
                                                  select new IConfiguracion
                                                  {
                                                      intIDConfiguracion = item.intIDConfiguraccion,
                                                      chrTitulo = item.chrTitulo,
                                                      chrSubtitulo = item.chrSubTitulo,

                                                  }).ToList();

            db.Dispose();
            return configuracion;
        }

        public string GetRutaHome()
        {
            string url = WebConfigurationManager.AppSettings["UrlHome"];

            return url;
        }

        public ISemaforo SaveSemaforo(ISemaforo data)
        {
            sbyte intDelete = Convert.ToSByte(true);
            tblSemaforo semaforo;
            if (data != null)
            {
                if (data.intIDSemaforo <= 0)
                {
                    semaforo = new tblSemaforo();
                    semaforo.chrColor = data.chrColor;
                    semaforo.chrTiempo = data.chrTiempo;
                    semaforo.chrOperador = data.chrOperador;
                    semaforo.intDelete = 1;
                    db.tblSemaforo.Add(semaforo);
                }
                else
                {
                    semaforo = (from item in db.tblSemaforo
                                where item.intIDSemaforo == data.intIDSemaforo
                                select item).FirstOrDefault();

                    semaforo.chrColor = data.chrColor;
                    semaforo.chrTiempo = data.chrTiempo;
                    semaforo.chrOperador = data.chrOperador;
                }
                db.SaveChanges();
                data.intIDSemaforo = semaforo.intIDSemaforo;

            }
            return data;
        }
        public List<ISemaforo> GetSemaforo()
        {
            List<ISemaforo> semaforo = (from item in db.tblSemaforo
                                        where item.intDelete == 1
                                        select new ISemaforo
                                        {
                                            intIDSemaforo = item.intIDSemaforo,
                                            chrColor = item.chrColor,
                                            chrTiempo = item.chrTiempo,
                                            chrOperador = item.chrOperador
                                        }).ToList();

            return semaforo;
        }

        public int DeleteSemaforo(int intIDSemaforo)
        {
            int eliminado = 0;
            if (intIDSemaforo > 0)
            {
                var semaforo = (from item in db.tblSemaforo
                                where item.intIDSemaforo == intIDSemaforo
                                select item).FirstOrDefault();
                if (semaforo != null)
                {
                    semaforo.intDelete = 0;
                    db.SaveChanges();
                }
            }
            else
            {
                eliminado = 1;

            }
            return eliminado;
        }
    }
}