﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IDatosFiscales
    {
        public long intIDDatosFiscales;
        public string chrRazonSocial;
        public string chrRFC;
        public string chrDireccion;
        public string chrCiudad;
    }
}