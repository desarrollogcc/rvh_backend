﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IReporteSolicitudAutorizados
    {
        public int intIDSolicitud;
        public DateTime? dtdFechaEnvioHora;
        public DateTime? dtdFechaCotizacion;
        public DateTime? dtdFechaAutorizacion;
        public int? intNumSolicitud;
        public string chrNombreSolicitante;
        public string chrApellidoPaternoSolicitante;
        public string chrApellidoMaternoSolicitante;
        public string chrTipoSolicitud;
        public string chrMotivoViaje;
        public string chrMotivoHotel;
        public int intIDEstatus;
        public string chrEstatus;
        public int? intNoCotizaciones;
        public int intFKIDDetalleSolicitudViaje;
        public int intFKIDDetalleSolicitudHotel;
        public int? intFKIDTipoSolicitud;


    }
}