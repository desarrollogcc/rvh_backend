﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class ITipoViaje
    {
        public long intIDTipoViajes;
        public string chrDescripcion;
        public Boolean intDelete { get; set;} 
    }
}