﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class INotificacionesUsuarios
    {
        public int intIDSolicitud;
        public string chrNombre;
        public string chrApellidPaterno;
        public string chrApellidoMaterno;
        public DateTime? dtdFechaSolicitud;
        public int intFKIdEstatus;
        public string chrEstatus;
        public string chrColor;
    }
}