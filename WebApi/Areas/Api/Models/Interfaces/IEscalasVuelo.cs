﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IEscalasVuelo
    {
        public long intIDOperacionesSolicitudEscalaAvion;
        public int? intFKIDOperacionesSolicitudAvion;
        //public string chrNoEscalas;
        public string chrClaveOrigenEscala;
        public string chrClaveDestinoEscala;
        public DateTime? dtdFechaSalidaEscala;
        //public DateTime dtdFechaRegresoEscala;
        public bool intDetele;
    }
}