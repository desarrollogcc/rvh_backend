﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IResumenVuelo
    {
        public int? intIDSolicitud;
        public string chrNoEmpleado;
        public string chrNombreEmpleado;
        public string chrApellidoPaterno;
        public string chrApellidoMaterno;
        public string chrCentroCosto;
        public string chrTipoViaje;
        public string chrTipoVuelo;
        public string chrAerolinea;
        public DateTime? dtdFechaNacimiento = null;
        public string chrOrigen;
        public string chrDestino;
        public DateTime? dtdFechaSalidad = null;
        public DateTime? dtdFechaRegreso = null;
        public int? intIDEstatus;
        public string chrEstatus;
        public decimal? decImporte;
        public int? intFKIDDetalleSolicitudViaje;
        public int? intFKIDTipoSolicitud;
    }
}