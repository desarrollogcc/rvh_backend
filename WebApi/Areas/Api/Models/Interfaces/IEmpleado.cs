﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IEmpleado
    {
        public string intIDEmpleado;
        public string chrCodigoEmpleado;
        public string chrNombre;
        public string chrApellidoPaterno;
        public string chrApellidoMaterno;
        public DateTime? dtdFechaNacimiento;
        public string chrPassword;
        public DateTime? dtdFechaAlta;
        public DateTime? dtdFechaBaja;
        public string chrCorreo;
        public int? intIDPerfil;
        public string chrEstatus;
        public string chrSociedad;
        public string chrPuesto;
        public string chrDepartamento;
        public string chrCentroCosto;
        public string chrPasaporte;
        public string chrSession;
        public int? intIDDetalleHuespedes;
        public int? intIDDetalleViajeros;
        public int? chkEliminar = 0;
        public string intFKIDEmpleadoAutorizador;

        public List<IEmpleado> empledoAutorizador;
        public List<IPerfil> perfilEmpleado;
        public List<IPantallaUsuario> pantallasUsuarios;
    }

}