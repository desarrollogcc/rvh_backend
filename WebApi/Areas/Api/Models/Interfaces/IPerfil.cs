﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IPerfil
    {
        public long intIDPerfil;
        public string chrDescripcion;
        public string chrClave;
    }
}