﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IDatosBancarios
    {
        public long intIDDatosBancarios;
        public string chrNombreBanco;
        public string chrCuenta;
        public string chrNombreBeneficiario;
        public string chrNumSeguridad;
        public string chrAnioMesVencimiento;
        public bool intDelete;
    }
}