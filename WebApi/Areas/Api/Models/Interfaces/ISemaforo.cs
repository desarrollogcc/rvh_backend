﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class ISemaforo
    {
        public long intIDSemaforo;
        public int? chrTiempo;
        public string chrColor;
        public string chrOperador;
        public bool intDelete;
    }
}