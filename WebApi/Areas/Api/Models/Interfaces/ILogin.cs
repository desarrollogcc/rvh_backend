﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class ILogin
    {
        private string chrCodigoEmpleado;
        private string chrPassword;

        public string ChrCodigoEmpleado
        {
            get
            {
                return chrCodigoEmpleado;
            }

            set
            {
                chrCodigoEmpleado = value;
            }
        }

        public string ChrPassword
        {
            get
            {
                return chrPassword;
            }

            set
            {
                chrPassword = value;
            }
        }
    }
}