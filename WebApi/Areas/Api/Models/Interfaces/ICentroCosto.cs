﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class ICentroCosto
    {
        public string KOKRS;
        public string KOSTL;
        public string DATBI;
        public string DATAB;
        public string BUKRS;
        public string GSBER;
        public string PRCTR;
        public string OBJNR;
        public string LTEXT;
    }
}