﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IProloginResult
    {
        public string autorizacion { get; set; }
        public string SID { get; set; }
        public string idUsuario { get; set; }
        public string contrasena { get; set; }
        public string iDTipoUsuario { get; set; }
        public string correoElectronico { get; set; }
        public string activo { get; set; }
        public System.DateTime fechaRegistro { get; set; }
        public string privilegio { get; set; }
        public string nombreUsuario { get; set; }
        public string noEmpleado { get; set; }
        public string IngSesion { get; set; }
        public string Lectura { get; set; }
        public string idUsuarioAutoriza { get; set; }
        public string CentroCostos { get; set; }
        public string Puesto { get; set; }
        public string Descripcion { get; set; }
    }
}