﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IEstatusSolicitud
    {
        public long intIDEstatus { get; set; }
        public string chrEstatus { get; set; }
        public string chrDescripcion { get; set; }
        public string chrColor { get; set; }
        public Boolean intDelete { get; set;  }
        public string chrEsUsuario;
        public string chrEsAdministrador;
        public string chrEsAutorizador;
        public string chrEsAuditor;

    }
}