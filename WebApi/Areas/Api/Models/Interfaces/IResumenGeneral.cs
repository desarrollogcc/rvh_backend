﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IResumenGeneral
    {
        public int intIDSolicitud;
        public string chrCompania;
        public string chrNoEmpleado;
        public string chrNombreEmpleado;
        public string chrApellidoPaterno;
        public string chrApellidoMaterno;
        public string chrDepartamento;
        public string chrTipoSolicitud;
        public string chrEstatus;
        public DateTime? dtdFechaSolicitud = null;
        public string chrMotivoViaje;
        public string chrMotivoHotel;
        public decimal? decImporteHotel;
        public decimal? decImporteVuelo;
        public string chrRazonSocialVuelo;
        public string chrRFCVuelo;
        public string chrRazonSocialHotel;
        public string chrRFCHotel;

    }
}