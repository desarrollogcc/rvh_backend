﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IReservacionHotel
    {
        public long intIDReservacionHotel;
        public int? intFKIDOperacionesSolicitudHotel;
        public int? intNoOpcion;
        public string chrContabilizada;
        public string chrTipoCompra;
        public string chrCodigoReservacion;
        public int? chrCargoExtraHotel;
    }
}