﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IDetalleSolicitudViaje 
    {
       

        public long intIDDetalleSolicitudViaje;
        public int? intFKIDSolicitud;
        public int? intFKIDTipoVuelo;
        public string chrClaveOrigen;
        public string chrClaveDestino;
        public DateTime? dtdFechaSalidad;
        public DateTime? dtdFechaRegreso;
        public Decimal? chrPresupuesto;
        public string chrComentarios;
        public int? intFKIDTipoViaje;
        public string chrCentroCostoViaje;
        public string chrAerolinea;
        public string chrMotivoViaje;
        public int? chrColaboradorSelect;

        public List<IEmpleado> listaEmpleados;

        public ITipoVuelo lstTipoVuelo;
        public ITipoViaje lstTipoViaje;
    }
}