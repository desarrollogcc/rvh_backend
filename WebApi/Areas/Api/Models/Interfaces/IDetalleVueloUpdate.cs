﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IDetalleVueloUpdate
    {
        /*detalle de solcicitud*/
        public int intIDSolicitud;
        public int intFolio;
        public DateTime dtdFechaSolicitud;
        public DateTime dtdFechaNacimiento;
        public int intNoSolicitante;
        public int intNoEmpleadoCreador;
        public int intFKIDEstatus;
        public string chrComentariosSolicitud;
        public int intFKIDTipoSolicitud;
        public int intIDAutorizadorSuplente;

        /*Detalle de solicitud viaje */
        public int intIDDetalleSolicitudViaje;
        public int intFKIDTipoVuelo;
        public int intFKIDTipoViaje;
        public int intFKIDSolicitud;
        public DateTime dtdFechaSalidadVuelo;
        public DateTime dtdFechaRegresoVuelo;
        public string chrPresupuesto;
        public string chrMotivoViaje;
        public string chrComentariosViaje;
        public string chrClaveOrigenVuelo;
        public string chrClaveDestinoVuelo;
        public string chrCentroCostoViaje;
        public string chrAerolinea;

        /*Detalle de Hotel*/
        public int intIDDetalleSolicitudHotel;
        public int intFKIDSolicitudHotel;
        public string chrClaveOrigenHotel;
        public string chrClaveDestinoHotel;
        public DateTime dtdFechaSalida;
        public DateTime dtdFechaRegresoHotel;
        public decimal chrPresupuestohotel;
        public string chrComentariosHotel;
        public int intFKIDTipoHabitacion;
        public string chrEsHotel;
        public int intNoHabitaciones;
        public string chrCentroCostoHospedaje;
        public int chrNochesReservadas;

        /*Empleado*/
        public string chrCodigoEmpleado;
        public string chrNombre;
        public string chrApellidoPaterno;
        public string chrApellidoMaterno;


        //public List<ISolicitud> solicitud;
        //public List<IDetalleViajeros> viajeros;
        public List<IDetalleSolicitudViaje> detalleViajes;
        
        public IDetalleHuespedes detalleHuespedes;


    }
}