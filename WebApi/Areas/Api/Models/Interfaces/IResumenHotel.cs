﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IResumenHotel
    {
        public int intIDSolicitud;
        public string chrNoEmpleado;
        public string chrNombreEmpleado;
        public string chrApellidoPaterno;
        public string chrApellidoMaterno;
        public string chrCentroCostoHotel;
        public string chrHotel;
        public int? chrHabitaciones;
        public string chrTipoHabitacion;
        public string chrOrigen;
        public string chrDestino;
        public DateTime? dtdFechaSalidad;
        public DateTime? dtdFechaRegreso;
        public int? chrNochesReservadas;
        public int? intIDEstatus;
        public string chrEstatus;
        public decimal? decImporte;
        public int? intFKIDDetalleSolicitudHotel;
        public int? intFKIDTipoSolicitud;
    }
}