﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IConfiguracion
    {
        public int intIDConfiguracion;
        public string chrTitulo;
        public string chrSubtitulo;
        public string chrModulo;
        public bool intDelete;
    }
}