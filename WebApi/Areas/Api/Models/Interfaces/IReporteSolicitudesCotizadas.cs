﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IReporteSolicitudesCotizadas
    {
        public int intIDSolicitud;
        public DateTime? dtdFechaSolicitud;
        public string chrNombreSolicitante;
        public string chrApellidoPaterno;
        public string chrApellidoMaterno;
        public string chrTipoSolicitud;
        public int intIDEstatus;
        public string chrEstatus;
        public string chrColorEstatus;
        public DateTime? dtdFechaRegresoVuelo;
        public DateTime? dtdFechaSalidaVuelo;
        public DateTime? dtdFechaSalidaHospedaje;
        public DateTime? dtdFechaRegresoHospedaje;
        public int? intNumCotizadas;
        public int intFKIDDetalleSolicitudViaje;
        public int intFKIDDetalleSolicitudHospedaje;
        public int? intFKIDTipoSolicitud;

    }
}