﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IReservacionViaje
    {
        public long intIDReservacionViaje;
        public int? intFKIDOperacionesSolicitudesAvion;
        public int? intNoOpcion;
        public string chrContabilizada;
        public string chrTipoCompra;
        public string chrCodigoReservacion;
        public int? chrCargaExtraViaje;
    }
}