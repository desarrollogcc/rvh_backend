﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IOperacionesSolicitudHotel
    {
        public long intIDOperacionesSolicitudHotel;
        public int? intFKIDDetalleSolicitudHotel;
        public int? intNoOpcion;
        public string chrCodigoConfirmacion;
        public DateTime? dtdFechaRespuesta;
        public string chrLineaHotel;
        public DateTime? dtdFechaEntrada;
        public DateTime? dtdFechaSalidad;
        public int? intNoHabitaciones;
        public int? intFKIDTipoHabitacion;
        public decimal decCosto;
        public int? intAceptada;
        public int? intAutorizada;
        public int? intReservada;
        public string chrImagen;
        public string chrRFC;
        public decimal chrCostoExtraHotel;
    }
}