﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IEmpleadoIntegracion
    {
     
        public string empleado;
        public string nombre;
        public string razonsocial;
        public string puesto;
        public string departamento;
        public string centrocosto;
        public DateTime? fechanac =  null ;
    }
}