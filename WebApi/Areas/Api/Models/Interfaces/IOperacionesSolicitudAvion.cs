﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IOperacionesSolicitudAvion
    {
        public long intIDOperacionesSolicitudesAvion;
        public int? intFKDetalleSolicitudViaje;
        public int? intNoOpcion;
        public string chrClaveReservacion;
        public DateTime? dtdFechaRespuesta;
        public string chrLineaArea;
        public int? intFKIDTipoVuelo;
        public string chrClaveOrigen;
        public string chrClaveDestino;
        public DateTime? dtdFechaSalida;
        public DateTime? dtdFechaRegreso;
        public decimal decCosto;
        public int? chrEscala;
        public int? chrNoEscalas;
        public string chrClaveOrigenEscala;
        public string chrClaveDestinoEscala;
        public DateTime dtdFechaSalidadEscala;
        public DateTime dtdFechaRegresoEscala;
        public int? chrAceptada;
        public int? chrAutorizada;
        public int? chrReservada;
        public string chrRFC;
        public decimal chrCostoExtraAvion;
        public List<IEscalasVuelo> escalasVuelo;
    }
}