﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class ISolicitud
    {


        public long intIDSolicitud { get; set; }
        public int? intFolio { get; set; }
        public DateTime dtdFechaSolicitud { get; set; }
        public string intNoSolicitante { get; set; }
        public string intNoEmpleadoCreador { get; set; }
        public int? intFKIDEstatus { get; set; }
        public string chrComentarios { get; set; }
        public int? intFKIDTipoSolicitud { get; set; }
        public string intIDAutorizadorSuplente { get; set; }
        public DateTime dtdFechaAutorizada { get; set; }
        public DateTime dtdFechaCotizada { get; set; }
        public int? intNumCotizadas { get; set; }
        //public List<IAcompaniante> listaAcompaniantes;
        public List<IDetalleSolicitudViaje> detalleSolicitudViaje;

        public List<IDetalleSolicitudHotel> detalleSolicitudHotel;

        public IEstatusSolicitud estatusSolicitud;

        public ITipoSolicitud tipoSoliciud;

        public IEmpleado empleadoSolicitante;



    }

}