﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class ICargaArchivos
    {
        public long intIDImagenesCotizar;
        public string chrNombre;
        public string chrPath;
        public int? intFKIDTipoSolicitud;
        public int? intFKIDSolicitud;
        public int? intNoOpcionAutorizador;
        public int? intNoOpcionUsuario;
        public int? intEsRechazado;
        public string chrContentType;
        public string chrTipoArchivo;
        public int intFKIDDetalleSolicitudVueloHotel;
    }
}