﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IModulos
    {
        public int idSistema;
        public int idModulo;
        public string descripcionModulo;
        public string idModuloPadre;
        public string pagina;
        public string imagenIcono;
        public int orden1;
        public int orden2;
        public int orden3;
        public int orden4;
        public int orden5;

    }
}