﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IDetalleSolicitudHotel
    {
        public long intIDDetalleSolicitudHotel;
        public int? intFKIDSolicitud;
        public string chrClaveDestino;
        public string chrClaveOrigen;
        public DateTime? dtdFechaSalidad;
        public DateTime? dtdFechaRegreso;
        public decimal? chrPresuspuesto;
        public string chrComentarios;
        public int? intFKIDTipoHabitacion;
        public string chrEsHotel;
        public int? intNoHabitaciones;
        public string chrCentroCostoHospedaje;
        public int? chrNochesReservadas;
        public int? intFKIDTipoVuelo;
        public int? intFKIDTipoViaje;

        public IList<IEmpleado> lstDtEmpleadoHuespedes;

        public ITipoHabitacion lstTipoHabitacion;
    
        
    }
}