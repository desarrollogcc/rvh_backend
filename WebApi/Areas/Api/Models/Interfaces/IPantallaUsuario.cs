﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Areas.Api.Models.Interfaces
{
    public class IPantallaUsuario
    {
        public long   intIDPantallas;
        public string intIDModulo;
        public string intIDSistema;
        public string chrDescripcionModulo;
        public string intIDModuloPadre;
        public string chrUrl;
        public int    intDelete;
        public string chrImagenIcono;
        public string chrValue;
        public string chrMenuPrincipal;
        public string chrCodigoEmpleado;
    }
}