﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using WebApi.Areas.Api.Models;
using WebApi.Areas.Api.Models.Manager;

namespace WebApi.Areas.Api.Controllers
{
    public class IntegrationWebServicesController : SessionController
    {
        IntegrationWebServicesManager integrationWebService;

        public IntegrationWebServicesController()
        {
            integrationWebService = new IntegrationWebServicesManager();
        }

        [HttpPost]
        [Route("Api/WebServices/GetCentrosCosto/")]
        public async Task<HttpResponseMessage> GetCentrosCosto(dynamic json)
        {
            HttpResponseMessage response;
            string urlWSControl = WebConfigurationManager.AppSettings["UrlWSContol"];
            string userWSControl = WebConfigurationManager.AppSettings["userWSContol"];
            string passWSControl = WebConfigurationManager.AppSettings["passWSContol"];
            string parametroCentroCosto = "";
            string credenciales = userWSControl + ":" + passWSControl;

            string chrCentroCostos = json.chrCentroCostos;

            if (json.chrCentroCostos != null)
            {
                parametroCentroCosto = "?id=" + chrCentroCostos;
            }

            var TARGETURL = urlWSControl + "api/ceco" + parametroCentroCosto;

            HttpClientHandler handler = new HttpClientHandler()
            {
                Proxy = new WebProxy(urlWSControl + "api/ceco" + parametroCentroCosto),
                UseProxy = true,
            };

            //Console.WriteLine("GET: + " + TARGETURL);
            System.Diagnostics.Debug.WriteLine("GET: + " + TARGETURL);
            // ... Use HttpClient.            
            HttpClient client = new HttpClient(handler);

            var byteArray = Encoding.ASCII.GetBytes(credenciales);
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            response = await client.GetAsync(TARGETURL);
            HttpContent content = response.Content;

            // ... Check Status Code                                

            System.Diagnostics.Debug.WriteLine("Response StatusCode: " + (int)response.StatusCode);
            // ... Read the string.
            string result = await content.ReadAsStringAsync();

            // ... Display the result.
            if (result != null)
            {

                System.Diagnostics.Debug.WriteLine(result);
            }

            return response;
        }

        [HttpPost]
        [Route("Api/WebServices/GetEmpleados/")]
        public async Task<HttpResponseMessage> GetEmpleados(dynamic json)
        {
            HttpResponseMessage response;

            string urlWSControl = WebConfigurationManager.AppSettings["UrlWSContol"];
            string userWSControl = WebConfigurationManager.AppSettings["userWSContol"];
            string passWSControl = WebConfigurationManager.AppSettings["passWSContol"];
            string parametros = "";
            string credenciales = userWSControl + ":" + passWSControl;
            int codigoEmpleado = json.chrCodigoEmpleado;

            if (codigoEmpleado != 0 || codigoEmpleado > 0)
            {
                parametros = "?id=" + codigoEmpleado;
            }
            var TARGETURL = urlWSControl + "api/empleado" + parametros;

            HttpClientHandler handler = new HttpClientHandler()
            {
                Proxy = new WebProxy(urlWSControl + "api/empleado" + parametros),
                UseProxy = true,
            };

            //Console.WriteLine("GET: + " + TARGETURL);
            System.Diagnostics.Debug.WriteLine("GET: + " + TARGETURL);
            // ... Use HttpClient.            
            HttpClient client = new HttpClient(handler);

            var byteArray = Encoding.ASCII.GetBytes(credenciales);
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            response = await client.GetAsync(TARGETURL);
            HttpContent content = response.Content;

            // ... Check Status Code                                

            System.Diagnostics.Debug.WriteLine("Response StatusCode: " + (int)response.StatusCode);
            // ... Read the string.
            string result = await content.ReadAsStringAsync();

            // ... Display the result.
            if (result != null)
            {
                EmpleadoManager empleado = new EmpleadoManager();
                empleado.ActualizaEmpleado(result);
                System.Diagnostics.Debug.WriteLine(result);
            }

            return response;

        }

    }
}