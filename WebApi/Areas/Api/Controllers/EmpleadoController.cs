﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApi.Areas.Api.Models;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Areas.Api.Models.Manager;

namespace WebApi.Areas.Api.Controllers
{
    public class EmpleadoController : SessionController
    {
        EmpleadoManager empleadosManager;
        public EmpleadoController()
        {
            empleadosManager = new EmpleadoManager();
        }
        // GET: Api/Empleado
        [HttpGet]
        [Route("Api/Empleados/GetEmpleados/")]
        public IHttpActionResult GetEmpleados()
        {

            var response = new ResponseEstatus();

            try
            {

                response.status = 1;
                response.result = empleadosManager.GetEmpleados();

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Empleados/Login/")]
        public IHttpActionResult LoginEmpleado(ILogin loginInfo)
        {
          
            var response = new ResponseEstatus();
            try
            {

                response.status = 1;
                response.result = empleadosManager.Login(loginInfo);
              
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Empleados/contrasena_olvido/")]
        public IHttpActionResult contrasena_olvido(dynamic json)
        {
            int estatus = 1;
            string mensaje = "Exito";
            try
            {
                estatus = empleadosManager.NotificaContrasenaOlvido((string)json.email);
                switch (estatus)
                {
                    case 0:
                        mensaje = "Error";
                        break;
                    case 1:
                        mensaje = "Ok";
                        break;
                    case 2:
                        mensaje = "El usuario no existe";
                        break;
                    case 3:
                        mensaje = "Problemas al enviar el correo";
                        break;
                }
                return Ok(new { estatus = estatus, mensaje = mensaje });

            }
            catch (Exception e)
            {
                estatus = 0;
                mensaje = e.Message;
            }

            return Ok(new { estatus = estatus, mensaje = mensaje });

        }

        [HttpPost]
        [Route("Api/Empleados/RegistroEmpleado/")]
        public IHttpActionResult RegistroEmpleado(IEmpleado loginEmpleado)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = empleadosManager.RegistroEmpleado(loginEmpleado);

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);

        }

        [HttpPost]
        [Route("Api/Empleados/UsuariosAutorizador/")]
        public IHttpActionResult UsuariosAutorizador(IEmpleado infoEmpleado) {
            var response = new ResponseEstatus();
            try {
                response.status = 1;
                response.result = empleadosManager.UsuariosAutorizador(infoEmpleado);
            }
            catch (Exception ex) {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);


        }


    }
}