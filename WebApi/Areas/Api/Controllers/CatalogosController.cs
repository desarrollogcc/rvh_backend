﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApi.Areas.Api.Models;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Areas.Api.Models.Manager;

namespace WebApi.Areas.Api.Controllers
{
    public class CatalogosController : SessionController
    {

        CatalogosManager catalogosmanager;
        public CatalogosController()
        {
            catalogosmanager = new CatalogosManager();
        }

        /*Servicio de tipo vuelo*/
        [HttpGet]
        [Route("Api/Catalogos/GetTipoVuelo/")]
        public IHttpActionResult GetTipoVuelo()
        {

            var response = new ResponseEstatus();

            try
            {

                response.status = 1;
                response.message = "información tipo vuelo";
                response.result = catalogosmanager.GetTipoVuelo();

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/SaveTipoVuelo/")]
        public IHttpActionResult SaveTipoVuelo(ITipoVuelo tipoVuelo)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = catalogosmanager.SaveTipoVuelo(tipoVuelo);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/DeleteTipovuelo/")]
        public IHttpActionResult DeleteTipovuelo(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.message = "Se elimino el registro";
                response.result = catalogosmanager.DeleteTipovuelo((int)json.intIDVuelo);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.message = ex.Message;

            }
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }

        /*Srvicio de tipo de habitación*/
        [HttpGet]
        [Route("Api/Catalogos/GetTipoHabitacion/")]
        public IHttpActionResult GetTipoHabitacion()
        {

            var response = new ResponseEstatus();

            try
            {

                response.status = 1;
                response.message = "información tipo habitación";
                response.result = catalogosmanager.GetTipoHabitacion();

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/SaveTipoHabitacion/")]
        public IHttpActionResult SaveTipoHabitacion(ITipoHabitacion tipoHabitacion)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.message = "OK";
                response.result = catalogosmanager.SaveTipoHabitacion(tipoHabitacion);

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/DeleteTipoHabitacion/")]
        public IHttpActionResult DeleteTipoHabitacion(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = catalogosmanager.DeleteTipoHabitacion((int)json.intIDTipoHabitacion);

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.result = ex.Message;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }

        /*Servicio de Tipo de viaje*/
        [HttpGet]
        [Route("Api/Catalogos/GetTipoViaje/")]
        public IHttpActionResult GetTipoViaje()
        {

            var response = new ResponseEstatus();

            try
            {

                response.status = 1;
                response.message = "información tipo viaje";
                response.result = catalogosmanager.GetTipoViaje();

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/SaveTipoViaje/")]
        public IHttpActionResult SaveTipoViaje(ITipoViaje tipoviaje)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.message = "OK";
                response.result = catalogosmanager.SaveTipoViaje(tipoviaje);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }

        [HttpPost]
        [Route("Api/Catalogos/DeleteTipoviaje/")]
        public IHttpActionResult DeleteTipoviaje(dynamic json)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.message = "Se elimino el registro";
                response.result = catalogosmanager.DeteleTipoviajes((int)json.intIDTipoViaje);
            }
            catch (Exception ex)
            {

                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }

        /* Servicio de Estatus solicitud*/
        [HttpGet]
        [Route("Api/Catalogos/GetEstatusSolicitud/")]
        public IHttpActionResult GetEstatusSolicitud()
        {

            var response = new ResponseEstatus();

            try
            {

                response.status = 1;
                response.message = "información estatus solicitud";
                response.result = catalogosmanager.GetEstatusSolicitud();

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/SaveEstatusSolicitud/")]
        public IHttpActionResult SaveEstatusSolicitud(IEstatusSolicitud estatusSolicitud)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.message = "OK";
                response.result = catalogosmanager.SaveEstatusSolicitud(estatusSolicitud);

            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/DeleteEstatusSolicitud/")]
        public IHttpActionResult DeleteEstatusSolicitud(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = catalogosmanager.DeleteEstatusSolicitud((int)json.intIDEstatusSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }

        [HttpGet]
        [Route("Api/Catalogos/GetEstatusPorPerfiles/")]
        public IHttpActionResult GetEstatusPorPerfiles()
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = catalogosmanager.GetEstatusPorPerfiles();
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        /*Servicio de tipo de solicitud*/
        [HttpGet]
        [Route("Api/Catalogos/GetTipoSolicitud/")]
        public IHttpActionResult GetTipoSolicitud()
        {

            var response = new ResponseEstatus();

            try
            {

                response.status = 1;
                response.message = "información tipo de solicitud";
                response.result = catalogosmanager.GetTipoSolicitud();

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/SaveTipoSolicitud/")]
        public IHttpActionResult SaveTipoSolicitud(ITipoSolicitud tipoSolicitud)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.message = "OK";
                response.result = catalogosmanager.SaveTipoSolicitud(tipoSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        /*Servicio de datos bancarios*/
        [HttpPost]
        [Route("Api/Catalogos/SaveCuentaBancaria/")]
        public IHttpActionResult SaveCuentaBancaria(IDatosBancarios datosbancarios)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = catalogosmanager.SaveCuentaBancaria(datosbancarios);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpGet]
        [Route("Api/Catalogos/ConsultaDatosBancarios/")]
        public IHttpActionResult ConsultaDatosBancarios()
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = catalogosmanager.ConsultaDatosBancarios();
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;

            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogos/DeleteCuentaBancaria/")]
        public IHttpActionResult DeleteCuentaBancaria(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = catalogosmanager.DeleteCuentaBancaria((int)json.intIDDatosBancarios);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }

        [HttpPost]
        [Route("Api/Catalogos/SaveDatosFiscales/")]
        public IHttpActionResult SaveDatosFiscales(IDatosFiscales datosFiscales)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 0;
                response.result = catalogosmanager.SaveDatosFiscales(datosFiscales);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);

        }

        [HttpGet]
        [Route("Api/Catalogos/GetDatosFiscales/")]
        public IHttpActionResult GetDatosFiscales()
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = catalogosmanager.GetDatosFiscales();
            }
            catch (Exception ex)
            {

                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        /*Servicio de configuracion del sistema*/
        [HttpPost]
        [Route("Api/Catalogos/SaveConfiguracion/")]
        public IHttpActionResult SaveConfiguracion(IConfiguracion dataConfiguracion)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = catalogosmanager.SaveConfiguracion(dataConfiguracion);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpGet]
        [Route("Api/Catalogos/GetConfiguracion/")]
        public IHttpActionResult GetConfiguracion()
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = catalogosmanager.GetConfiguracion();
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);


        }

        [HttpGet]
        [Route("Api/Catalagos/GetRutaHome/")]
        public IHttpActionResult GetRutaHome()
        {

            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = catalogosmanager.GetRutaHome();
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);

        }

        [HttpPost]
        [Route("Api/Catalogos/SaveSemaforo/")]
        public IHttpActionResult SaveSemaforo(ISemaforo dataSemaforo)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = catalogosmanager.SaveSemaforo(dataSemaforo);
            }
            catch (Exception e)
            {
                response.status = 1;
                response.result = e.Message;
                response.message = "Error";
            }
            return Json(response);
        }
        [HttpGet]
        [Route("Api/Catalogos/GetSemaforo/")]
        public IHttpActionResult GetSemaforo()
        {
            var response = new ResponseEstatus();
            try
            {
                response.result = catalogosmanager.GetSemaforo();
                response.status = 0;
            }
            catch (Exception ex)
            {
                response.result = ex.Message;
                response.status = 1;
                response.message = "Error";
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Catalogo/DeleteSemaforo/")]
        public IHttpActionResult DeleteSemaforo(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = catalogosmanager.DeleteSemaforo((int)json.intIDSemaforo);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.result = ex.Message;
                response.message = "Error";

            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }
    }
}