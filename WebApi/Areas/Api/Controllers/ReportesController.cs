﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApi.Areas.Api.Models;
using WebApi.Areas.Api.Models.Manager;
using WebApi.Models;

namespace WebApi.Areas.Api.Controllers
{
    public class ReportesController : GSController
    {
        private GS_CCCONTROL_VIAJESEntities db;
        ReportesManager reportesmanager;

        private ReportesController()
        {
            db = new GS_CCCONTROL_VIAJESEntities();
            reportesmanager = new ReportesManager();
        }

        [HttpPost]
        [Route("Api/Reportes/GetResumenVueloPorEmpleado/")]
        public IHttpActionResult GetResumenVueloPorEmpleado(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = reportesmanager.GetResumenVueloPorEmpleado((string)json.intIDEmpleado);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);

        }

        [HttpPost]
        [Route("Api/Reportes/GetResumenHotelPorEmpleado/")]
        public IHttpActionResult GetResumenHotelPorEmpleado(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = reportesmanager.GetResumenHotelPorEmpleado((string)json.intIDEmpleado);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Reportes/GetResumenVueloHotel/")]
        public IHttpActionResult GetResumenVueloHotel(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
               // response.result = reportesmanager.GetResumenVueloHotel((int)json.intIDEmpleado);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }


        [HttpPost]
        [Route("Api/Reportes/ConsultaSolicitudesGeneral/")]
        public IHttpActionResult ConsultaSolicitudesGeneral(dynamic json)
        {
            var result = new ResponseEstatus();
            try
            {
                result.status = 1;
                result.result = reportesmanager.ConsultaSolicitudesGeneral((string)json.intIDCodigoEmpleado);
            }
            catch (Exception ex)
            {
                result.status = 0;
                result.message = "Error";
                result.result = ex.Message;
            }

            return Json(result);

        }


        [HttpPost]
        [Route("Api/Reportes/ConsultaSolicitudesACotizar/")]
        public IHttpActionResult ConsultaSolicitudesACotizar(dynamic json)
        {
            //var dtdFechaInicio = json.dtdFechaInicio;

            //DateTime? dtdFechaInicio = null;
            //DateTime? dtdFechaFinal = null;
            //System.Nullable<DateTime> dtdFechaFinal = json.dtdFechaFinal;
            //System.Nullable<DateTime> dtdFechaInicio = json.dtdFechaInicio;
            var result = new ResponseEstatus();
            try
            {
                result.status = 1;
                //result.result = reportesmanager.ConsultaSolicitudesACotizar((int)json.intIDFolio, (DateTime?)json.dtdFechaInicio, (DateTime?)json.dtdFechaFinal);
            }
            catch (Exception ex)
            {
                result.status = 0;
                result.message = "Error";
                result.result = ex.Message;
            }

            return Json(result);

        }
    }
}