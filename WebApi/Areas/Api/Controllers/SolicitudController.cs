﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebApi.Areas.Api.Models;
using WebApi.Areas.Api.Models.Interfaces;
using WebApi.Areas.Api.Models.Manager;
using WebApi.Models;
using System.Web.Configuration;

namespace WebApi.Areas.Api.Controllers
{
    public class SolicitudController : GSController
    {
        private GS_CCCONTROL_VIAJESEntities db;
        SolicitudManager solicitudManager;

        private SolicitudController()
        {
            solicitudManager = new SolicitudManager();
            db = new GS_CCCONTROL_VIAJESEntities();
        }
        [HttpPost]
        [Route("Api/Solicitud/SaveSolicitud/")]
        public IHttpActionResult SaveSolicitud(ISolicitud solicitud)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.SaveSolicitud(solicitud);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/DetalleSolicitudHotel/")]
        public IHttpActionResult DetalleSolicitudHotel(IDetalleSolicitudHotel detalleSolicitudHotel)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = solicitudManager.DetalleSolicitudHotel(detalleSolicitudHotel);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/DetalleHuespedes/")]
        public IHttpActionResult DetalleHuespedes(IDetalleHuespedes detalleHuespedes)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.DetalleHuespedes(detalleHuespedes);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/DeleteDetalleHuespedes/")]
        public IHttpActionResult DeleteDetalleHuespedes(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.DeleteDetalleHuespedes((int)json.intFKIDDetalleSolicitudHotel, (string)json.chrCodigoEmpleado);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);


        }

        [HttpPost]
        [Route("Api/Solicitud/OperacionesSolicitudHotel/")]
        public IHttpActionResult OperacionesSolicitudHotel(IOperacionesSolicitudHotel detalleOperacionesHotel)
        {

            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.OperacionesSolicitudHotel(detalleOperacionesHotel);

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/OperacionesUpdateSolicitudHotel/")]
        public IHttpActionResult OperacionesUpdateSolicitudHotel(IOperacionesSolicitudHotel detalleOperacionesHotel)
        {

            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.OperacionesUpdateSolicitudHotel(detalleOperacionesHotel);

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/ReservacionHotel/")]
        public IHttpActionResult ReservacionHotel(IReservacionHotel reservacionHotel)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = solicitudManager.ReservacionHotel(reservacionHotel);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/DetalleSolicitudViaje/")]
        public IHttpActionResult DetalleSolicitudViaje(IDetalleSolicitudViaje detalleSolitudViaje)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = solicitudManager.DetalleSolicitudViaje(detalleSolitudViaje);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/DetalleViajeros/")]
        public IHttpActionResult DetalleViajeros(IDetalleViajeros detalleViajesros)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.DetalleViajeros(detalleViajesros);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "ERROR";
                response.result = ex.Message;

            }

            return Json(response);


        }

        [HttpPost]
        [Route("Api/Solicitud/DeleteDetalleViajeros/")]
        public IHttpActionResult DeleteDetalleViajeros(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.DeleteDetalleViajeros((int)json.intFKIDDetalleSolicitudViaje, (string)json.chrCodigoEmpleado);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);


        }

        [HttpPost]
        [Route("Api/Solicitud/OperacionesSolicitudAvion/")]
        public IHttpActionResult OperacionesSolicitudAvion(IOperacionesSolicitudAvion operacionesSolicitudAvion)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = solicitudManager.OperacionesSolicitudAvion(operacionesSolicitudAvion);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/OperacionesUpdateSolicitudAvion/")]
        public IHttpActionResult OperacionesUpdateSolicitudAvion(IOperacionesSolicitudAvion operacionesSolicitudAvion)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = solicitudManager.OperacionesUpdateSolicitudAvion(operacionesSolicitudAvion);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }


        [HttpPost]
        [Route("Api/Solicitud/ReservacionViaje/")]
        public IHttpActionResult ReservacionViaje(IReservacionViaje reservacionViaje)
        {

            var response = new ResponseEstatus();

            try
            {
                response.status = 1;
                response.result = solicitudManager.ReservacionViaje(reservacionViaje);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/GetResumenVueloPorEmpleado/")]
        public IHttpActionResult GetResumenVueloPorEmpleado(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.GetResumenVueloPorEmpleado((string)json.intIDEmpleado);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);

        }

        [HttpPost]
        [Route("Api/Solicitud/GetResumenHotelPorEmpleado/")]
        public IHttpActionResult GetResumenHotelPorEmpleado(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.GetResumenHotelPorEmpleado((string)json.intIDEmpleado);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }
        [HttpPost]
        [Route("Api/Solicitud/ObtieneSolicitud/")]
        public IHttpActionResult ObtieneDetallesSolicitud(dynamic json) {
            var response = new ResponseEstatus();

            try {
                response.result = solicitudManager.ObtieneDetallesSolicitud((int)json.intIDSolicitud);
                response.status = 1;
            }
            catch (Exception ex) {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }
        [HttpPost]
        [Route("Api/Solicitud/MuestraDatosVueloHotel/")]
        public IHttpActionResult MuestraDatosVueloHotel(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                //(int) json.intFKIDDetalleSolicitudHospedaje
                response.status = 1;
                response.result = solicitudManager.MuestraDatosVueloHotel((int)json.intIDSolicitud, (int)json.intFKIDDetalleSolicitudViaje);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;

            }

            return Json(response);

        }

        [HttpPost]
        [Route("Api/Solicitud/DeleteSolicitud/")]
        public IHttpActionResult DeleteSolicitud(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.DeleteSolicitud((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);


        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaSolicitudesACotizar/")]
        public IHttpActionResult ConsultaSolicitudesACotizar(dynamic json)
        {
            //var dtdFechaInicio = json.dtdFechaInicio;

            //DateTime? dtdFechaInicio = null;
            //DateTime? dtdFechaFinal = null;
            //System.Nullable<DateTime> dtdFechaFinal = json.dtdFechaFinal;
            //System.Nullable<DateTime> dtdFechaInicio = json.dtdFechaInicio;
            var result = new ResponseEstatus();
            try
            {
                result.status = 1;
                result.result = solicitudManager.ConsultaSolicitudesACotizar((int)json.intIDFolio, (DateTime?)json.dtdFechaInicio, (DateTime?)json.dtdFechaFinal, (int)json.intFKIDDetalleSolicitudViaje);
            }
            catch (Exception ex)
            {
                result.status = 0;
                result.message = "Error";
                result.result = ex.Message;
            }

            return Json(result);

        }

        [HttpPost]
        [Route("Api/Solicitud/CargaImagenes/")]
        public async Task<HttpResponseMessage> Insert()
        {
            HttpResponseMessage response;

            if (!Request.Content.IsMimeMultipartContent())
            {
                response = Request.CreateResponse(HttpStatusCode.BadRequest);
                response.Headers.Add("Error Message: ", HttpStatusCode.UnsupportedMediaType.ToString());
            }

            var providerInsert = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
            //access form data  
            NameValueCollection formData = providerInsert.FormData;

            //access files  
            IList<HttpContent> files = providerInsert.Files;

            response = await saveFile(providerInsert.FormData, providerInsert.Files);

            return response;

        }
        private async Task<HttpResponseMessage> saveFile(NameValueCollection formData, IList<HttpContent> files)
        {
            string filename = String.Empty;
            string nombre = string.Empty;
            string contentType = string.Empty;
            string directoryName = String.Empty;
            string urlFirma = String.Empty;
            string urlEvidencia = String.Empty;
            string urlArchivos = WebConfigurationManager.AppSettings["UrlArchivos"];

            string DocsPath = "Documentos" + "/";
            int contador = 0;

            var path = HttpRuntime.AppDomainAppPath;



            var response = Request.CreateResponse(HttpStatusCode.BadRequest);

            foreach (HttpContent file1 in files)
            {
                var thisFileName = file1.Headers.ContentDisposition.FileName.Trim('\"');
                nombre = thisFileName;
                contentType = file1.Headers.ContentType.ToString();
                Stream input = await file1.ReadAsStreamAsync();
                try
                {
                    if ((contentType == "text/xml" || contentType == "application/xml" || contentType == "application/pdf"))
                    {
                        directoryName = urlArchivos;
                        string[] separtor = urlArchivos.Split('\\');

                        urlFirma = separtor[1] + "/" + thisFileName; ;
                    }
                    else
                    {
                        directoryName = System.IO.Path.Combine(path, "Documentos");
                        urlFirma = DocsPath + thisFileName;
                    }
                    filename = System.IO.Path.Combine(directoryName, thisFileName);

                    if (!Directory.Exists(directoryName))
                    {
                        Directory.CreateDirectory(path);
                    }

                    //Deletion exists file  
                    if (File.Exists(filename))
                    {
                        File.Delete(filename);
                    }

                    try
                    {
                        using (Stream file = File.OpenWrite(filename))
                        {
                            input.CopyTo(file);
                            //close file  
                            file.Close();

                            int intFKIDSolicitud = Convert.ToInt32(formData["intFKIDSolicitud"]);

                            if (intFKIDSolicitud > 0)
                            {
                                if (solicitudManager.cargaArchivos(crearCargaArchivos(formData, urlFirma, nombre, contentType)))
                                {

                                    response = Request.CreateResponse(HttpStatusCode.OK);
                                    response.Headers.Add("Firma", urlFirma);
                                    response.Headers.Add("nombre", nombre);
                                }
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        response = Request.CreateResponse(HttpStatusCode.BadRequest);
                        response.Headers.Add("Error Message: ", e.Message);
                    }

                }
                catch (Exception e)
                {
                    response = Request.CreateResponse(HttpStatusCode.BadRequest);
                    response.Headers.Add("Error Message: ", e.Message);
                }

                contador++;
            }
            return response;
        }
        private ICargaArchivos crearCargaArchivos(NameValueCollection data, string pathFirma, string nombreArchivo, string contentType)
        {
            ICargaArchivos item = new ICargaArchivos();
            item.intFKIDTipoSolicitud = Convert.ToInt32(data["intFKIDTipoSolicitud"]);
            item.intFKIDSolicitud = Convert.ToInt32(data["intFKIDSolicitud"]);
            item.intFKIDDetalleSolicitudVueloHotel= Convert.ToInt32(data["intFKIDDetalleSolicitudVueloHotel"]);
            item.chrNombre = nombreArchivo;
            item.chrPath = pathFirma;
            item.intIDImagenesCotizar = Convert.ToInt32(data["intIDImagenesCotizar"]);
            item.intIDImagenesCotizar = Convert.ToInt32(data["intNoOpcionCotizador"]);
            item.chrContentType = contentType;
            return item;
        }

        [HttpPost]
        [Route("Api/Solicitud/EliminaImagenes/")]
        public IHttpActionResult EliminaImagenes(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.EliminaImagenes((int)json.intIDImagen);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaImagenes/")]
        public IHttpActionResult ConsultaImagenes(dynamic json)
        {
            var respose = new ResponseEstatus();
            try
            {
                respose.status = 1;
                respose.result = solicitudManager.ConsultaImagenes((int)json.intIDSolicitud, (int) json.intFKIDDetalleSolicitudViajeHotel);
            }
            catch (Exception ex)
            {
                respose.status = 0;
                respose.message = "Error";
                respose.result = ex.Message;
            }

            return Json(respose);
        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaCarga/")]
        public IHttpActionResult ConsultaCarga(dynamic json)
        {
            var respose = new ResponseEstatus();
            try
            {
                respose.status = 1;
                respose.result = solicitudManager.ConsultaCarga((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                respose.status = 0;
                respose.message = "Error";
                respose.result = ex.Message;
            }

            return Json(respose);
        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaArchivos/")]
        public IHttpActionResult ConsultaArchivos(dynamic json)
        {
            var respose = new ResponseEstatus();
            try
            {
                respose.status = 1;
                respose.result = solicitudManager.ConsultaArchivos((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                respose.status = 0;
                respose.message = "Error";
                respose.result = ex.Message;
            }

            return Json(respose);
        }

        [HttpPost]
        [Route("Api/Solicitud/PrioridadCotizacionesUsuario/")]
        public IHttpActionResult PrioridadCotizacionesUsuario(List<ICargaArchivos> listaArchivos)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.PrioridadCotizacionesUsuario(listaArchivos);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/PrioridadCotizacionesAutorizador/")]
        public IHttpActionResult PrioridadCotizacionesAutorizador(List<ICargaArchivos> listaArchivos)
        {

            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.PrioridadCotizacionesAutorizador(listaArchivos);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaSolicitudesAutorizar/")]
        public IHttpActionResult ConsultaSolicitudesAutorizar(dynamic json)
        {
            //var dtdFechaInicio = json.dtdFechaInicio;

            //DateTime? dtdFechaInicio = null;
            //DateTime? dtdFechaFinal = null;
            //System.Nullable<DateTime> dtdFechaFinal = json.dtdFechaFinal;
            //System.Nullable<DateTime> dtdFechaInicio = json.dtdFechaInicio;
            var result = new ResponseEstatus();
            try
            {
                result.status = 1;
                result.result = solicitudManager.ConsultaSolicitudesAutorizar((string)json.intIDAutorizador, (int)json.intFKIDEstatus, (int)json.intFKIDTipoSolicitud, (string)json.intFKIDSolicitante, (int) json.intFKIDDetalleSolicitudVieloHotel);
            }
            catch (Exception ex)
            {
                result.status = 0;
                result.message = "Error";
                result.result = ex.Message;
            }

            return Json(result);

        }

        [HttpPost]
        [Route("Api/Solicitud/SaveEscalasVuelo/")]
        public IHttpActionResult SaveEscalasVuelo(List<IEscalasVuelo> escalasVuelo)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.SaveEscalasVuelo(escalasVuelo);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/NotificacionesSolicitudes/")]
        public IHttpActionResult NotificacionesSolicitudes(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.NotificacionesSolicitudes((string)json.intCodigoEmpleado);

            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;

            }

            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/MarcarSolicitudVisto/")]
        public IHttpActionResult MarcarSolicitudVisto(dynamic json)
        {
            var response = new ResponseEstatus();

            try
            {
                response.status = 0;
                response.result = solicitudManager.MarcarSolicitudVisto((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;
            }

            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);

        }

        [HttpPost]
        [Route("Api/Solicitud/CerrarSolicitud/")]
        public IHttpActionResult CerrarSolicitud(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 1;
                response.result = solicitudManager.CerrarSolicitud((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 0;
                response.message = "Error";
                response.result = ex.Message;
            }
            JsonSerializerSettings serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            return Json(response, serializerSettings);
        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaOperacionAvion/")]
        public IHttpActionResult ConsultaOperacionAvion(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = solicitudManager.ConsultaOperacionAvion((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;

            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaReservacionAvion/")]
        public IHttpActionResult ConsultaReservacionAvion(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = solicitudManager.ConsultaReservacionViaje((int)json.intIDOperacionesSolicitudesAvion);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;

            }
            return Json(response);
        }
        [HttpPost]
        [Route("Api/Solicitud/ConsultaEscalasAvion/")]
        public IHttpActionResult ConsultaEscalasAvion(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = solicitudManager.ConsultaEscalasAvion((int)json.intIDOperacionesSolicitudesAvion);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;

            }
            return Json(response);
        }
        [HttpPost]
        [Route("Api/Solicitud/ConsultaOperacionHotel/")]
        public IHttpActionResult ConsultaOperacionHotel(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = solicitudManager.ConsultaOperacionHotel((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;

            }
            return Json(response);
        }
        [HttpPost]
        [Route("Api/Solicitud/ConsultaReservacionHotel/")]
        public IHttpActionResult ConsultaReservacionHotel(dynamic json)
        {
            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = solicitudManager.ConsultaReservacionHotel((int)json.intIDOperacionesSolicitudesHotel);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;

            }
            return Json(response);
        }

        [HttpPost]
        [Route("Api/Solicitud/ConsultaEmpleadosSolicitud/")]
        public IHttpActionResult ConsultaEmpleadosSolicitud(dynamic json)
        {

            var response = new ResponseEstatus();
            try
            {
                response.status = 0;
                response.result = solicitudManager.ConsultaEmpleadosSolicitud((int)json.intIDSolicitud);
            }
            catch (Exception ex)
            {
                response.status = 1;
                response.message = "Error";
                response.result = ex.Message;

            }
            return Json(response);
        }

    }
}