﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApi.Areas.Api.Models;
using WebApi.Areas.Api.Models.Manager;
using WebApi.Models;

namespace WebApi.Areas.Api.Controllers
{
    public class NotificacionesController : SessionController
    {
       
        NotificacionesManager notificacionesManager;
        private GS_CCCONTROL_VIAJESEntities db;
        public NotificacionesController()
        {
            db = new GS_CCCONTROL_VIAJESEntities();
            notificacionesManager = new NotificacionesManager();
        }

        [HttpPost]
        [Route("Api/Notificaciones/EnviarSolicitudAdministrador/")]
        public IHttpActionResult EnviarSolicitudAdministrador(dynamic json)
        {
            int estatus = 1;
            string mesaje = "Exito";

            try
            {

                estatus = notificacionesManager.EnviarSolicitudAdministrador((int)json.intIDSolicitud);
                switch (estatus)
                {
                    case 0:
                        mesaje = "Error";
                        break;
                    case 1:
                        mesaje = "OK";
                        break;
                    case 2:
                        mesaje = "El usuario no existe";
                        break;
                    case 3:
                        mesaje = "Problemas al enviar el correo";
                        break;

                }

                return Ok(new { estatus = estatus, mesaje = mesaje });
            }
            catch (Exception ex)
            {
                estatus = 0;
                mesaje = ex.Message;
            }

            return Ok(new { estatus = estatus, mesaje = mesaje });
        }

        [HttpPost]
        [Route("Api/Notificaciones/EnviaCotizacionUsuario/")]
        public IHttpActionResult EnviaCotizacionUsuario(dynamic json) {
           int estatus = 1;
            string mesaje = "Exito";

            try
            {

                estatus = notificacionesManager.EnviaCotizacionUsuario((int)json.intIDSolicitud);
                switch (estatus)
                {
                    case 0:
                        mesaje = "Error";
                        break;
                    case 1:
                        mesaje = "OK";
                        break;
                    case 2:
                        mesaje = "El usuario no existe";
                        break;
                    case 3:
                        mesaje = "Problemas al enviar el correo";
                        break;

                }

                return Ok(new { estatus = estatus, mesaje = mesaje });
            }
            catch (Exception ex)
            {
                estatus = 0;
                mesaje = ex.Message;
            }

            return Ok(new { estatus = estatus, mesaje = mesaje });
        }

        [HttpPost]
        [Route("Api/Notificaciones/CancelarCotizacionAdministrador/")]
        public IHttpActionResult CancelarCotizacionAdministrador(dynamic json)
        {
            int estatus = 1;
            string mesaje = "Exito";

            try
            {

                estatus = notificacionesManager.CancelarCotizacionAdministrador((int)json.intIDSolicitud, (string)json.chrMotivoCancelacion);
                switch (estatus)
                {
                    case 0:
                        mesaje = "Error";
                        break;
                    case 1:
                        mesaje = "OK";
                        break;
                    case 2:
                        mesaje = "El usuario no existe";
                        break;
                    case 3:
                        mesaje = "Problemas al enviar el correo";
                        break;

                }

                return Ok(new { estatus = estatus, mesaje = mesaje });
            }
            catch (Exception ex)
            {
                estatus = 0;
                mesaje = ex.Message;
            }

            return Ok(new { estatus = estatus, mesaje = mesaje });
        }

        [HttpPost]
        [Route("Api/Notificaciones/CancelarCotizacionUsuario/")]    
        public IHttpActionResult CancelarCotizacionUsuario(dynamic json)
        {
            int estatus = 1;
            string mesaje = "Exito";

            try
            {

                estatus = notificacionesManager.CancelarCotizacionUsuario((int)json.intIDSolicitud, (string)json.chrMotivoCancelacion);
                switch (estatus)
                {
                    case 0:
                        mesaje = "Error";
                        break;
                    case 1:
                        mesaje = "OK";
                        break;
                    case 2:
                        mesaje = "El usuario no existe";
                        break;
                    case 3:
                        mesaje = "Problemas al enviar el correo";
                        break;

                }

                return Ok(new { estatus = estatus, mesaje = mesaje });
            }
            catch (Exception ex)
            {
                estatus = 0;
                mesaje = ex.Message;
            }

            return Ok(new { estatus = estatus, mesaje = mesaje });
        }

        [HttpPost]
        [Route("Api/Notificaciones/EnviarSolicitudUsuarioAuditorizador/")]
        public IHttpActionResult EnviarSolicitudUsuarioAuditorizador(dynamic json) {
            int estatus = 1;
            string mesaje = "Exito";

            try
            {

                estatus = notificacionesManager.EnviarSolicitudUsuarioAuditorizador((int)json.intIDSolicitud);
                switch (estatus)
                {
                    case 0:
                        mesaje = "Error";
                        break;
                    case 1:
                        mesaje = "OK";
                        break;
                    case 2:
                        mesaje = "El usuario no existe";
                        break;
                    case 3:
                        mesaje = "Problemas al enviar el correo";
                        break;

                }

                return Ok(new { estatus = estatus, mesaje = mesaje });
            }
            catch (Exception ex)
            {
                estatus = 0;
                mesaje = ex.Message;
            }

            return Ok(new { estatus = estatus, mesaje = mesaje });
        }

        [HttpPost]
        [Route("Api/Notificaciones/EnviaSolicitudAuditorizadorAdministrador/")]
        public IHttpActionResult EnviaSolicitudAuditorizadorAdministrador(dynamic json) {
            int estatus = 1;
            string mesaje = "Exito";

            try
            {

                estatus = notificacionesManager.EnviaSolicitudAuditorizadorAdministrador((int)json.intIDSolicitud);
                switch (estatus)
                {
                    case 0:
                        mesaje = "Error";
                        break;
                    case 1:
                        mesaje = "OK";
                        break;
                    case 2:
                        mesaje = "El usuario no existe";
                        break;
                    case 3:
                        mesaje = "Problemas al enviar el correo";
                        break;

                }

                return Ok(new { estatus = estatus, mesaje = mesaje });
            }
            catch (Exception ex)
            {
                estatus = 0;
                mesaje = ex.Message;
            }

            return Ok(new { estatus = estatus, mesaje = mesaje });
        }

        [HttpPost]
        [Route("Api/Notificaciones/EnviaCorreoHotel/")]
        public IHttpActionResult EnviaCorreoHotel(dynamic json) {
            int estatus = 1;
            string mesaje = "Exito";

            try
            {

                estatus = notificacionesManager.EnviaCorreoHotel((int)json.intIDSolicitud,(string) json.chrNombreHotel, (string)json.chrCorreo,(string) json.chrEmpleados, (string) json.pathCarta);
                switch (estatus)
                {
                    case 0:
                        mesaje = "Error";
                        break;
                    case 1:
                        mesaje = "OK";
                        break;
                    case 2:
                        mesaje = "El usuario no existe";
                        break;
                    case 3:
                        mesaje = "Problemas al enviar el correo";
                        break;

                }

                return Ok(new { estatus = estatus, mesaje = mesaje });
            }
            catch (Exception ex)
            {
                estatus = 0;
                mesaje = ex.Message;
            }

            return Ok(new { estatus = estatus, mesaje = mesaje });
        }


    }
}