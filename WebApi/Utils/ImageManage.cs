﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;

namespace WebApi.Utils
{
    public class ImageManage
    {
        //public string Path
        //{
        //    get
        //    {
        //        return _path;
        //    }

        //    set
        //    {
        //        _path = value;
        //    }
        //}

        //public string PathDocumento
        //{
        //    get
        //    {
        //        return _pathDocumento;
        //    }

        //    set
        //    {
        //        _pathDocumento = value;
        //    }
        //}

        //public string NombreCarpetaGuardias
        //{
        //    get
        //    {
        //        return _nombreCarpetaGuardias;
        //    }

        //    set
        //    {
        //        _nombreCarpetaGuardias = value;
        //    }
        //}

        //public string pathFechaVisitaGuardia
        //{
        //    get
        //    {
        //        return _pathFechaVisitaGuardia;
        //    }

        //    set
        //    {
        //        _pathFechaVisitaGuardia = value;
        //    }
        //}

        public string comprimir(string path)
        {
            byte[] arreglo = null;
            byte[] gzBuffer = null;
            FileStream foto = null;
            try
            {
                foto = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                arreglo = new byte[foto.Length];
                BinaryReader reader = new BinaryReader(foto);
                arreglo = reader.ReadBytes(Convert.ToInt32(foto.Length));
                MemoryStream ms = new MemoryStream();
                using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
                {
                    zip.Write(arreglo, 0, arreglo.Length);
                }
                ms.Position = 0;
                MemoryStream outStream = new MemoryStream();

                byte[] compressed = new byte[ms.Length];
                ms.Read(compressed, 0, compressed.Length);

                gzBuffer = new byte[compressed.Length + 4];
                System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length);
                System.Buffer.BlockCopy(BitConverter.GetBytes(arreglo.Length), 0, gzBuffer, 0, 4);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                foto.Close();
                foto.Dispose();
            }
            return Convert.ToBase64String(gzBuffer);
        }

        /// <summary>
        /// Este metodo obtiene la url del path y retrna los bytes de este
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        //public byte[] getByteByPath(string path)
        //{
        //    byte[] arreglo = null;
        //    FileStream foto = null;
        //    if (path != "" && path != null)
        //        try
        //        {
        //            foto = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
        //            arreglo = new byte[foto.Length];
        //            BinaryReader reader = new BinaryReader(foto);
        //            arreglo = reader.ReadBytes(Convert.ToInt32(foto.Length));
        //            byte[] bytesToCompress = arreglo;
        //        }
        //        catch (Exception ex)
        //        {
        //            ex.ToString();
        //        }
        //        finally
        //        {
        //            foto.Close();
        //            foto.Dispose();

        //        }
        //    return arreglo;
        //}

        ///// <summary>
        ///// Este metodo obtiene los bytes del documento para crear el path del documento  
        ///// </summary> poner Mayusculas
        ///// <param name="documento"></param>
        ///// <returns></returns>
        //public Image getPathByByte(byte[] documento)
        //{
        //    Bitmap imagen = null;
        //    try
        //    {
        //        byte[] bytes = (byte[])(documento);
        //        MemoryStream ms = new MemoryStream(bytes);
        //        imagen = new Bitmap(ms);

        //    }
        //    catch (Exception ex)
        //    {

        //        ex.ToString();
        //    }
        //    return imagen;
        //}
        //string rutaimg = "";

        //public string SaveFile(string directorio, string nombre, byte[] documento)
        //{
        //    MemoryStream ms = null;
        //    Image imagen = null;

        //    if (documento == null)
        //        return "";

        //    try
        //    {
        //        ms = new MemoryStream(documento, 0, documento.Length);
        //        imagen = Image.FromStream(ms, true);

        //        directorio = directorio + nombre + ".png";
        //        if (imagen != null)
        //        {
        //            imagen.Save(directorio);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //    finally
        //    {
        //        imagen.Dispose();
        //    }
        //    return directorio;
        //}

        //public Image Base64ToImage(string base64String)
        //{
        //    byte[] imageBytes = Convert.FromBase64String(base64String);
        //    MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
        //    Image image = Image.FromStream(ms, true);
        //    return image;
        //}

        //public string ValidarDirectorio(int socio, string fechaVisita)
        //{
        //    string direccionpath = "";
        //    string PathDocumentos = Path + PathDocumento;
        //    string DirectorioCarpetaGuardia = NombreCarpetaGuardias + socio;
        //    string DirectorioCarpetaVisitaguardia = pathFechaVisitaGuardia + "/" + fechaVisita;
        //    direccionpath = PathDocumentos + DirectorioCarpetaGuardia + DirectorioCarpetaVisitaguardia;
        //    try
        //    {
        //        if (!Directory.Exists(PathDocumentos + DirectorioCarpetaGuardia + DirectorioCarpetaVisitaguardia))
        //        {
        //            Directory.CreateDirectory(PathDocumentos + DirectorioCarpetaGuardia + DirectorioCarpetaVisitaguardia);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //    return direccionpath;
        //}

    }
}