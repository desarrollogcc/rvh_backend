﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;

namespace WebApi.Utils
{
    public class GS_MAIL
    {
        string VP_STR_CUENTA_CORREO_SISTEMA = null;
        string VP_STR_PASSWORD_CORREO_SISTEMA = "";
        string VP_STR_HOST = null;
        int VP_INT_PUERTO = 587;

        public Attachment getAttachment(string file)
        {

            // Create the file attachment for this e-mail message. 
            Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);
            // Add time stamp information for the file. 
            ContentDisposition disposition = data.ContentDisposition;
            disposition.CreationDate = System.IO.File.GetCreationTime(file);
            disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
            disposition.ReadDate = System.IO.File.GetLastAccessTime(file);

            return data;

        }

        public string FM_ENVIA_CORREO(string PR_STR_TITULO, string PR_STR_MENSAJE, string PR_STR_ENVIAR_CUENTA_CORREO, Exception PR_EXC_EXCEPTION = null, List<Attachment> LIST_PR_OBJ_ATTACH = null)
        {
            string VP_STR_ERROR = "";
            VP_STR_CUENTA_CORREO_SISTEMA = ConfigurationManager.AppSettings["userName"].ToString();
            VP_STR_PASSWORD_CORREO_SISTEMA = ConfigurationManager.AppSettings["Contrasena"].ToString();
            VP_STR_HOST = ConfigurationManager.AppSettings["host"].ToString();
            VP_INT_PUERTO = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
            bool enableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["ssl"].ToString());

            try
            {
                MailMessage correo = new MailMessage();
                correo.From = new System.Net.Mail.MailAddress(VP_STR_CUENTA_CORREO_SISTEMA, "Control de Viajes");
                correo.To.Add(PR_STR_ENVIAR_CUENTA_CORREO);
                correo.Subject = PR_STR_TITULO;
                if (LIST_PR_OBJ_ATTACH != null)
                {
                    foreach (Attachment adjunto in LIST_PR_OBJ_ATTACH)
                    {
                        if ((adjunto != null))
                        {
                            correo.Attachments.Add(adjunto);
                        }
                    }
                }
                correo.IsBodyHtml = true;
                correo.Body = PR_STR_MENSAJE;
                correo.Priority = System.Net.Mail.MailPriority.High;

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(VP_STR_HOST, VP_INT_PUERTO);
                smtp.Credentials = new System.Net.NetworkCredential(VP_STR_CUENTA_CORREO_SISTEMA, VP_STR_PASSWORD_CORREO_SISTEMA);
                smtp.EnableSsl = enableSsl;
                
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                //smtp.Credentials = new System.Net.NetworkCredential(correo.From.Address, VP_STR_PASSWORD_CORREO_SISTEMA);
                smtp.Credentials = new System.Net.NetworkCredential(VP_STR_CUENTA_CORREO_SISTEMA, VP_STR_PASSWORD_CORREO_SISTEMA);
                smtp.Send(correo);

                //System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
                //smtp.Send(correo);

            }
            catch (Exception ex)
            {
                VP_STR_ERROR = "Ocurrió un problema al intentar enviar correo electrónico. " + ex.Message;
            }
            return VP_STR_ERROR;

        }

    }
}