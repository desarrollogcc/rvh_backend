﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace WebApi.Utils
{
    public class UtilsGS
    {
        public async Task<string> httpPost(string wsService, Dictionary<string, string> queryParams = null)
        {
            HttpClient client = new HttpClient();
            string responseString = string.Empty;
            FormUrlEncodedContent content = null;

            try
            {
                if (queryParams != null)
                    content = new FormUrlEncodedContent(queryParams);

                var response = await client.PostAsync(wsService, content);

                responseString = await response.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {
                responseString = null;
            }

            return responseString;
        }
    }
}